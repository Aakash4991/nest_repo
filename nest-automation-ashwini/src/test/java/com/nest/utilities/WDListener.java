package com.nest.utilities;

import org.openqa.selenium.Capabilities;

import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriverCommandListener;

public class WDListener implements QAFWebDriverCommandListener{

	@Override
	public void beforeCommand(QAFExtendedWebDriver driver, CommandTracker commandHandler) {
		
		
	}

	@Override
	public void afterCommand(QAFExtendedWebDriver driver, CommandTracker commandHandler) {
		
		
	}

	@Override
	public void onFailure(QAFExtendedWebDriver driver, CommandTracker commandHandler) {
		
		
	}

	@Override
	public void beforeInitialize(Capabilities desiredCapabilities) {
		
		
	}

	@Override
	public void onInitialize(QAFExtendedWebDriver driver) {
		
		
	}

	@Override
	public void onInitializationFailure(Capabilities desiredCapabilities, Throwable t) {
		
		
	}

}
