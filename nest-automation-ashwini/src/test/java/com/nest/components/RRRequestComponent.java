package com.nest.components;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class RRRequestComponent extends QAFWebComponent {

	public RRRequestComponent(String locator) {
		super(locator);
		
	}

	public List<QAFWebElement> getNominee() {
		return nominee;
	}

	public List<QAFWebElement> getLocationName() {
		return locationName;
	}

	public List<QAFWebElement> getRewardName() {
		return rewardName;
	}

	public List<QAFWebElement> getManagerStatus() {
		return managerStatus;
	}

	public List<QAFWebElement> getHrStatus() {
		return hrStatus;
	}

	public List<QAFWebElement> getPostedDate() {
		return postedDate;
	}

	@FindBy(locator = "column.nominee")
	private List<QAFWebElement> nominee;

	@FindBy(locator = "column.location")
	private List<QAFWebElement> locationName;

	@FindBy(locator = "column.reward.name")
	private List<QAFWebElement> rewardName;

	@FindBy(locator = "column.manager.status")
	private List<QAFWebElement> managerStatus;

	@FindBy(locator = "column.hr.status")
	private List<QAFWebElement> hrStatus;

	@FindBy(locator = "column.posted.date")
	private List<QAFWebElement> postedDate;


}
