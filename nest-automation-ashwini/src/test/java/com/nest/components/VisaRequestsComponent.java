package com.nest.components;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class VisaRequestsComponent extends QAFWebComponent {

	public VisaRequestsComponent(String locator) {
		super(locator);
	}

	public VisaRequestsComponent(QAFExtendedWebElement parent, String locator) {
		super(parent, locator);
	}

	public VisaRequestsComponent(QAFExtendedWebElement parent) {
		this(parent, "");
	}
	
	public VisaRequestsComponent(QAFWebElement parent) {
		this((QAFExtendedWebElement) parent, "");
	}

	public QAFWebElement getRequesterName() {
		return new QAFExtendedWebElement("label.requester.name.myvisarequestspage");
	}

	public QAFWebElement getCountry() {
		return new QAFExtendedWebElement("label.country.myvisarequestspage");
	}

	public QAFWebElement getVisaType() {
		return new QAFExtendedWebElement("label.visa.type.myvisarequestspage");
	}

	public QAFWebElement getInitiatedOn() {
		return new QAFExtendedWebElement("label.initiated.on.myvisarequestspage");
	}

	public QAFWebElement getRequestStatus() {
		return new QAFExtendedWebElement("label.request.status.myvisarequestspage");
	}

	public QAFWebElement getVisaStatus() {
		return new QAFExtendedWebElement("label.visa.status.myvisarequestspage");
	}

	public QAFWebElement getAction() {
		return new QAFExtendedWebElement("button.action.myvisarequestspage");
	}

}
