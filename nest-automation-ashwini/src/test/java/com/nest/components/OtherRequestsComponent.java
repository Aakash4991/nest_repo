package com.nest.components;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class OtherRequestsComponent extends QAFWebComponent {

	public OtherRequestsComponent(String locator) {
		super(locator);
	}

	@FindBy(locator = "requests.label")
	private List<QAFWebElement> requestsLabel;

	public List<QAFWebElement> getRequestsLabel() {
		return requestsLabel;
	}

	public List<QAFWebElement> getNeedInputDescription() {
		return needInputDescription;
	}

	public List<QAFWebElement> getViewDetailsIcon() {
		return viewDetailsIcon;
	}

	@FindBy(locator = "need.input.description")
	private List<QAFWebElement> needInputDescription;

	@FindBy(locator = "view.details.icon")
	private List<QAFWebElement> viewDetailsIcon;

}