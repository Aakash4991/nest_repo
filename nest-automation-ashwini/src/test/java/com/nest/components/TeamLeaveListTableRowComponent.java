package com.nest.components;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class TeamLeaveListTableRowComponent extends QAFWebComponent {

	public TeamLeaveListTableRowComponent(QAFExtendedWebElement parent, String locator) {
		super(parent, locator);
	}

	public TeamLeaveListTableRowComponent(QAFExtendedWebElement parent) {
		this(parent, "");
	}

	public TeamLeaveListTableRowComponent(String locator) {
		super(locator);
	}

	public QAFWebElement getEmployeeNameAndId() {
		return new QAFExtendedWebElement("label.empname.leavestable.teamleavelistpage");
	}

	public QAFWebElement getAppliedDate() {
		return new QAFExtendedWebElement(
				"label.applieddate.leavestable.teamleavelistpage");
	}

	public QAFWebElement getLeaveType() {
		return new QAFExtendedWebElement("label.leavetype.leavestable.teamleavelistpage");
	}

	public QAFWebElement getLeaveDuration() {
		return new QAFExtendedWebElement(
				"label.leaveduration.leavestable.teamleavelistpage");
	}

	public QAFWebElement getLeaveDate() {
		return new QAFExtendedWebElement("label.leavedate.leavestable.teamleavelistpage");
	}

	public QAFWebElement getStatus() {
		return new QAFExtendedWebElement("label.status.leavestable.teamleavelistpage");
	}

	public QAFWebElement getLeaveReason() {
		return new QAFExtendedWebElement(
				"label.leavereason.leavestable.teamleavelistpage");
	}

	public QAFWebElement getProject() {
		return new QAFExtendedWebElement(
				"label.projectname.leavestable.teamleavelistpage");
	}

	public QAFWebElement getManagerComment() {
		return new QAFExtendedWebElement(
				"label.managers.comment.leavestable.teamleavelistpage");
	}

	public QAFWebElement getActions() {
		return new QAFExtendedWebElement("label.actions.leavestable.teamleavelistpage");
	}

	public QAFWebElement getBackDatedLeave() {
		return new QAFExtendedWebElement(
				"label.backdatedleave.leavestable.teamleavelistpage");
	}

	public void performAction(String actionName) {
		boolean actionPerformed = false;
		for (QAFWebElement option : NestUtils
				.getQAFWebElements("list.options.actions.teamleavelistpage")) {
			if (option.getText().trim().equalsIgnoreCase(actionName.trim())) {
				actionPerformed = true;
				NestUtils.clickUsingJavaScript(option);
				break;
			}
		}
		if (actionPerformed)
			Reporter.log("Action performed successfully  on " + actionName,
					MessageTypes.Pass);
		else Reporter.log("Failed to perform action on " + actionName, MessageTypes.Fail);
	}
}
