package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.interactions.Actions;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class NavigationPage {

	@QAFTestStep(description = "user navigate to {0} page")
	public static void menuNavigation(String navpath) throws InterruptedException {
		NestUtils.waitForLoader();
		String[] menu = navpath.split(" : ");
		String mainMenuName = menu[0].trim();
		String subMenuName = menu[1].trim();
		openMainMenu(mainMenuName);
		waitForVisible("nav.menu.expand");
		openSubMenu(subMenuName);
		if (menu.length == 3) {
			String insidesubMenuoption = menu[2].trim();
			openSubMenu(insidesubMenuoption);
		}
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify {0} text is {1}", stepName = "userVerifyTextIs")
	public void userVerifyTextIs(String loc, String text) {
		String locText = CommonStep.getText(loc);
		String ActualBreadCrumbText = text.substring(0, locText.indexOf("\n"));
		Validator.verifyThat("BreadCrumb visible!",
				ActualBreadCrumbText.equalsIgnoreCase(text), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify {0} text contains {1}")
	public void userVerifyTextContainsIs(String loc, String text) {
		String locText = CommonStep.getText(loc);
		String ActualBreadCrumbText =
				locText.substring(locText.indexOf("Home"), locText.length())
						.replaceAll(" ", "").trim();
		Validator.verifyThat("BreadCrumb visible!",
				ActualBreadCrumbText.equalsIgnoreCase(text.replaceAll(" ", "").trim()),
				Matchers.equalTo(true));
	}

	public static void openMainMenu(String navmenu) {
		for (NavMainMenu m : NavMainMenu.values()) {
			if (m.menuEquals(navmenu)) {
				int count = 0;
				while (!m.getElement().isDisplayed()) {
					Actions dragger = new Actions(NestUtils.getDriver());
					int numberOfPixelsToDragTheScrollbarDown = 800;
					dragger.moveToElement(new QAFExtendedWebElement("nav.scrollbar"))
							.clickAndHold()
							.moveByOffset(0, numberOfPixelsToDragTheScrollbarDown)
							.release().perform();
					count++;
					if (count > 5)
						break;
				}
				count = 0;
				while (!m.getElement().isDisplayed()) {
					Actions dragger = new Actions(NestUtils.getDriver());
					int numberOfPixelsToDragTheScrollbarDown = -800;
					dragger.moveToElement(new QAFExtendedWebElement("nav.scrollbar"))
							.clickAndHold()
							.moveByOffset(0, numberOfPixelsToDragTheScrollbarDown)
							.release().perform();
					count++;
					if (count > 5)
						break;
				}
				NestUtils.clickUsingJavaScript(m.getElement());
				return;
			}
		}
		Reporter.log(navmenu + " is not present", MessageTypes.Info);
	}

	public static void openSubMenu(String navmenu) {
		for (NavSubMenu m : NavSubMenu.values()) {
			if (m.subMenuEquals(navmenu)) {
				NestUtils.clickUsingJavaScript(m.getElement());
				return;
			}
		}
		Reporter.log(navmenu + " is not present", MessageTypes.Info);
	}

	enum NavMainMenu {
		ESS("ESS"),
		LEAVE("Leave"),
		ATTENDANCE("Attendance"),
		MISC("MISC"),
		TRAVEL("Travel"),
		PERFORMANCE("Performance"),
		TRAINING("Training"),
		SEPARATION("Separation"),
		REIMBURSEMENT("Reimbursement"),
		VISA("Visa"),
		SUREYQUIZ("Survey & Quiz"),
		PUBLISHER("Publisher"),
		RELEASENOTE("Release Note"),
		IFS("IFS"),
		RR("R & R"),
		ADMINHELPDESK("Admin Helpdesk"),
		BILLINGANALYSIS("Billing Analysis"),
		ASSET("Asset"),
		COMPLIANCE("Compliance");
		private final String name;

		private NavMainMenu(String s) {
			name = s;
		}

		public QAFWebElement getElement() {
			QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("nav.list.mainmenu"),
					name));
			element.setDescription("Navigation tab for" + name);
			return element;
		}

		public boolean menuEquals(String s) {
			String menu = s != null ? s : "";
			return this.name.toUpperCase().equalsIgnoreCase(menu.toUpperCase());
		}

	}

	enum NavSubMenu {

		MYPROFILE("My Profile"),
		MYSKILLS("My Skills"),
		MYALLOCATION("My Allocation"),
		APPLYLEAVE("Apply Leave"),
		MYLEAVELST("My Leave List"),
		MYRECORDS("My Records"),
		MYTRAVELREQUESTS("My Travel Requests"),
		WORKBEHAVIOURSURVEY("Work Behaviour Survey"),
		MYKRA("My KRA"),
		ASSESMENTSUMMARY("Assessment Summary"),
		MANAGERINDEXSURVEY("Manager Index Survey"),
		FillSURVEY("Fill Survey"),
		PERFORMANCEFEEDBACK("Performance Feedback"),
		MYMONTHLYQUARTELYFEEDBACKS("My Monthly & Quarterly Feedbacks"),
		SELFASSESSMENT("Self Assessment"),
		TRAININGCALENDRA("Training Calendar"),
		MYTRAININGS("My Trainings"),
		POSTSEPARATIONREQUEST("Post Separation Request"),
		MYEXPENSELIST("My Expense List"),
		VISAREQUESTS("Visa Requests"),
		TEAMVISAREQUESTS("Team Visa Requests"),
		TEAMVISATRAVELREQUESTS("Team Visa Travel Requests"),
		VISATRAVEL("Visa Travel"),
		MYVISAREQUESTS("My Visa Requests"),
		MYVISATRAVELREQUESTS("My Visa Travel Requests"),
		MYSUREYQUIZ("My Survey & Quiz"),
		POSTS("Posts"),
		RELEASENOTES("Release Notes"),
		CONTACTS("Contacts"),
		HOLIDAYS("Holidays"),
		BIRTHDAYCALENDAR("Birthday Calendar"),
		EMPLOYMENTANNIVERSARY("Employment Anniversary"),
		FlOATINGHOLIDAYS("Floating Holidays"),
		RESOURCES("Resources"),
		MYFEEDBACK("My Feedback"),
		POSTFEEDBACK("Post Feedback"),
		NOMINATE("Nominate"),
		YOUMADEMYDAY("You Made My Day"),
		PATONBACK("Pat On Back"),
		BRIGHTSPARK("Bright Spark"),
		BRIGHTSPARKUSUK("Bright Spark (US/UK)"),
		MYRR("My R & R"),
		MYNOMINATIONS("My Nominations"),
		MYREWARDS("My Rewards"),
		MYTICKET("My Ticket"),
		POSTTICKET("Post Ticket"),
		MYACTIVITYLOG("My Activity Log"),
		ASSETMANAGEMENT("Asset Management"),
		GENDERNEUTRALITYCOMMITTEE("Gender Neutrality Committee"),
		SEXUALHARASSMENTPOLICY("Sexual Harassment Policy"),
		RRREQUESTS("R & R Requests"),
		TRAVELREQUESTS("Travel Requests"),
		TEAMLEAVELIST("Team Leave List"),
		PASTREPORTS("Past Reports"),
		SUMMARYREPORT("Summary Report"),
		MYPOSTS("My Posts"),
		REPORTS("Reports"),
		APPLYTEAMLEAVE("Apply Team Leave"),
		TEAMSREIMBURSEMENTLIST("Teams Reimbursement List"),
		MASTERS("Masters"),
		EXPENSECATEGORIES("Expense Categories"),
		STATISTICS("Statistics"),
		CONFIGURELEAVE("Configure Leave"),
		ERPCATEGORIES("ERP Categories"),
		VISACONFIGURE("Configure Visa Type"),
		EXPENSEREPORTS("Expense Reports"),
		VISATRAVELREQUEST("Visa Travel Requests"),
		TRAININGVENUES("Training Venues"),
		AIRLINEMEMBERSHIP("Airline Membership");
		private final String name;

		private NavSubMenu(String s) {
			name = s;
		}

		public QAFWebElement getElement() {

			List<QAFWebElement> elements = NestUtils.getQAFWebElements(String.format(
					ConfigurationManager.getBundle().getString("nav.list.submenu"),
					name));
			return elements.get((elements.size() - 1));
		}

		public boolean subMenuEquals(String s) {
			String subMenu = s != null ? s : "";
			return this.name.toUpperCase().equalsIgnoreCase(subMenu.toUpperCase());
		}
	}

	@QAFTestStep(description = "user verify user navigate to event page after cliking on respective event from event list also clicking on View Event List")
	public void usaUserNavigationToEventPage() {
		String description =
				new QAFExtendedWebElement("lable.eventlist.usauserhomepage").getText();
		String oldWindow = CommonPage
				.switchTabAfterClickingOnLink("lable.eventlist.usauserhomepage");
		if (CommonPage.verifyTitle(description)) {
			Reporter.logWithScreenShot("user navigated to event page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user is not  navigated to event page",
					MessageTypes.Fail);
		}
		NestUtils.getDriver().close();
		NestUtils.getDriver().switchTo().window(oldWindow);
		CommonPage.switchTabAfterClickingOnLink("link.viewAllEvent.usauserhomepage");
		if (CommonPage.verifyTitle(
				ConfigurationManager.getBundle().getString("eventPage.Title"))) {
			Reporter.logWithScreenShot("user navigated to event page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user is not  navigated to event page",
					MessageTypes.Fail);
		}

	}

}
