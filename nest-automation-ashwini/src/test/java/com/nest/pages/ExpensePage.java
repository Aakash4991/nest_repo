package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.Arrays;
import java.util.List;
import org.hamcrest.Matchers;
import org.openqa.selenium.interactions.Actions;

import com.nest.pages.CommonPage;
import com.nest.utilities.NestUtils;

import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

import com.nest.beans.AddExpenseDetailsBean;

public class ExpensePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	public static int rowcount;
	public static int num;
	public static final String TITLENEWEXPENSE = "text.title.newexpensepage";
	public static final String TITLEBOX = "textbox.title.newexpensepage";
	public static final String DROPDOWNEXPENSE =
			"dropdown.expensecategory.newexpensepage";
	public static final String TEXTBOXEXPENSE = "textbox.expenseamount.newexpensepage";
	public static final String SAVEBUTTON = "save.button.newexpensepage";
	public static final String ADDROWBUTTON = "addrow.button.newexpensepage";
	public static final String SUBMITBUTTON = "submit.button.newexpensepage";
	public static final String NUMBERPAGE = "number.page.expensepage";
	public static final String BUTTONNEWEXPENSE = "button.newexpense.expensepage";
	public static final String BUTTONBACK = "button.back.addexpensecategorypage";
	public static final String TITLEEXPENSE = "title.expense.expensepage";
	public static final String EXPENSEDATE = "item.expensedate.viewexpensepage";
	public static final String PROJECTNAME = "item.projectname.viewexpensepage";
	public static final String EXPENSECATEGORY = "expensecategory.viewexpensepage";
	public static final String BILLNUMBER = "title.billnumber.viewexpensepage";
	public static final String BASEAMOUNT = "title.baseamount.viewexpensepage";
	public static final String REMARKS = "title.remarks.viewexpensepage";
	public static final String ATTACHMENT = "title.attachment.viewexpensepage";
	public static final String APPROVALFLOW = "text.approvalflow.viewexpensepage";
	public static final String BUTTONAPPROVE = "button.approve.viewexpensepage";
	public static final String EMPLOYEEDETAILS =
			"text.employeedetails.managerviewexpensepage";
	public static final String ICONCOPY = "icon.copy.viewexpensepage";
	public static final String TEXTREQUESTTITLE = "text.requesttitle.teamexpensepage";
	public static final String TEXTEMPNAME = "text.employeename.viewexpensepage";
	public static final String DROPDOWNSTATUS = "dropdown.status.addexpensecategorypage";
	public static final String DROPDOWNACTUAL = "dropdown.actual.addexpensecategorypage";
	public static final String BUTTONSUBMIT = "button.submit.addexpensecategorypage";
	public static final String BUTTONADDEXP =
			"button.addexpensecategory.expensecategorypage";
	public static final String SEARCHBUTTON = "button.search.teamexpensepage";
	public static final String LABELDIRECT = "label.direct.teamexpensepage";
	public static final String RESETBUTTON = "reset.button.newexpensepage";
	public static final String PROJECTDROPDOWN = "dropdown.project.newexpensepage";
	public static final String EXPENSEITEM = "items.expense.viewexpensepage";
	public static final String TEXTEXPSUMMERY = "text.expensesumary.viewexpensepage";
	public static final String STATUSDROPDOWN = "btn.dropdown.status.teamexpensepage";
	public static final String DROPDOWNVALUES = "dropdown.values.teamexpensepage";
	public static final String BORDERCOLOR = "rgba(85, 85, 85, 1)";
	public static final String BORDERCOLOR_DROPDOWN = "rgba(51, 51, 51, 1)";
	public static final String EXPENSEREPORT_DROPDOWN =
			"dropdown.employee.expensereportpage";
	public static final String FINANCEDROPDOWN = "dropdown.finance.expensereportpage";
	public static final String LOCATIONVALUES = "values.location.expensereportpage";
	public static final String GROUPVALUES = "values.group.expensereportpage";
	public static final String GROUPDROPDOWN = "dropdown.group.expensereportpage";
	public static final String LOCATIONDROPDOWN = "dropdown.location.expensereportpage";
	public static final String REIMBURSEMENT_LIST =
			CommonPage.elementTextEquals("Team Reimbursement List");
	public static final String CLIENT_NAME = "value.testclient.project.newexpensepage";
	public static final String EXPENSECATEGORY_VALUE =
			"firstvalue.expensecategory.newexpensepage";
	public static final String EXPENSE_DESCRIPTION =
			"textbox.expensedescription.addexpensecategorypage";
	public static final String ACTUAL_DROPDOWN =
			"value.yes.actualdropdown.addexpensecategorypage";
	public static final String STATUSDROPDOWN_VALUE =
			"value.active.statusdropdown.addexpensecategorypage";
	public static final String EDITICON = "list.editicon.expensecategorypage";
	public static final String STATUSLABEL = "label.status.common";
	AddExpenseDetailsBean addData = new AddExpenseDetailsBean();

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		// openpage
	}

	@QAFTestStep(description = "user performs action on expense title name")
	public void clickOnExpenseTitle() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript(TITLEEXPENSE);
	}

	public List<QAFWebElement> getEmployeeDetails() {
		return NestUtils.getQAFWebElements("level.employeedetails.viewexpensepage");
	}

	@QAFTestStep(description = "user verifies view expense details")
	public void verifyViewExpenseDetails() {
		NestUtils.waitForLoader();
		Validator.verifyThat("title name is present",
				verifyPresent("title.expense.viewexpensepage"), Matchers.equalTo(true));

		Validator.verifyThat("back button is present",
				verifyPresent("button.back.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("expensedetails is present",
				verifyPresent("expensedetails.viewexpensepage"), Matchers.equalTo(true));

		Validator.verifyThat("expensedate is present", verifyPresent(EXPENSEDATE),
				Matchers.equalTo(true));
		Validator.verifyThat("projectname is present", verifyPresent(PROJECTNAME),
				Matchers.equalTo(true));
		Validator.verifyThat("expensecategory is present", verifyPresent(EXPENSECATEGORY),
				Matchers.equalTo(true));
		Validator.verifyThat("baseamount title is present", verifyPresent(BASEAMOUNT),
				Matchers.equalTo(true));

		Validator.verifyThat("billnumber title is present", verifyPresent(BILLNUMBER),
				Matchers.equalTo(true));

		Validator.verifyThat("remarks title is present", verifyPresent(REMARKS),
				Matchers.equalTo(true));
		Validator.verifyThat("attachment title is present", verifyPresent(ATTACHMENT),
				Matchers.equalTo(true));
		Validator.verifyThat("employeeid number is present",
				verifyPresent("number.employeeid.viewexpensepage"),
				Matchers.equalTo(true));
		for (int i = 0; i < getEmployeeDetails().size(); i++) {
			getEmployeeDetails().get(i).verifyPresent();
		}
		expenseEmployeeDetailscontent();
	}

	public List<QAFWebElement> getExpenseSummaryDetails() {
		return NestUtils.getQAFWebElements("label.expensesummary.viewexpensepage");
	}

	@QAFTestStep(description = "user verifies expense summary details")
	public void verifyExpenseSummaryDetails() {
		for (int i = 0; i < getExpenseSummaryDetails().size(); i++) {
			getExpenseSummaryDetails().get(i).verifyPresent();
		}
	}

	@QAFTestStep(description = "user verify navigation to {0} page")
	public void verifyNavigationDetails(String url) {
		NestUtils.waitForLoader();
		if (driver.getCurrentUrl().contains(url)) {
			Reporter.log(url + " " + "page is open", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(url + " " + "page is not open", MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getDropdownDetails(String alldropdownvalues) {
		return NestUtils.getQAFWebElements(alldropdownvalues);
	}

	public void selectValueFromDropdown(String dropdownlocator, String dropdownvaluesloc,
			String visibletext) {
		click(dropdownlocator);
		waitForVisible(dropdownvaluesloc);
		for (int i = 0; i < getDropdownDetails(dropdownvaluesloc).size(); i++) {
			if (getDropdownDetails(dropdownvaluesloc).get(i).getText()
					.equals(visibletext)) {
				getDropdownDetails(dropdownvaluesloc).get(i).click();
			}
		}
	}

	@QAFTestStep(description = "user chooses any value from Link to travel Request")
	public void chooseValueFromLinkToTravel() {
		selectValueFromDropdown("button.linktotravel.newexpensepage",
				"options.linktotravel.expensepage", "office");
	}

	@QAFTestStep(description = "user performs action on new expense")
	public void clickOnNewExpense() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript(BUTTONNEWEXPENSE);
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies expense details")
	public void verifyLinkedTravelInnercontent() {
		NestUtils.waitForLoader();
		List<QAFWebElement> linkedTravelContent =
				NestUtils.getQAFWebElements("text.linktravelrelated.expensepage");
		for (int i = 0; i < linkedTravelContent.size(); i++) {

			String value = linkedTravelContent.get(i).getText();

			Reporter.log("  " + i + linkedTravelContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in linked travel expense is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in linked travel expense is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user verifies travel details")
	public void verifyTravelDetailsInnercontent() {
		NestUtils.waitForLoader();
		List<QAFWebElement> travelContent =
				NestUtils.getQAFWebElements("text.traveldetails.expensepage");

		for (int i = 0; i < travelContent.size(); i++) {
			String value = travelContent.get(i).getText();
			Reporter.log("  " + i + travelContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in travel details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in travel details is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user choose {0} from status Dropdown")
	public void chooseStatusDropdown(String valueToSelect) {
		NestUtils.waitForLoader();
		click(STATUSDROPDOWN);
		waitForVisible(DROPDOWNVALUES);
		List<QAFWebElement> status = NestUtils.getQAFWebElements(DROPDOWNVALUES);
		for (int k = 0; k < status.size(); k++) {
			if (status.get(k).getText().contains(valueToSelect)) {
				status.get(k).click();
				status.get(k).click();
				click(STATUSDROPDOWN);
				break;
			}
		}
	}

	@QAFTestStep(description = "user performs action on pending request")
	public void clickOnPendingRequest() {
		NestUtils.waitForLoader();
		try {
			CommonPage.clickUsingJavaScript("values.title.teamexpensepage");
			NestUtils.waitForLoader();
		} catch (Exception e) {
			Reporter.logWithScreenShot("pending request tilte is not available",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies expense details for pending request")
	public void verifyPendingExpenseReq() {
		Validator.verifyThat("name title is present",
				verifyPresent("title.name.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("approvalflow is present",
				verifyPresent("approvalflow.viewexpensepage"), Matchers.equalTo(true));
		for (int i = 0; i < NestUtils.getQAFWebElements(BUTTONAPPROVE).size(); i++) {
			NestUtils.getQAFWebElements(BUTTONAPPROVE).get(i).verifyPresent();
		}
		for (int j = 0; j < NestUtils.getQAFWebElements(EMPLOYEEDETAILS).size(); j++) {
			String value = NestUtils.getQAFWebElements(EMPLOYEEDETAILS).get(j).getText();

			Reporter.log("  " + j
					+ NestUtils.getQAFWebElements(EMPLOYEEDETAILS).get(j).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in employee details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in employee details is not present</font>");
			}
		}
		verifyExpenseSummarycontent();
		for (int l = 0; l < NestUtils.getQAFWebElements(EXPENSEITEM).size(); l++) {
			String value = NestUtils.getQAFWebElements(EXPENSEITEM).get(l).getText();

			Reporter.log(
					"  " + l + NestUtils.getQAFWebElements(EXPENSEITEM).get(l).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in expense items is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in expense items is not present</font>");
			}
		}
	}
	@QAFTestStep(description = "user click on new expense button")
	public void clickOnExpenseButton() {
		NestUtils.waitForLoader();
		verifyPresent(BUTTONNEWEXPENSE);
		CommonPage.clickUsingJavaScript(BUTTONNEWEXPENSE);
	}

	@QAFTestStep(description = "user fill the required details")
	public void fillExpenseDetails() {
		NestUtils.waitForLoader();
		addData.fillRandomData();
		sendKeys(addData.getProjectTitle(), TITLEBOX);
		click(PROJECTDROPDOWN);
		click(CLIENT_NAME);
		sendKeys(addData.getProjectOther(), "value.other.project.addexpensepage");
		verifyPresent(DROPDOWNEXPENSE);
		click(DROPDOWNEXPENSE);
		click(EXPENSECATEGORY_VALUE);
		verifyPresent(TEXTBOXEXPENSE);
		sendKeys(addData.getExpenseAmount(), TEXTBOXEXPENSE);
		sendKeys(addData.getBillNumber(), "textfield.bill.addexpensepage");
		sendKeys(addData.getRemarks(), "textfield.remarks.addexpensepage");
	}

	@QAFTestStep(description = "user click on save button")
	public void clickonSAVEBUTTON() {
		waitForVisible(SAVEBUTTON);
		QAFWebElement element = new QAFExtendedWebElement(SAVEBUTTON);
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user click on reset button")
	public void clickOnResetButton() {
		QAFWebElement element = new QAFExtendedWebElement(RESETBUTTON);
		NestUtils.clickUsingJavaScript(element);
	}

	public static List<QAFWebElement> getSavedExpenseList() {
		return NestUtils.getQAFWebElements("list.saved.expense.expensepage");
	}

	public QAFWebElement getTitleTextbox() {
		return new QAFExtendedWebElement(TITLEBOX);
	}

	@QAFTestStep(description = "user details should be cleared")
	public void checkDetailsCleared() {
		verifyPresent(TITLENEWEXPENSE);
		if (getTitleTextbox().getAttribute("class").contains("ng-empty")) {
			Reporter.log("User details has been cleared", MessageTypes.Pass);
		} else {
			Reporter.log("User details not cleared", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify new expense page")
	public void verifyNewExpensePage() {
		verifyPresent(TITLEBOX);
		verifyPresent("dropdown.linktotravelrequest.newexpensepage");
		verifyPresent("datefield.expensedate.newexpensepage");
		verifyPresent(PROJECTDROPDOWN);
		verifyPresent(DROPDOWNEXPENSE);
		verifyPresent(TEXTBOXEXPENSE);
		verifyPresent("textbox.billnumber.newexpensepage");
		verifyPresent("textbox.remarks.newexpensepage");
		verifyPresent("attachment.newexpensepage");
		verifyPresent("globalattachments.newexpensepage");
		verifyPresent(ADDROWBUTTON);
		verifyPresent(SUBMITBUTTON);
		verifyPresent(SAVEBUTTON);
		verifyPresent("back.button.newexpensepage");
		verifyPresent(RESETBUTTON);
	}

	@QAFTestStep(description = "user verify new expense form")
	public void verifyNewExpenseForm() {
		verifyPresent(TITLENEWEXPENSE);
		verifyPresent("text.linktotravelrequest.newexpensepage");
		verifyPresent("text.expensedate.newexpensepage");
		verifyPresent("text.project.newexpensepage");
		verifyPresent("text.expensecategory.newexpensepage");
		verifyPresent("text.expenseamount.newexpensepage");
		verifyPresent("text.billnumber.newexpensepage");
		verifyPresent("text.remarks.newexpensepage");
		verifyPresent("text.attachment.newexpensepage");
		verifyPresent("text.attachments.newexpensepage");
		verifyPresent("text.action.newexpensepage");
		verifyPresent(SUBMITBUTTON);
		verifyPresent(SAVEBUTTON);
		verifyPresent(RESETBUTTON);
		verifyPresent("globalattachments.newexpensepage");
		verifyPresent(ADDROWBUTTON);
	}

	public List<QAFWebElement> getExpenseRow() {
		return NestUtils.getQAFWebElements("list.expenserow.newexpensepage");
	}

	@QAFTestStep(description = "user click on add row button")
	public void clickOnADDROWBUTTON() {
		NestUtils.waitForLoader();
		rowcount = getExpenseRow().size();
		verifyPresent(ADDROWBUTTON);
		click(ADDROWBUTTON);
	}

	@QAFTestStep(description = "user verify new blank row is added")
	public void verifyNewBlankRow() {
		NestUtils.waitForLoader();
		int newrowcount = getExpenseRow().size();
		if (newrowcount > rowcount) {
			Reporter.log("new blank row is added", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("new blank Row is not added", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies the breadcrumb {0}")
	public void verifyBreadcrumb(String title) {
		String navigationTitle =
				new QAFExtendedWebElement("title.reimbursement.expensepage").getText();
		if (navigationTitle.contains(title)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies page navigation")
	public void verifyPageNavigation() {
		NestUtils.waitForLoader();
		CommonPage.scrollUpToElement(NUMBERPAGE);
		List<QAFWebElement> pageNumber = NestUtils.getQAFWebElements(NUMBERPAGE);
		for (int i = 0; i < pageNumber.size(); i++) {
			pageNumber.get(i).click();
			if (new QAFExtendedWebElement(TITLEEXPENSE).isPresent()) {
				Reporter.log("user is navigated to the particular page",
						MessageTypes.Pass);
				break;
			}
		}
	}

	@QAFTestStep(description = "user verify pagination")
	public void clickOnPageNumbers() {
		NestUtils.waitForLoader();
		List<QAFWebElement> pageNumber =
				NestUtils.getQAFWebElements("number.page.expensepage");
		if (pageNumber.size() > 1) {
			for (int i = 0; i < pageNumber.size(); i++) {
				NestUtils.scrollUpToElement(pageNumber.get(i));
				pageNumber.get(i).click();
				CommonPage.clickUsingJavaScript("link.firstpage.visarequests.visa");
				CommonPage.clickUsingJavaScript("link.lastpage.visarequests.visa");
			}
		} else {
			Reporter.log("page number size" + pageNumber.size());
			Reporter.logWithScreenShot("multiple pages are not present",
					MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user fill invalid details")
	public void fillInvalidExpenseDetails() {
		NestUtils.waitForLoader();
		verifyPresent(TITLENEWEXPENSE);
		click(PROJECTDROPDOWN);
		click(CLIENT_NAME);
		verifyPresent(DROPDOWNEXPENSE);
		click(DROPDOWNEXPENSE);
		click(EXPENSECATEGORY_VALUE);
		verifyPresent(TEXTBOXEXPENSE);
		addData.fillRandomData();
		sendKeys(addData.getExpenseAmount(), TEXTBOXEXPENSE);
	}

	@QAFTestStep(description = "user verify error indication in form")
	public void verifyErrorIndication() {
		NestUtils.waitForLoader();
		verifyPresent("text.error.title.newexpensepage");
		final String input =
				new QAFExtendedWebElement("text.error.title.newexpensepage").getText();
		if (input.trim().equals("This is a required field.")) {
			Reporter.log("Error Indication appeared below Title field",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Error Indication doesnot appeared below Title field",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on submit button")
	public void clickonSUBMITBUTTON() {

		waitForVisible(SUBMITBUTTON);
		QAFWebElement element = new QAFExtendedWebElement(SUBMITBUTTON);
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user verifies Teams Reimbursement List page")
	public void verifyTeamsReimbursementPage() {
		NestUtils.waitForLoader();
		Validator.verifyThat("reimbursementlist title is present",
				verifyPresent(REIMBURSEMENT_LIST), Matchers.equalTo(true));
		Validator.verifyThat("reimbursement pagetitle is present",
				verifyPresent("pagetitle.reimbursement.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("startdate option is present",
				verifyPresent("option.startdate.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("enddate option is present",
				verifyPresent("option.enddate.teamexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("direct label is present", verifyPresent(LABELDIRECT),
				Matchers.equalTo(true));
		List<QAFWebElement> buttons = NestUtils.getQAFWebElements(SEARCHBUTTON);
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).verifyPresent();
		}
		List<QAFWebElement> calenderIcon =
				NestUtils.getQAFWebElements("icon.calender.teamexpensepage");
		for (int j = 0; j < calenderIcon.size(); j++) {
			calenderIcon.get(j).verifyPresent();
		}

		List<QAFWebElement> columnName =
				NestUtils.getQAFWebElements("title.tablecolumn.teamexpensepage");
		for (int k = 0; k < columnName.size(); k++) {
			columnName.get(k).verifyPresent();
		}

		List<QAFWebElement> pageNumber = NestUtils.getQAFWebElements(NUMBERPAGE);
		for (int l = 0; l < pageNumber.size(); l++) {
			pageNumber.get(l).verifyPresent();
		}

		List<QAFWebElement> dropdown =
				NestUtils.getQAFWebElements("button.employee.teamexpensepage");
		for (int m = 0; m < dropdown.size(); m++) {
			dropdown.get(m).verifyPresent();
		}
	}

	@QAFTestStep(description = "user verifies my expense list page")
	public void verifyMyExpenseListPage() {
		NestUtils.waitForLoader();
		Validator.verifyThat("myexpense title is present",
				verifyPresent("title.myexpense.myexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("reimbursement pagetitle is present",
				verifyPresent("pagetitle.reimbursement.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("newexpense button is present",
				verifyPresent(BUTTONNEWEXPENSE), Matchers.equalTo(true));
		List<QAFWebElement> columnTitle =
				NestUtils.getQAFWebElements("title.tablecolumn.myexpensepage");
		for (int i = 0; i < columnTitle.size(); i++) {
			columnTitle.get(i).verifyPresent();
		}
		List<QAFWebElement> pageNumber = NestUtils.getQAFWebElements(NUMBERPAGE);
		for (int j = 0; j < pageNumber.size(); j++) {
			pageNumber.get(j).verifyPresent();
		}
	}

	@QAFTestStep(description = "user click on copy button")
	public void clickOnCopyButton() {
		NestUtils.waitForLoader();
		rowcount = getExpenseRow().size();
		click("button.copy.newexpensepage");
	}

	@QAFTestStep(description = "current row should be copied")
	public void verifyRowCopied() {
		int newrowcount = getExpenseRow().size();
		if (newrowcount > rowcount) {
			Reporter.log("current row copied", MessageTypes.Pass);
		} else Reporter.logWithScreenShot("current row not copied", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user click on delete button")
	public void clickOnDeleteButton() {
		rowcount = getExpenseRow().size();
		CommonPage.clickUsingJavaScript("list.button.delete.newexpensepage");
	}

	@QAFTestStep(description = "row should be deleted")
	public void verifyRowDeleted() {
		if (getExpenseRow().size() != rowcount) {
			Reporter.log("expense row deleted", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("not able to delete expense row",
					MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getstatusList() {
		return NestUtils.getQAFWebElements("list.status.expensepage");
	}

	@QAFTestStep(description = "user verifies my expense List screen")
	public void verifyExpenseListScreen() {
		NestUtils.waitForLoader();
		String[] statusList =
				{"", "Pending (Manager)", "Approved, Paid", "Rejected (Manager)"};

		verifyPresent("text.requestDate.expensepage");
		verifyPresent("text.title.expensepage");
		verifyPresent("text.approvalstatus.expensepage");
		verifyPresent(BUTTONNEWEXPENSE);
		verifyPresent(NUMBERPAGE);

		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(statusList).contains(getstatusList().get(i).getText());
		}
	}

	@QAFTestStep(description = "user verifies expense category screen")
	public void verifyExpenseCategoryScreen() {
		NestUtils.waitForLoader();
		verifyPresent("text.column.expensecategory.expensecategorypage");
		verifyPresent("text.column.categorydescription.expensecategorypage");
		verifyPresent("text.column.actual.expensecategorypage");
		verifyPresent("text.column.elligibleamount.expensecategorypage");
		verifyPresent("text.column.status.expensecategorypage");
		verifyPresent("text.column.action.expensecategorypage");
		verifyPresent(BUTTONADDEXP);
		verifyPresent(NUMBERPAGE);
		verifyPresent("list.toggleoption.expensecategorypage");
		verifyPresent(EDITICON);
	}

	@QAFTestStep(description = "user click on add expense category button")
	public void clickOnAddExpenseCategoryButton() {
		waitForVisible(BUTTONADDEXP);
		CommonPage.clickUsingJavaScript(BUTTONADDEXP);
	}

	@QAFTestStep(description = "user verify add expense category screen")
	public void verifyAddExpenseCategoryScreen() {
		NestUtils.waitForLoader();
		verifyPresent("text.expensetitle.addexpensecategorypage");
		verifyPresent("text.expensedescription.addexpensecategorypage");
		verifyPresent("text.actual.addexpensecategorypage");
		verifyPresent("text.status.addexpensecategorypage");
		verifyPresent(BUTTONSUBMIT);
		verifyPresent(BUTTONBACK);
		Validator.verifyThat("eligible amount field is present",
				verifyPresent("textfield.amount.addexpensecategorypage"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user fill valid details")
	public void fillValidDetails() {
		NestUtils.waitForLoader();
		addData.fillRandomData();
		sendKeys(addData.getExpenseTitle(),
				"textbox.expensetitle.addexpensecategorypage");
		sendKeys(addData.getExpenseDescription(), EXPENSE_DESCRIPTION);
		waitForPresent(DROPDOWNACTUAL);
		click(DROPDOWNACTUAL);
		click(ACTUAL_DROPDOWN);
		waitForPresent(DROPDOWNSTATUS);
		click(DROPDOWNSTATUS);
		click(STATUSDROPDOWN_VALUE);
	}

	public List<QAFWebElement> getExpenseCategoryList() {
		return NestUtils
				.getQAFWebElements("list.saved.expensecategory.expensecategorypage");
	}

	@QAFTestStep(description = "user details should be updated")
	public void verifyUserDetailUpdated() {
		NestUtils.waitForLoader();
		Validator.verifyThat("data updated successfully", verifyPresent(STATUSLABEL),
				Matchers.equalTo(true));
		CommonPage.clickUsingJavaScript(STATUSLABEL);
	}

	@QAFTestStep(description = "user fill invalid expense category details")
	public void fillInvalidExpense() {
		NestUtils.waitForLoader();
		addData.fillRandomData();
		sendKeys(addData.getExpenseDescription(), EXPENSE_DESCRIPTION);
		waitForPresent(DROPDOWNACTUAL);
		click(DROPDOWNACTUAL);
		click(ACTUAL_DROPDOWN);
		waitForPresent(DROPDOWNSTATUS);
		click(DROPDOWNSTATUS);
		click(STATUSDROPDOWN_VALUE);
	}

	@QAFTestStep(description = "user verify error highlighted")
	public void verifyErrorTITLEBOX() {
		verifyPresent("error.expensetitle.expensecategorypage");
		click(BUTTONBACK);
	}

	@QAFTestStep(description = "user click on submit button on expense category page")
	public void clickOnSubmit() {
		NestUtils.waitForLoader();
		waitForVisible(BUTTONSUBMIT);
		QAFExtendedWebElement element = new QAFExtendedWebElement(BUTTONSUBMIT);
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user click on back button on expense category page")
	public void clickOnBackButton() {
		NestUtils.waitForLoader();
		waitForVisible(BUTTONBACK);
		QAFExtendedWebElement element = new QAFExtendedWebElement(BUTTONBACK);
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user details should not be saved")
	public void verifyUserDetailNotSaved() {
		NestUtils.waitForLoader();
		Validator.verifyThat("expense category not added successfully",
				verifyNotVisible(STATUSLABEL), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user click on expense first available expense")
	public void clickOnFirstAvailableExpense() {
		NestUtils.waitForLoader();
		waitForVisible("list.saved.expense.expensepage");

		if (getSavedExpenseList().isEmpty()) {
			Reporter.log("No expense list available", MessageTypes.Fail);
		} else {
			getSavedExpenseList().get(0).click();
			Reporter.log("clicked on first expense", MessageTypes.Pass);
		}
	}

	public List<QAFWebElement> getExpenseColumnList() {
		return NestUtils
				.getQAFWebElements("list.text.expensesummarytable.viewexpensepage");
	}

	@QAFTestStep(description = "user verifies view expense details screen elements")
	public void verifyViewExpenseScreen() {
		NestUtils.waitForLoader();
		waitForVisible(TEXTEMPNAME);

		verifyPresent(TEXTEMPNAME);
		verifyPresent("text.employeeid.viewexpensepage");
		String[] employeecolumn = {"Group", "Designation", "Band", "Location"};
		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(employeecolumn).contains(getEmployeeDetails().get(i).getText());
		}

		String[] expenseDetails = {"Currency", "Employee Paid Expense", "Total Expense",
				"Less Company Paid Expense", "Less Advance Cash",
				"Balance (Pay/Recover)"};
		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(expenseDetails)
					.contains(getExpenseColumnList().get(i).getText());
		}
		verifyPresent(EXPENSEDATE);
		verifyPresent(PROJECTNAME);
		verifyPresent(EXPENSECATEGORY);
		verifyPresent(BASEAMOUNT);
		verifyPresent(BILLNUMBER);
		verifyPresent(REMARKS);
		verifyPresent(ATTACHMENT);
		verifyPresent(APPROVALFLOW);
	}

	@QAFTestStep(description = "user verify team reimbursement screen")
	public void verifyTeamReimbursementPage() {
		NestUtils.waitForLoader();
		verifyPresent("option.startdate.teamexpensepage");
		verifyPresent("option.enddate.teamexpensepage");
		List<QAFWebElement> buttons = NestUtils.getQAFWebElements(SEARCHBUTTON);
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).verifyPresent();
		}
		List<QAFWebElement> calenderIcon =
				NestUtils.getQAFWebElements("icon.calender.teamexpensepage");
		for (int j = 0; j < calenderIcon.size(); j++) {
			calenderIcon.get(j).verifyPresent();
		}
		verifyPresent("text.name.teamexpensepage");
		verifyPresent(LABELDIRECT);

		List<QAFWebElement> columnName =
				NestUtils.getQAFWebElements("title.tablecolumn.teamexpensepage");
		final String[] statusList = {"Pending", "Approved", "Rejected", "Canceled"};
		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(statusList).contains(columnName.get(i).getText());
		}
	}

	@QAFTestStep(description = "user search for expense request")
	public void searchForExpenseRequest() {
		waitForVisible(SEARCHBUTTON);
		QAFExtendedWebElement element = new QAFExtendedWebElement(SEARCHBUTTON);
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user verifies expense search result")
	public void verifieExpenseSearchResult() {
		waitForVisible(SEARCHBUTTON);
		verifyPresent(TEXTREQUESTTITLE);

		if (verifyPresent(TEXTREQUESTTITLE)) {
			Reporter.log("expense search result present", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("no expense search result present",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on first available saved expense")
	public void clickOnFirstSavedExpense() {
		waitForVisible("list.savedexpense.expensepage");
		click("list.savedexpense.expensepage");
		NestUtils.waitForLoader();
		waitForVisible(TEXTEMPNAME);
		verifyPresent(TEXTEMPNAME);
	}

	@QAFTestStep(description = "user click on edit icon")
	public void clickOnEditIcon() {
		CommonPage.clickUsingJavaScript("icon.edit.viewexpensepage");
	}

	@QAFTestStep(description = "user click on copy icon")
	public void clickOnCopyIcon() {
		waitForVisible(ICONCOPY);
		CommonPage.clickUsingJavaScript(ICONCOPY);
	}

	@QAFTestStep(description = "user edits field in expense page")
	public void editField() {
		NestUtils.waitForLoader();
		addData.fillRandomData();
		sendKeys(addData.getProjectTitle(), TITLEBOX);
	}

	@QAFTestStep(description = "user uncheck direct reportees and verify for expense request")
	public void searchExpenseAfterUncheck() {
		waitForVisible(SEARCHBUTTON);
		QAFExtendedWebElement element = new QAFExtendedWebElement(LABELDIRECT);
		NestUtils.clickUsingJavaScript(element);
		QAFExtendedWebElement element1 = new QAFExtendedWebElement(SEARCHBUTTON);
		NestUtils.clickUsingJavaScript(element1);

		if (verifyPresent(TEXTREQUESTTITLE)) {
			Reporter.log("expense present after unchecking direct reportees",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					" no expense present after unchecking direct reportees",
					MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getPendingExpense() {
		return NestUtils.getQAFWebElements("list.pendingexpense.teamexpensepage");
	}

	@QAFTestStep(description = "user verifies field with {0} error message")
	public void errorMessage(String message) {
		QAFExtendedWebElement element =
				new QAFExtendedWebElement(CommonPage.elementTextEquals(message));
		List<QAFWebElement> title = NestUtils
				.getQAFWebElements("textarea.expensetitle.addexpensecategorypage");
		for (int i = 0; i < title.size(); i++) {
			Validator.verifyThat("FieldBorderColor",
					title.get(i).getCssValue("color").equals(BORDERCOLOR),
					Matchers.equalTo(true));
			new Actions(driver).moveToElement(title.get(i)).build().perform();
			if (element.verifyPresent()) {
				Reporter.log("This is a required field is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("This is a required field is not displayed",
						MessageTypes.Fail);
			}
		}

		List<QAFWebElement> dropdown = NestUtils
				.getQAFWebElements("button.actualdropdown.addexpensecategorypage");
		for (int j = 0; j < dropdown.size(); j++) {
			Validator.verifyThat("FieldBorderColor",
					dropdown.get(j).getCssValue("color").equals(BORDERCOLOR_DROPDOWN),
					Matchers.equalTo(true));
			new Actions(driver).moveToElement(dropdown.get(j)).build().perform();
			if (element.verifyPresent()) {
				Reporter.log("This is a required field is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("This is a required field is not displayed",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user performs action erp category")
	public void clickOnErpCategory() {
		CommonPage.clickUsingJavaScript("button.adderp.erpcategorypage");
	}

	@QAFTestStep(description = "user verifies EPR categories page")
	public void verifyErpPage() {
		Validator.verifyThat(CommonStep.getText("title.allpage"),
				Matchers.containsString("ERP Categories"));
		List<QAFWebElement> columnName =
				NestUtils.getQAFWebElements("list.coulmntitle.erpcategorypage");
		final String[] statusList =
				{"Expense Category Id", "ERP Category", "Status", "Action"};
		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(statusList).contains(columnName.get(i).getText().trim());
		}
		verifyPresent("button.adderp.erpcategorypage");
	}

	public void verifyColumnHeader() {
		List<QAFWebElement> tableHeader =
				NestUtils.getQAFWebElements("table.header.post.publisher");
		Validator.verifyThat("Column name must be", tableHeader.get(0).getText(),
				Matchers.equalToIgnoringCase("Reimbursement Id"));
		Validator.verifyThat("Column name must be", tableHeader.get(1).getText(),
				Matchers.equalToIgnoringCase("Employee Name (ID)"));
		Validator.verifyThat("Column name must be", tableHeader.get(2).getText(),
				Matchers.equalToIgnoringCase("Designation"));
		Validator.verifyThat("Column name must be", tableHeader.get(3).getText(),
				Matchers.equalToIgnoringCase("Group/ Account"));
		Validator.verifyThat("Column name must be", tableHeader.get(4).getText(),
				Matchers.equalToIgnoringCase("Location"));
		Validator.verifyThat("Column name must be", tableHeader.get(5).getText(),
				Matchers.equalToIgnoringCase("Expense Date"));
		Validator.verifyThat("Column name must be", tableHeader.get(6).getText(),
				Matchers.equalToIgnoringCase("Expense Type"));
		Validator.verifyThat("Column name must be", tableHeader.get(7).getText(),
				Matchers.equalToIgnoringCase("Project"));
		Validator.verifyThat("Column name must be", tableHeader.get(8).getText(),
				Matchers.equalToIgnoringCase("Total Amount"));
	}

	@QAFTestStep(description = "user verifies ui of expenses reports")
	public void verifyExpenseReports() {
		List<QAFWebElement> allDropdown =
				NestUtils.getQAFWebElements(EXPENSEREPORT_DROPDOWN);
		for (QAFWebElement dropdown : allDropdown) {
			Validator.verifyThat(
					"Employee Name,Group/Account,Location and Finance Status Dropdown are present",
					dropdown.verifyPresent(), Matchers.equalTo(true));
		}
		verifyColumnHeader();
	}

	@QAFTestStep(description = "user verifies other details")
	public void verifyExpenseReportsOther() {
		NestUtils.waitForLoader();
		List<QAFWebElement> allDropdown =
				NestUtils.getQAFWebElements(EXPENSEREPORT_DROPDOWN);
		for (QAFWebElement dropdown : allDropdown) {
			Validator.verifyThat(
					"Employee Name,Group/Account,Location and Finance Status Dropdown are not visible",
					dropdown.verifyNotVisible(), Matchers.equalTo(true));
		}
		verifyColumnHeader();
	}

	@QAFTestStep(description = "user select from {0} date")
	public void selectFromDateExpenseReport(String date) {
		LeavePage leave = new LeavePage();
		leave.fillFromDate(NestUtils.getLeaveDate(Integer.parseInt(date)));
	}

	@QAFTestStep(description = "user verifies title {0} of a page")
	public void verifyTitleExpenseReport(String pageTitle) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement title =
				new QAFExtendedWebElement("text.title.expensereportpage");
		Validator.verifyThat("Title must be", title.getText(),
				Matchers.equalTo(pageTitle));
	}

	@QAFTestStep(description = "user choose {0} from employee Dropdown in expense reports")
	public void chooseEmployeeDropdownExport(String employeeName) {
		NestUtils.waitForLoader();
		selectValueFromDropdown(VisaRequests.EMPLOYEE_DROPDOWN,
				VisaRequests.DROPDOWN_VALUES, employeeName);
	}

	@QAFTestStep(description = "user choose {0} from finance Dropdown in expense reports")
	public void chooseFinanceDropdownExport(String financeOption) {
		NestUtils.waitForLoader();
		selectValueFromDropdown(FINANCEDROPDOWN, VisaRequests.DROPDOWN_VALUES,
				financeOption);
	}

	@QAFTestStep(description = "user choose {0} from location Dropdown in expense reports")
	public void chooseLocationDropdownExport(String location) {
		NestUtils.waitForLoader();
		selectValueFromDropdown("dropdown.location.expensereportpage", LOCATIONVALUES,
				location);
	}

	@QAFTestStep(description = "user choose {0} from group Dropdown in expense reports")
	public void chooseGroupDropdownExport(String groupOption) {
		NestUtils.waitForLoader();
		selectValueFromDropdown("dropdown.group.expensereportpage", GROUPVALUES,
				groupOption);
	}

	@QAFTestStep(description = "user verifies search criteria details")
	public void verifySearchExpenseReport() {
		NestUtils.waitForLoader();
		List<QAFWebElement> searchContent =
				NestUtils.getQAFWebElements("table.data.expensereportpage");
		for (int i = 0; i < searchContent.size(); i++) {

			String value = searchContent.get(i).getText();

			Reporter.log("  " + i + searchContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot("Inner content value is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user verifies dropdown default values in expense reports")
	public void DefaultValueDropdownExport() {
		NestUtils.waitForLoader();
		Validator.verifyThat("Employee name dropdown",
				new QAFExtendedWebElement("dropdown.selectemployee.visarequests.visa")
						.getText().contains("Select"),
				Matchers.equalTo(true));
		Validator.verifyThat("Group dropdown",
				new QAFExtendedWebElement(GROUPDROPDOWN).getText().contains("All"),
				Matchers.equalTo(true));
		Validator.verifyThat("Location dropdown",
				new QAFExtendedWebElement(LOCATIONDROPDOWN).getText().contains("All"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies dynamic content")
	public void verifyDynamiccontent() {
		NestUtils.waitForLoader();
		verifyLinkedTravelInnercontent();
		verifyTravelDetailsInnercontent();
		verifyExpenseSummaryDetails();
		verifyExpenseSummarycontent();
	}

	public void verifyExpenseSummarycontent() {
		for (int k = 0; k < NestUtils.getQAFWebElements(TEXTEXPSUMMERY).size(); k++) {
			String value = NestUtils.getQAFWebElements(TEXTEXPSUMMERY).get(k).getText();

			Reporter.log("  " + k
					+ NestUtils.getQAFWebElements(TEXTEXPSUMMERY).get(k).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in expense summary details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in expense summary details is not present</font>");
			}
		}
	}

	public void expenseEmployeeDetailscontent() {
		for (int j = 0; j < NestUtils.getQAFWebElements(EMPLOYEEDETAILS).size(); j++) {
			String value = NestUtils.getQAFWebElements(EMPLOYEEDETAILS).get(j).getText();

			Reporter.log("  " + j
					+ NestUtils.getQAFWebElements(EMPLOYEEDETAILS).get(j).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in employee details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in employee details is not present</font>");
			}
		}
	}

	public void clickOnPendingExpenseRequest(String button) {
		NestUtils.waitForLoader();
		if (getPendingExpense().isEmpty()) {
			Reporter.logWithScreenShot(" There is no pending request", MessageTypes.Fail);
		} else {
			getPendingExpense().get(0).click();
		}
		NestUtils.waitForLoader();
		sendKeys(button, "textbox.commentarea.viewexpensepage");
		PublisherPage publisher = new PublisherPage();
		publisher.clickOnButton(button);
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify {0} functionality for first pending expense request")
	public void verifyExpenseRequestStatus(String button) {
		clickOnPendingExpenseRequest(button);
		verifyUserDetailUpdated();
	}

	@QAFTestStep(description = "user fills data in add erpcategory page")
	public void fillErpCategoryDetails() {
		NestUtils.waitForLoader();
		addData.fillRandomData();
		sendKeys(addData.getErpId(), "textfield.erpid.adderpcategorypage");
		sendKeys(addData.getProjectTitle(), TITLEBOX);
	}

	@QAFTestStep(description = "user performs action on edit icon in row")
	public void clickOnRowEditIcon() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript(EDITICON);
	}

	@QAFTestStep(description = "user choose {0} value from dropdown in expense page")
	public void chooseValueDropdownExport(String actualOption) {
		NestUtils.waitForLoader();
		selectValueFromDropdown(DROPDOWNACTUAL, "value.actual.addexpensecategorypage",
				actualOption);
	}
}
