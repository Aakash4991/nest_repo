package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.nest.beans.InitiateVisaRequestBean;
import com.nest.components.CalendarComponent;
import com.nest.components.VisaRequestComponent;
import com.nest.components.VisaRequestsComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class VisaRequests extends WebDriverBaseTestPage<WebDriverTestPage> {
	public static final String EMPLOYEE_DROPDOWN =
			"dropdown.selectemployee.visarequests.visa";
	public static final String REQUEST_STATUS = "dropdown.selectstatus.visarequests.visa";
	public static final String VISA_STATUS = "dropdown.visastatus.visarequests.visa";
	public static final String VISA_TYPE = "dropdown.visatype.visarequests.visa";
	public static final String DROPDOWN_VALUES = "value.dropdown.visarequests.visa";
	public static final String HEADING_VISAPAGE =
			CommonPage.elementTextEquals("Initiate Visa Request");
	public static final String EMP_NAME = CommonPage.elementTextEquals("Employee Name");
	public static final String BREADCRUMB_MESSAGE =
			"breadcrumb is present as per menu navigation";
	public static final String BREADCRUMB_NOTPRESENT =
			"breadcrumb is not present as per menu navigation";
	public static final String FIELD_DROPDOWN = "field format is dropdown";
	public static final String BACKGROUND_COLOR = "rgba(51, 51, 51, 1)";
	public static final String BACKGROUND_COLOUR = "rgba(255, 255, 255, 1)";
	public static final String COLOR_MSG = "Theme of application is Gray!";
	public static final String SELECT_EMPLOYEE = "Select Employee";
	public static final String LOADING_BAR = "loading.bar";
	public static final String SUCCESS_MSG = "success.msg";
	public static final String BUTTON_NAME = "button.name";
	public static final String VISA_DETAIL = "visa.detail.column";
	public static final String VISA_SUMMARY = "visa.summary.view.details.tab";
	public static final String VISA_SWITCHING = "visa.switching.tab";
	public static final String PREVIOUS_HISTORY = "link.view.previosus.history";
	public static final String DATE_FORMAT = "dd-MMMM-yyyy";
	public static final String DROPDOWN_SELECT_TYPE = "dropdown.select.type.of.visa";
	public static final String SELECT_COUNTRY = "dropdown.select.country";
	public static final String VISA_SELECT_STATUS = "dropdown.select.visa.status";
	public static final String VISA_REQUEST_STATUS =
			"dropdown.select.visa.request.status";
	public static final String DATE_PICKER_POPUP = "uib-datepicker-popup";
	public static final String SCROLL_TO =
			"window.scrollTo(0,document.body.scrollHeight)";
	/*
	 * public static final String VISA_SWITCHING_TABS =
	 * "list.switching.tabs.myvisarequestspage";
	 */
	public static final String EMPLOYEE_IMAGE = "visa.detail.view.image.of.emp";
	public static final String EMPLOYEE_DESIGNATION =
			"visa.detail.view.designation.of.emp";
	public static final String FOOTER_TEXT =
			"All Rights Reserved @ Infostretch NEST 2018";
	public static final String RESET = "Reset";
	public static final String READYDATE = "visaReadyDate";
	public static final String FIELD_CALENDAR = "field format is calendar";
	public static final String BACKBUTTON = "Back";
	public static final String HEADINGVISA = "Initiate Visa Request";
	public static final String SEARCH_BUTTON = CommonPage.elementTextEquals("Search");

	CommonPage common = new CommonPage();
	RnRPage rnrPage = new RnRPage();
	RR_RequestPage rrrequest = new RR_RequestPage();
	CalendarComponent calendar = new CalendarComponent();
	List<QAFWebElement> filterValues = null;

	@FindBy(locator = "pagination.number.list")
	private List<QAFWebElement> paginations;

	@FindBy(locator = "rr.requests.table")
	private List<VisaRequestComponent> visasRequestList;

	@FindBy(locator = "list.switching.tabs.myvisarequestspage")
	private List<QAFWebElement> switchingTabs;

	@FindBy(locator = "list.header.sections.visapage")
	private List<QAFWebElement> sectionHeaders;

	@FindBy(locator = "list.header.subsection.visapage")
	private List<QAFWebElement> subSectionHeaders;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		// Over-rided method
	}

	@QAFTestStep(description = "user switch the window to manager view")
	public void switchwindow() {
		common.switchToViews();
	}

	@QAFTestStep(description = "user clicks on {0}")
	public void clickNewVisaRequest(String newvisa) {
		QAFExtendedWebElement newvisarequest =
				new QAFExtendedWebElement(CommonPage.elementTextContains(newvisa));

		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForElementToBeClickable(newvisarequest);
		NestUtils.clickUsingJavaScript(newvisarequest);
	}

	@QAFTestStep(description = "user should be able to click on {0} name dropdown")
	public void clickonemployeenameforvisapage(String selectemp) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement emp =
				new QAFExtendedWebElement(CommonPage.elementTextContains(selectemp));
		Reporter.log(emp.getText());
		emp.click();
	}

	@QAFTestStep(description = "user enters text {0} in search textbox")
	public void enteremployeename(String employeename) {
		sendKeys(employeename, "txtbox.search.visapage");
	}

	@QAFTestStep(description = "user should be able to see employee name {0} in dropdown")
	public void verifyemployeename(String selectemp) {
		QAFExtendedWebElement selectemployee =
				new QAFExtendedWebElement(CommonPage.elementTextEquals(selectemp));
		selectemployee.verifyPresent();

	}
	@QAFTestStep(description = "click on {0} for visa page")
	public void clikonempname(String empname) {
		QAFExtendedWebElement nameofemp =
				new QAFExtendedWebElement(CommonPage.elementTextEquals(empname));

		nameofemp.click();
	}
	@QAFTestStep(description = "click on employeename {0}")
	public void clickonserachemployeename(String name) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement nameemp = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("label.common.vispage"),
				name));
		nameemp.click();
	}

	@QAFTestStep(description = "user should verify the details with name and designation {0} {1}")
	public void verifyfieldformat(String empdetailpage, String designation) {
		NestUtils.waitForLoader();
		verifyPresent("img.employeename.visapage");

		QAFWebElement editButton = new QAFExtendedWebElement("img.edit.visapage");
		NestUtils.scrollUpToElement(editButton);
		verifyPresent("img.edit.visapage");

		QAFExtendedWebElement countryvisit =
				new QAFExtendedWebElement("calendar.common.visapage");
		String dropdowncountryvisit = countryvisit.getAttribute("data-toggle");
		if (dropdowncountryvisit.contains("dropdown")) {
			Reporter.log(FIELD_DROPDOWN, MessageTypes.Pass);
		}
		QAFExtendedWebElement calendar =
				new QAFExtendedWebElement("calendar.common.visapage");
		String traveldate = calendar.getAttribute("uib-datepicker-popup");
		if (traveldate.contains("dd-MMMM-yyyy")) {
			Reporter.log(FIELD_CALENDAR, MessageTypes.Pass);
		}

		QAFWebElement initiateButton = new QAFExtendedWebElement(HEADING_VISAPAGE);
		NestUtils.scrollUpToElement(initiateButton);
		verifyPresent("img.attachment.visapage");
		QAFExtendedWebElement backbtn = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("label.common.vispage"),
				BACKBUTTON));
		backbtn.verifyPresent();
		QAFExtendedWebElement headingteamvisa = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("label.common.vispage"),
				HEADINGVISA));
		headingteamvisa.verifyPresent();
	}

	@QAFTestStep(description = "user clicks on filter icon")
	public void clicksOnFilter() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("ngClick.isCollapsed");
	}

	@QAFTestStep(description = "user verifies filters are {0}")
	public void launchNest(String visible) {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		if (visible.equals("visible")) {
			verifyVisible("ng-model.fromDate");
			verifyVisible("ng-model.toDate");
			verifyVisible(CommonPage.elementTextContains("Select Employee"));
			verifyVisible(VISA_REQUEST_STATUS);
			verifyVisible(VISA_SELECT_STATUS);
			verifyVisible(SELECT_COUNTRY);
			verifyVisible(DROPDOWN_SELECT_TYPE);

		} else {
			verifyNotVisible("ng-model.fromDate");
			verifyNotVisible("ng-model.toDate");
			verifyNotVisible(CommonPage.elementTextContains("Select Employee"));
			verifyNotVisible(VISA_REQUEST_STATUS);
			verifyNotVisible(VISA_SELECT_STATUS);
			verifyNotVisible(SELECT_COUNTRY);
			verifyNotVisible(DROPDOWN_SELECT_TYPE);
		}
	}

	@QAFTestStep(description = "user verifies filters are {0} for my visa page")
	public void verifyfilter(String visible) {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		if (visible.equals("visible")) {
			verifyVisible("ng-model.fromDate");
			verifyVisible("ng-model.toDate");
			verifyVisible(VISA_REQUEST_STATUS);
			verifyVisible(VISA_SELECT_STATUS);
			verifyVisible(SELECT_COUNTRY);
			verifyVisible(DROPDOWN_SELECT_TYPE);

		} else {
			verifyNotVisible("ng-model.fromDate");
			verifyNotVisible("ng-model.toDate");
			verifyNotVisible(VISA_REQUEST_STATUS);
			verifyNotVisible(VISA_SELECT_STATUS);
			verifyNotVisible(SELECT_COUNTRY);
			verifyNotVisible(DROPDOWN_SELECT_TYPE);
		}
	}
	@QAFTestStep(description = "user should verify the employee detail")
	public void verifyemployeedetail() {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement(
				CommonPage.elementTextEquals("Visa Request Details")));
		Validator.verifyThat(
				CommonStep.getText(CommonPage.elementTextEquals("Visa Request Details")),
				Matchers.containsString("Visa Request Details"));
	}

	@QAFTestStep(description = "user clicks on My Visa Requests")
	public void clickmyvisarequest() {
		click(CommonPage.elementTextEquals("My Visa Requests"));
	}

	@QAFTestStep(description = "user should verify the field formats")
	public void verifyfieldformatmyvisa() {
		String initiatedfrom = new QAFExtendedWebElement(("ng-class.invalidStartDate"))
				.getAttribute(DATE_PICKER_POPUP);
		if (initiatedfrom.contains(DATE_FORMAT)) {
			Reporter.logWithScreenShot("field format is calendar", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("field format is not calendar",

					MessageTypes.Fail);

		}
		String initiatedto = new QAFExtendedWebElement("ngClass.invalidEndDate")
				.getAttribute(DATE_PICKER_POPUP);
		if (initiatedto.contains(DATE_FORMAT)) {
			Reporter.logWithScreenShot("field format is calendar", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("field format is not calendar",

					MessageTypes.Fail);

		}

		String myvisa =
				new QAFExtendedWebElement("ng-class.settings").getAttribute("ng-click");
		if (myvisa.contains("toggleDropdown()")) {
			Reporter.logWithScreenShot(FIELD_DROPDOWN, MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("field format is not dropdown",

					MessageTypes.Fail);
		}

		verifyVisible(SEARCH_BUTTON);
		verifyVisible(CommonPage.elementTextEquals("Reset"));
		verifyPresent("pagination.mypost.publisher");
	}

	@QAFTestStep(description = "user clicks on view previous history")
	public void userClicksOnLink() {
		NestUtils.waitForLoader();
		NestUtils.scrollUpToElement(new QAFExtendedWebElement(PREVIOUS_HISTORY));
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement(PREVIOUS_HISTORY));
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "user selects visa request employee")
	public void selectEmployeeVisa() {
		NestUtils.waitForLoader();
		if (visasRequestList.size() > 0)
			visasRequestList.get(0).getEmployeeName().get(0).click();
		else Reporter.log("Visa Request List no record!", MessageTypes.Fail);
	}

	private void clickVisaRequest(int i) {
		ConfigurationManager.getBundle().setProperty("EXPECTED_EMPLOYEE",
				visasRequestList.get(i).getEmployeeName().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_COUNTRY",
				visasRequestList.get(i).getCountry().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_TYPE_OF_VISA",
				visasRequestList.get(i).getTypeOfVisa().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_INITIATED_ON",
				visasRequestList.get(i).getInitiatedOn().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_VR_STATUS",
				visasRequestList.get(i).getVisaRequestStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_VISA_STATUS",
				visasRequestList.get(i).getVisaStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("INDEX", i);
		NestUtils.waitForLoader();
		NestUtils.waitForElementToBeClickable(
				visasRequestList.get(i).getEmployeeName().get(i));
		NestUtils.clickUsingJavaScript(visasRequestList.get(i).getEmployeeName().get(i));
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "user should able to see employee under employee name")
	public void verifyemployee() {
		NestUtils.waitForLoader();
		verifyPresent(CommonPage.elementTextEquals("aaa rrrr"));
	}

	@QAFTestStep(description = "user clicks on request detail")
	public void clickrequestdetail() {
		NestUtils.waitForLoader();
		click("label.requesthistory.visarequest");
	}

	@QAFTestStep(description = "user should be able to verify the details on request detail page")
	public void verifydetails() {
		NestUtils.waitForLoader();
		verifyPresent(CommonPage.elementTextEquals("Summary View"));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, 200)");
		// NestUtils.scrollToElement("label.summaryview.visapage");
		verifyPresent("label.employeename.visapage");
		verifyPresent(CommonPage.elementTextEquals("Designation"));
		verifyPresent(CommonPage.elementTextEquals("Group"));
		verifyPresent(CommonPage.elementTextEquals("Requester"));

		verifyPresent(CommonPage.elementTextEquals("Country to Visit"));
		verifyPresent(CommonPage.elementTextEquals("Visa Type"));
		verifyPresent(CommonPage.elementTextEquals("Initiated On"));
		verifyPresent(CommonPage.elementTextEquals("Client Name"));

		// NestUtils.scrollToElement("label.detailview.vispage");
		js.executeScript("window.scrollTo(200, 300)");
		verifyPresent(CommonPage.elementTextEquals("Detail View"));
		verifyPresent(CommonPage.elementTextEquals("Visa Details"));
		js.executeScript("window.scrollTo(300, 500)");
		// NestUtils.scrollToElement("label.visadetails.visapage");
		// NestUtils.scrollToElement("label.clientdetail.visapage");
		verifyPresent(CommonPage.elementTextEquals("Client Details"));
		verifyPresent(CommonPage.buttonTextEquals("Back"));

	}

	@QAFTestStep(description = "user clicks on request history")
	public void clickrequesthistory() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		click(CommonPage.elementTextEquals("Request History"));

	}

	@QAFTestStep(description = "user should be able to see history")
	public void verifyhistory() {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement("label.history.visapage"));
		NestUtils.waitForLoader();
		verifyPresent("label.history.visapage");
	}

	@QAFTestStep(description = "{0} selects visa request employee with status {1}")
	public void selectEmployeeStatus(String usertype, String fromStatus) {
		NestUtils.waitForLoader();
		int pageindex = 0;
		NestUtils.waitForLoader();
		Boolean requestFound = false;
		for (int i = 0; i < visasRequestList.size(); i++) {
			if (usertype.equalsIgnoreCase("manager")) {
				requestFound = visasRequestList.get(i).getVisaRequestStatus().get(i)
						.getText().contains(fromStatus);
			} else {
				requestFound = visasRequestList.get(i).getVisaStatus().get(i).getText()
						.contains(fromStatus);
			}

			if (requestFound) {
				clickVisaRequest(i);
				return;
			} else {
				if (pageindex == paginations.size()) {
					break;
				}
				if ((i == visasRequestList.size() - 1)
						&& (pageindex != paginations.size())) {
					i = 0;
					waitForVisible("pagination.number.list");
					NestUtils.clickUsingJavaScript(paginations.get(pageindex));
					NestUtils.waitForLoader();
					pageindex++;
				}
			}
		}
		Reporter.log("Visa Request List no record!", MessageTypes.Fail);

	}

	@QAFTestStep(description = "veriy look and feel Visa Request Detail page")
	public void verifyLookAndFeelOfVisaRequestDetailPage() {
		rrrequest.getElement(VISA_SWITCHING, "Request Details")
				.verifyVisible("Request Details");
		rrrequest.getElement(VISA_SWITCHING, "Visa Processing Checklist")
				.verifyVisible("Visa Processing Checklist");
		rrrequest.getElement(VISA_SWITCHING, "Visa Interview Checklist")
				.verifyVisible("Visa Interview Checklist");
		rrrequest.getElement(VISA_SWITCHING, "Request History")
				.verifyVisible("Request History");

		rrrequest.getElement(VISA_SUMMARY, "VISA REQUEST STATUS")
				.verifyVisible("VISA REQUEST STATUS");
		rrrequest.getElement(VISA_SUMMARY, "Summary View").verifyVisible("SUMMARY VIEW");
		rrrequest.getElement(VISA_SUMMARY, "Detail View").verifyVisible("DETAIL VIEW");

		CommonStep.verifyVisible(EMPLOYEE_IMAGE);
		CommonStep.verifyVisible(EMPLOYEE_DESIGNATION);

		rrrequest.getElement(VISA_DETAIL, "Employee Details")
				.verifyVisible("Employee Details");
		rrrequest.getElement(VISA_DETAIL, "Client Details")
				.verifyVisible("Client Details");
		rrrequest.getElement(VISA_DETAIL, "Visa Details").verifyVisible("Visa Details");

		CommonStep.verifyVisible(PREVIOUS_HISTORY);
		rrrequest.getElement(RnRPage.BUTTON_NAME, "Approve")
				.verifyVisible("Approve button");
		rrrequest.getElement(RnRPage.BUTTON_NAME, "Reject")
				.verifyVisible("Reject button");
		rrrequest.getElement(RnRPage.BUTTON_NAME, "Back").verifyVisible("Back button");
	}

	@QAFTestStep(description = "user role {0} responds {1} request to {2} for visa request")
	public void userRoleRespondsRequestToForVisaRequest(String usertype,
			String fromStatus, String toStatus) {
		selectEmployeeStatus(usertype, fromStatus);
		NestUtils.scrollUpToElement(rrrequest.getElement(RnRPage.BUTTON_NAME, toStatus));
		NestUtils.clickUsingJavaScript(
				rrrequest.getElement(RnRPage.BUTTON_NAME, toStatus));
		writeComment(toStatus);
	}

	private void writeComment(String toStatus) {
		String comment = rrrequest.getRandomString(20);
		sendKeys(comment,
				ConfigurationManager.getBundle().getString("input.comment.visa.request"));
		rrrequest.getElement(RnRPage.BUTTON_NAME, toStatus).click();
		verifyVisible(SUCCESS_MSG);
		waitForNotVisible(SUCCESS_MSG);
		waitForNotPresent(LOADING_BAR);
	}

	@QAFTestStep(description = "verify visa status is changed from {0} to {1} for {2} field")
	public void verifyVisaStatusIsChanged(String fromStatus, String toStatus,
			String filterColumn) {
		String employee = ConfigurationManager.getBundle()
				.getProperty("EXPECTED_EMPLOYEE").toString();
		String country = ConfigurationManager.getBundle().getProperty("EXPECTED_COUNTRY")
				.toString();
		String typeOfVisa = ConfigurationManager.getBundle()
				.getProperty("EXPECTED_TYPE_OF_VISA").toString();
		String initiatedOn = ConfigurationManager.getBundle()
				.getProperty("EXPECTED_INITIATED_ON").toString();
		String visaRequestStatus = ConfigurationManager.getBundle()
				.getProperty("EXPECTED_VR_STATUS").toString();
		String visaStatus = ConfigurationManager.getBundle()
				.getProperty("EXPECTED_VISA_STATUS").toString();
		int index = Integer.parseInt(
				ConfigurationManager.getBundle().getProperty("INDEX").toString());

		Validator.verifyThat(
				"employee name verified in list!", visasRequestList.get(index)
						.getEmployeeName().get(index).getText().equals(employee),
				Matchers.equalTo(true));
		Validator
				.verifyThat(
						"country name verified in list!", visasRequestList.get(index)
								.getCountry().get(index).getText().equals(country),
						Matchers.equalTo(true));
		Validator.verifyThat(
				"type of visa name verified in list!", visasRequestList.get(index)
						.getTypeOfVisa().get(index).getText().equals(typeOfVisa),
				Matchers.equalTo(true));
		Validator
				.verifyThat("initiated on verified in list!",
						visasRequestList.get(index).getInitiatedOn().get(index).getText()
								.trim().equalsIgnoreCase(initiatedOn),
						Matchers.equalTo(true));

		if (filterColumn.equalsIgnoreCase("Visa Request Status")) {
			Validator
					.verifyThat("Visa Request Status appoved verified in list!",
							visasRequestList.get(index).getVisaRequestStatus().get(index)
									.getText().contains(toStatus),
							Matchers.equalTo(true));
		} else {
			Validator.verifyThat("Visa Request Status appoved verified in list!",
					visasRequestList.get(index).getVisaRequestStatus().get(index)
							.getText().contains(visaRequestStatus),
					Matchers.equalTo(true));
		}
		if (filterColumn.equalsIgnoreCase("Visa Status")) {
			Validator
					.verifyThat("HR: Visa Status status appoved verified in list!",
							visasRequestList.get(index).getVisaStatus().get(index)
									.getText().contains(toStatus),
							Matchers.equalTo(true));
		} else {
			if (filterColumn.equals("Visa Request Status")
					&& toStatus.equals("Rejected")) {
				visaStatus = "-";
			}
			if (filterColumn.equals("Visa Request Status")
					&& toStatus.equals("Approved")) {
				visaStatus = "In Process";
			}
			Validator.verifyThat(
					"Visa Status status verified in list!", visasRequestList.get(index)
							.getVisaStatus().get(index).getText().contains(visaStatus),
					Matchers.equalTo(true));
		}

	}

	@QAFTestStep(description = "user choose {0} from employee Dropdown")
	public void chooseDataEmployeeDropdown(String valueToSelect) {
		NestUtils.waitForLoader();
		click(EMPLOYEE_DROPDOWN);
		waitForVisible(DROPDOWN_VALUES);
		List<QAFWebElement> employee = NestUtils.getQAFWebElements(DROPDOWN_VALUES);
		for (int i = 0; i < employee.size(); i++) {
			if (employee.get(i).getText().contains(valueToSelect)) {
				employee.get(i).click();
				click(EMPLOYEE_DROPDOWN);
				break;
			}
		}
	}

	@QAFTestStep(description = "user choose {0} from visa request Dropdown")
	public void chooseDataVisaDropdown(String valueToSelect) {
		click(REQUEST_STATUS);
		waitForVisible(DROPDOWN_VALUES);
		List<QAFWebElement> visaRequest = NestUtils.getQAFWebElements(DROPDOWN_VALUES);
		for (int j = 0; j < visaRequest.size(); j++) {
			if (visaRequest.get(j).getText().contains(valueToSelect)) {
				visaRequest.get(j).click();
				click(REQUEST_STATUS);
				break;
			}
		}
	}

	@QAFTestStep(description = "user choose {0} from visa status Dropdown")
	public void chooseDataVisaStatusDropdown(String valueToSelect) {
		click(VISA_STATUS);
		waitForVisible(DROPDOWN_VALUES);
		List<QAFWebElement> visaStatus = NestUtils.getQAFWebElements(DROPDOWN_VALUES);
		for (int k = 0; k < visaStatus.size(); k++) {
			if (visaStatus.get(k).getText().contains(valueToSelect)) {
				visaStatus.get(k).click();
				click(VISA_STATUS);
				break;
			}
		}
	}

	@QAFTestStep(description = "user choose {0} from country Dropdown")
	public void chooseDataCountryDropdown(String valueToSelect) {
		click("dropdown.country.visarequests.visa");
		waitForVisible(DROPDOWN_VALUES);
		List<QAFWebElement> country = NestUtils.getQAFWebElements(DROPDOWN_VALUES);
		for (int l = 0; l < country.size(); l++) {
			if (country.get(l).getText().contains(valueToSelect)) {
				country.get(l).click();
				break;
			}
		}
	}

	@QAFTestStep(description = "user choose {0} from visa type Dropdown")
	public void chooseDataVisaTypeDropdown(String valueToSelect) {
		NestUtils.waitForLoader();
		click(VISA_TYPE);
		waitForVisible(DROPDOWN_VALUES);
		List<QAFWebElement> visaType = NestUtils.getQAFWebElements(DROPDOWN_VALUES);
		for (int k = 0; k < visaType.size(); k++) {
			if (visaType.get(k).getText().contains(valueToSelect)) {
				visaType.get(k).click();
				click(VISA_TYPE);
				break;
			}
		}
	}

	@QAFTestStep(description = "user performs action on {0} button")
	public void clickOnSearchButton(String value) {
		// QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
		// ConfigurationManager.getBundle().getString("btn.visarequests.visa"),
		// value));
		QAFExtendedWebElement element =
				new QAFExtendedWebElement(CommonPage.buttonTextContains(value));
		element.click();
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies search details")
	public void verifySearchVisaRequestContent() {
		NestUtils.waitForLoader();
		List<QAFWebElement> searchContent =
				NestUtils.getQAFWebElements("text.table.visarequests.visa");
		Reporter.log("" + searchContent.size());
		for (int i = 0; i < searchContent.size(); i++) {

			String value = searchContent.get(i).getText();

			Reporter.log("  " + i + searchContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in visarequests is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in visarequests is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user verifies dropdown default values")
	public void verifyDefaultValueDropdown() {
		NestUtils.waitForLoader();
		Validator.verifyThat("Employee name dropdown",
				new QAFExtendedWebElement("dropdown.selectemployee.visarequests.visa")
						.getText().contains(SELECT_EMPLOYEE),
				Matchers.equalTo(true));
		Validator.verifyThat("Visa Request Status dropdown",
				new QAFExtendedWebElement("dropdown.selectstatus.visarequests.visa")
						.getText().contains("Select Status"),
				Matchers.equalTo(true));
		Validator
				.verifyThat("Visa Status dropdown",
						new QAFExtendedWebElement("dropdown.visastatus.visarequests.visa")
								.getText().contains("Select Status"),
						Matchers.equalTo(true));
		Validator
				.verifyThat("Country dropdown",
						new QAFExtendedWebElement("dropdown.country.visarequests.visa")
								.getText().contains("Select Country"),
						Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user performs action on {0} employee name")
	public void clickOnEmployeeName(String value) {
		value = value.trim();
		NestUtils.waitForLoader();
		try {
			if (new QAFExtendedWebElement("text.table.visarequests.visa").isDisplayed()) {
				List<QAFWebElement> tableData =
						NestUtils.getQAFWebElements("text.table.visarequests.visa");
				if (value == null || value.isEmpty())
					tableData.get(0).click();
				else {
					for (int k = 0; k < tableData.size(); k++) {
						if (tableData.get(k).getText().contains(value)) {
							tableData.get(k).click();
							NestUtils.waitForLoader();
							break;
						}
					}
				}
			} else
				Reporter.log("Element is not visible, so no action performaed",
						MessageTypes.Fail);

		} catch (Exception e) {
			Reporter.log("No data found to perform this operation", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user navigates to visa requests detail page")
	public void verifyNavigationVisaRequests() {
		NestUtils.waitForLoader();
		Validator.verifyThat("User is redirected to visa request detail page!",
				driver.getCurrentUrl().contains("visarequestdetails"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify pagination for team visa requests page for {0}")
	public void clickOnPageNumberVisaRequests(String employee) {
		NestUtils.waitForLoader();
		try {
			if (new QAFExtendedWebElement("lnk.nextpage.travelrequestpage").isEnabled()) {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript(SCROLL_TO);
				NestUtils.waitForLoader();
				click("lnk.nextpage.travelrequestpage");
				Reporter.logWithScreenShot("Pagination successful", MessageTypes.Pass);

			} else {
				// Reporter.log(
				// "As multiple pages are not avaialbale.Creating testdata for the same");
				// addNewTeamVisaRequests(employee);
				//
				// if (new
				// QAFExtendedWebElement("lnk.nextpage.travelrequestpage").isEnabled()) {
				// JavascriptExecutor jse = (JavascriptExecutor) driver;
				// jse.executeScript(SCROLL_TO);
				// NestUtils.waitForLoader();
				// click("lnk.nextpage.travelrequestpage");
				// Reporter.logWithScreenShot("Pagination successful", MessageTypes.Pass);
				// } else {
				// Reporter.logWithScreenShot("Pagination failed", MessageTypes.Fail);
				// }
				Reporter.log(
						"As multiple pages are not avaialbale. Not able to verify pagination properly");
			}
		} catch (Exception e) {
			Reporter.log("There are no multiple page.");
		}
	}

	private void addNewTeamVisaRequests(String employee) {
		for (int i = 0; i < 25; i++) {
			initiateVisaRequest(employee);
		}

	}

	@QAFTestStep(description = "user selects {0} tab on visa request page")
	public void userSelectsTabOnVisaPage(String tab) {
		rrrequest.getElement(VISA_SWITCHING, tab).waitForVisible();
		NestUtils.clickUsingJavaScript(rrrequest.getElement(VISA_SWITCHING, tab));
	}

	@QAFTestStep(description = "verify user is redirected to tab {0}")
	public void userIsRedirectedToTab(String tabName) {
		Validator.verifyThat(
				"Active tab is " + tabName, rrrequest.getElement(VISA_SWITCHING, tabName)
						.getAttribute("class").contains("active_tab"),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user initiates visa request for employee {0} successfully")
	public void initiateVisaRequest(String emloyeename) {
		clickNewVisaRequest("New Visa Request");
		fillVisaDetails(emloyeename);
		NestUtils.clickUsingJavaScript(
				rrrequest.getElement(RnRPage.BUTTON_NAME, "Initiate Visa Request"));
		verifyVisible(SUCCESS_MSG);
		waitForNotPresent(SUCCESS_MSG);
		NestUtils.waitForLoader();
	}

	private void fillVisaDetails(String employee) {
		NestUtils.waitForLoader();
		userSelectWhereValueIs(SELECT_EMPLOYEE, employee);
		userSelectWhereValueIs("Select Visiting Country", "United States");
		userSelectWhereValueIs("Select Visa Type", "H1");
		userSelectWhereValueIs("Select Current Status", "H4");
		selectDateVisaTobeReady();
		selectDateTentativeDate();
		InitiateVisaRequestBean iniateBean = new InitiateVisaRequestBean();
		iniateBean.fillRandomData();
		iniateBean.fillUiElements();
	}

	private void selectDateTentativeDate() {
		NestUtils.clickUsingJavaScript(
				new QAFExtendedWebElement("date.picker.tentative.date"));
		String date = getDateAfterToday("to");
		NestUtils.clickUsingJavaScript(
				rrrequest.getElement("select.date", String.valueOf(date)));
	}

	private String getDateAfterToday(String fromDate) {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Calendar cal = Calendar.getInstance();
		if (fromDate.equals("from")) {
			cal.add(Calendar.DATE, 1);
		} else {
			cal.add(Calendar.DATE, 3);
		}
		Date today = cal.getTime();
		String currentDate = dateFormat.format(today);
		String dateAfterToday[] = currentDate.split("-");
		return dateAfterToday[0];
	}

	private void selectDateVisaTobeReady() {
		NestUtils.clickUsingJavaScript(
				new QAFExtendedWebElement("date.picker.visa.to.be.ready"));
		String date = getDateAfterToday("from");
		NestUtils.clickUsingJavaScript(
				rrrequest.getElement("select.date", String.valueOf(date)));
		NestUtils.waitForLoader();
	}

	private List<QAFWebElement> getElements(String loc, String text) {
		return NestUtils.getQAFWebElements(
				String.format(ConfigurationManager.getBundle().getString(loc), text));
	}

	private List<QAFWebElement> getFilterOptions(String filterLabel) {
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString("dropdown.values"),
				filterLabel));
		filterValues = getElements("dropdown.values", filterLabel);
		return filterValues;
	}

	private void userSelectWhereValueIs(String filterLabel, String filterValue) {
		rrrequest.getElement("dropdown.menu", filterLabel).waitForVisible();
		NestUtils.waitForLoader();
		NestUtils
				.clickUsingJavaScript(rrrequest.getElement("dropdown.menu", filterLabel));
		NestUtils.waitForLoader();
		List<QAFWebElement> filterValues = getFilterOptions(filterValue);
		for (QAFWebElement filter : filterValues) {
			String username = filterValue.replace(".", " ");
			NestUtils.scrollUpToElement(filter);
			if (filter.getText().toString().equals(username)) {
				if (filterLabel.equalsIgnoreCase(SELECT_EMPLOYEE)
						|| filterLabel.equalsIgnoreCase("Select Visiting Country")) {
					waitForVisible("input.search.box");
					sendKeys(username, "input.search.box");
				}
				NestUtils.clickUsingJavaScript(filter);
				NestUtils.waitForLoader();
				return;
			}
		}
		Reporter.log("No values found in dropdown:" + filterLabel, MessageTypes.Fail);
	}

	@QAFTestStep(description = "verify user is redirected to previous visa history Page")
	public void verifyUserIsRedirectedToPreviousVisaHistory() {
		NestUtils.waitForLoader();
		Validator.verifyThat("User is redirected to view previous visa history page!",
				driver.getCurrentUrl().equals(
						"http://10.12.40.86/nest_security_testing/#/previousvisahistory"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies history details of visa activities")
	public void verifiesHistoryDetailsOfVisaActivities() {
		verifyVisible("request.initated.label");
		verifyVisible("request.history.date");
		verifyVisible("request.history.time");
	}

	@QAFTestStep(description = "user performs action on button {0}")
	public void clickOnButtonOnVisaRequests(String value) {
		// QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
		// ConfigurationManager.getBundle().getString("button.visarequests.visa"),
		// value));

		QAFExtendedWebElement element =
				new QAFExtendedWebElement(CommonPage.elementTextContains(value));
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user verifies UI elements of visa request history page")
	public void verifyUIElemeentsPreviousVisaRequestHistoryPage() {
		if (CommonStep.getText("visa.request.status.description").toString()
				.equalsIgnoreCase("Approved")) {
			verifyVisible("visa.request.attachments");
		} else {
			Validator.verifyThat("Color of approval icon verified!",
					new QAFExtendedWebElement("approval.icon").getCssValue("color")
							.equalsIgnoreCase(BACKGROUND_COLOR),
					Matchers.equalTo(true));
		}
		verifyVisible("visa.image");
		verifyVisible("approval.icon");
		rrrequest.getElement(RnRPage.BUTTON_NAME, "Back").verifyVisible("Back button");
	}

	@QAFTestStep(description = "user clicks on view details link")
	public void viewVisaDetails() {
		NestUtils.waitForLoader();
		QAFWebElement element = NestUtils
				.getQAFWebElements("row.visa.requests.myvisarequestspage").get(0);
		if (element.getText().contains("No Data Found")) {
			Reporter.log("No records found to verify details.", MessageTypes.Warn);
			return;
		}
		VisaRequestsComponent tableData = new VisaRequestsComponent(element);
		tableData.getAction().click();
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "verify user is redirected to visa request detail page")
	public void verifyUserIsRedirectedToVisaRequestDetailPage() {
		NestUtils.waitForLoader();
		Validator.verifyThat("User is redirected to visa request detail page!",
				driver.getCurrentUrl().equals(
						"http://10.12.40.86/nest_security_testing/#/authorityvisarequestdetails"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user should be able to see default values")
	public void verifyresetbutton() {
		NestUtils.waitForLoader();
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		// new QAFExtendedWebElement("dropdown.select.type.of.visa").verifyNotPresent();
		String country = new QAFExtendedWebElement("dropdown.select.country").getText();
		if (country.contains("Select Country")) {
			Reporter.logWithScreenShot("reset button is working", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("reset button is not working", MessageTypes.Fail);
		}
	}
	@QAFTestStep(description = "user should be able to edit the detail of employee using {0}")
	public void verifyeditbutton(String onsitemana) {
		new QAFExtendedWebElement("ng-class.visaTypeFlag").isEnabled();
		new QAFExtendedWebElement("ng-class.onsiteManagerFlag").isEnabled();
		new QAFExtendedWebElement("ng-class.offsiteManagerFlag").isEnabled();
		new QAFExtendedWebElement("input.txt.duration.number").isEnabled();
		new QAFExtendedWebElement("input.txt.under.graduate.degree").isEnabled();
		new QAFExtendedWebElement("input.txt.master.degree").isEnabled();
		new QAFExtendedWebElement("input.txt.under.graduate.year").isEnabled();
		new QAFExtendedWebElement("input.txt.master.graduate.year").isEnabled();
		new QAFExtendedWebElement("input.txt.under.graduate.uni").isEnabled();
		new QAFExtendedWebElement("input.txt.master.uni").isEnabled();
		new QAFExtendedWebElement("ng-class.officeAddressFlag").isEnabled();
		new QAFExtendedWebElement("ng-class.reasonForVisaFlag").isEnabled();
		new QAFExtendedWebElement("input.txt.client.name").isEnabled();
		sendKeys(onsitemana, "txtbox.onsitemana.visapage");
	}
	@QAFTestStep(description = "user should be able to see edited value")
	public void verifyeditedvalue() {

	}
	@QAFTestStep(description = "user should be able to verify the details using {0}")
	public void verifydetailinitiatevisa(String loc) {
		QAFExtendedWebElement element =
				new QAFExtendedWebElement("row.initiatevisaemployee.teamvisa");
		NestUtils.waitForElementToBeClickable(element);
		NestUtils.clickUsingJavaScript("row.initiatevisaemployee.teamvisa");
		String location = new QAFExtendedWebElement("label.location.teamvisa").getText();
		if (location.contains(loc)) {
			Reporter.logWithScreenShot("Employee detail is correct", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Employee detail is not correct",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see configure visa type page")
	public void verifyuiconfigurevisatype() {
		Validator.verifyThat(
				CommonStep.getText(CommonPage.elementTextEquals("Country Name")),
				Matchers.containsString("Country Name"));
	}

	@QAFTestStep(description = "user verifies field format of my visa details page")
	public void verifyVisaDetailsPage() {
		verifyFieldsOnMyVisaRequests(switchingTabs, ConfigurationManager.getBundle()
				.getString("switching.tabs.in.visa.requests"));
		verifyFieldsOnMyVisaRequests(sectionHeaders, ConfigurationManager.getBundle()
				.getString("sections.in.visa.request.details"));
		verifyFieldsOnMyVisaRequests(subSectionHeaders, ConfigurationManager.getBundle()
				.getString("sub.sections.in.visa.request.details"));
		verifyPresent(EMPLOYEE_IMAGE);
		verifyPresent(EMPLOYEE_DESIGNATION);
		verifyPresent("link.previous.history.myvisarequestspage");
		verifyPresent("button.save.myvisarequestspage");
		verifyPresent("button.back.myvisarequestspage");
	}

	@QAFTestStep(description = "user searches for visa request")
	public void searchMyVisaRequests() {
		click(REQUEST_STATUS);
		selectOption("list.options.request.status.myvisarequestspage",
				ConfigurationManager.getBundle().getString("request.status")).click();

		click(VISA_STATUS);
		selectOption("list.options.request.status.myvisarequestspage",
				ConfigurationManager.getBundle().getString("visa.status")).click();
		click(SEARCH_BUTTON);
		NestUtils.waitForLoader();

	}

	public QAFWebElement selectOption(String loc, String selOption) {
		QAFWebElement element = null;
		for (QAFWebElement tab : NestUtils.getQAFWebElements(loc)) {
			if (tab.getText().trim().toLowerCase()
					.contentEquals(selOption.toLowerCase())) {
				element = tab;
				break;
			}
		}
		return element;
	}

	@QAFTestStep(description = "user verify Colors should be according to the theme of the application")
	public void userCheckColor() {

		NestUtils.waitForLoader();

		Validator.verifyThat("buttons color",
				new QAFExtendedWebElement(CommonPage.buttonTextContains("Search"))
						.getCssValue("background-color").contains("rgba(235, 114, 3, 1)"),

				Matchers.equalTo(true));

		Validator.verifyThat("buttons color",
				new QAFExtendedWebElement(
						CommonPage.buttonTextContains("Initiate Visa Travel Request"))
								.getCssValue("background-color")
								.contains("rgba(235, 114, 3, 1)"),

				Matchers.equalTo(true));

		Reporter.logWithScreenShot("all field display in orange color");

	}

	public void verifyFieldsOnMyVisaRequests(List<QAFWebElement> elements,
			String fields) {
		String[] arrTabs = fields.split(":");
		for (String field : arrTabs) {
			boolean status = false;
			for (QAFWebElement tab : elements) {
				if (tab.getText().trim().contains(field.trim())) {
					NestUtils.scrollToViewElement((QAFExtendedWebElement) tab);
					Reporter.log(field + " is found.", MessageTypes.Pass);
					status = true;
					break;
				}
			}
			if (!status)
				Reporter.log(field + " is not found.", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on add visa type icon")
	public void userClickOnAddVisaTypeIcon() {
		List<QAFWebElement> addVisaTypeList =
				NestUtils.getQAFWebElements("list.AddVisaType.icon.visa");
		addVisaTypeList.get(0).click();
	}

	@QAFTestStep(description = "user should verify logo and footer")
	public void userShouldVerifyLogoAndFooter() {
		Validator.verifyThat("Logo is present", CommonStep.verifyPresent("img.logo.visa"),
				Matchers.equalTo(true));

		Validator.verifyThat("Footer is present",
				CommonStep.getText("text.footer.visa").equalsIgnoreCase(FOOTER_TEXT),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user verify search result for visa status as {0}")
	public void userVerifySearchResultForVisaStatusAs(String str0) {
		NestUtils.waitForLoader();
		try {
			List<QAFWebElement> totalStatus =
					NestUtils.getQAFWebElements("list.visaStatus.teamvisaRequestPage");
			for (int i = 0; i < totalStatus.size(); i++) {

				if (totalStatus.get(i).isDisplayed()) {
					if (!totalStatus.get(i).getText().equalsIgnoreCase(str0)) {
						Reporter.logWithScreenShot("Search result is incorrect.Actual: "
								+ totalStatus.get(i).getText() + "Expected: " + str0,
								MessageTypes.Fail);
					} else {
						Reporter.logWithScreenShot("Search result is correct.Actual: "
								+ totalStatus.get(i).getText() + "Expected: " + str0,
								MessageTypes.Pass);
					}
				}
			}
		} catch (Exception e) {
			Reporter.log("No data found", MessageTypes.Warn);
		}
	}
	@QAFTestStep(description = "user should see {0} fields as disabled for {1}")
	public void verifyDisabledFields(String fields, String loc) {
		String[] fieldList = fields.split(",");

		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(
					getWebElementFromList(fieldList[i], loc).verifyDisabled(),
					Matchers.equalTo(true));
		}

	}
	@QAFTestStep(description = "user should verify the {0} fields format for {1}")
	public void verifyformat(String field, String locator) {

	}

	public QAFWebElement getWebElementFromList(String generalizedLocator,
			String fieldToCheck) {

		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(generalizedLocator),
				fieldToCheck));

		return element;
	}

	@QAFTestStep(description = "user should see visa details page with colors according to the theme of the application")
	public void verifyUI() {
		String color = CommonPage
				.getWebElementFromList("label.month.calendarcomponent", "Search")
				.getCssValue("background-color");
		Reporter.log(color);
		Validator.verifyThat("rgba(235, 114, 3, 1)", Matchers.containsString(color));
	}

	@QAFTestStep(description = "user should see {0} page with {1}")
	public void verifyNewVisaRequestpage(String heading, String employee) {
		NestUtils.waitForLoader();
		/*
		 * QAFExtendedWebElement head = new QAFExtendedWebElement(String.format(
		 * ConfigurationManager.getBundle().getString("label.common.vispage"),
		 * heading));
		 */
		QAFWebElement initiate = new QAFExtendedWebElement(HEADING_VISAPAGE);
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, -250)");
		initiate.verifyPresent();
		QAFWebElement emp = new QAFExtendedWebElement(EMP_NAME);
		/*
		 * QAFExtendedWebElement emp = new QAFExtendedWebElement(String.format(
		 * ConfigurationManager.getBundle().getString("label.common.vispage"),
		 * employee));
		 */
		emp.verifyPresent();
	}
	@QAFTestStep(description = "user should be able to redirect to visa request page")
	public void verifybackbutton() {
		QAFWebElement verification = new QAFExtendedWebElement(HEADING_VISAPAGE);
		verification.verifyPresent();
	}

	@QAFTestStep(description = "user verifies visa request history status")
	public void visaHistory() {
		NestUtils.waitForLoader();
		List<QAFWebElement> historyActions =
				NestUtils.getQAFWebElements("list.visa.history.visapage");
		for (QAFWebElement historyAction : historyActions) {
			VisaHistoryStatus historyObject = new VisaHistoryStatus(historyAction);
			historyObject.getHistoryTime().verifyVisible();
			historyObject.getHistoryDate().verifyVisible();
			historyObject.getHistoryStatus().verifyVisible();
		}
	}

	public class VisaHistoryStatus {
		QAFExtendedWebElement parent;
		public VisaHistoryStatus(QAFWebElement parentElement) {
			parent = (QAFExtendedWebElement) parentElement;
		}
		public VisaHistoryStatus(QAFExtendedWebElement parentElement) {
			parent = parentElement;
		}

		public QAFWebElement getHistoryTime() {
			return new QAFExtendedWebElement(parent, "label.time.visa.history.visapage");
		}

		public QAFWebElement getHistoryDate() {
			return new QAFExtendedWebElement(parent, "label.date.visa.history.visapage");
		}

		public QAFWebElement getHistoryStatus() {
			return new QAFExtendedWebElement(parent,
					"label.status.visa.history.visapage");
		}
	}

	// development in progress
	@QAFTestStep(description = "user verifies {0} button on page")
	public void verifyActions(String buttonName) {
		switch (buttonName.trim().toLowerCase()) {
			case "save" :
				break;
			case "edit" :
				break;
			case "back" :
				break;
			default :
				Reporter.logWithScreenShot("invalid option, please select ");
		}
	}
}
