package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.nest.beans.CreateNewPostBean;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class PublisherPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
	}
	public static final String HEADTITLE = "titel.mypreferences.post.publisher";
	public static final String BREADCRUMB_HEAD = "breadcrumb.mypost.publisher";
	public static final String BUTTON_NOT_PRESENT =
			"button.search.reset.mypost.publisher";
	public static final String HTML_COMMON = "html.common";
	public static final String SUBMIT_BUTTON = CommonPage.buttonTextContains("Submit");
	public static final String BACK_BUTTON = CommonPage.buttonTextContains("Back");
	public static final String CATEGORY_DROPDOWN =
			CommonPage.buttonTextContains("Select");

	@QAFTestStep(description = "user clicks on create newpost")
	public void clicksCreateNewpost() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("btn.createNewPost.publisher");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify Titel of the page should {0}")
	public void verifyTitelNewpost(String titel) {

		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		Reporter.log(currentUrl);
		if (currentUrl.contains(titel)) {
			Reporter.logWithScreenShot("verify Titel of the page is verified",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("verify Titel of the page is not verified",
					MessageTypes.Fail);
		}
	}

	public void verifyBreadcrumbs(String loc, String breadcrumb) {
		NestUtils.waitForLoader();
		boolean flag = CommonPage.verifyLocContainsText(loc, breadcrumb);
		if (flag) {
			Reporter.logWithScreenShot("Breadcrumb is displayed", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Breadcrumb is not displayed", MessageTypes.Fail);
		}
	}
	public List<QAFWebElement> allTextField() {
		return NestUtils.getQAFWebElements("textField.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify All the given Text field")
	public void verifyTextField() {
		for (int i = 0; i < allTextField().size(); i++) {
			allTextField().get(i).verifyPresent();
		}
	}

	public List<QAFWebElement> allDropDown() {
		return NestUtils.getQAFWebElements("dropdown.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify {0} given below")
	public void verifyDropdowns(String value) {

		for (int i = 0; i < allDropDown().size(); i++) {
			String result = allDropDown().get(i).getAttribute("class");
			if (result.contains(value)) {
				Reporter.logWithScreenShot("Dropdown is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("Dropdown is not displayed",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "User click on submit")
	public void clickSubmit() {
		CommonPage.clickUsingJavaScript(SUBMIT_BUTTON);
		NestUtils.waitForLoader();
	}

	public List<QAFWebElement> compulsoryField() {
		return NestUtils.getQAFWebElements("compulsory.field.mypost.publisher");
	}

	@QAFTestStep(description = "User verify compulsory field display popover {0}")
	public void verifyCompulsoryFieldTooltip(String actual) {

		for (int i = 0; i < compulsoryField().size(); i++) {
			Actions toolAct = new Actions(driver);
			toolAct.moveToElement(compulsoryField().get(i)).build().perform();
			if (verifyPresent("tooltip.box.mypost.publisher")) {
				Reporter.logWithScreenShot("tool tip is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("tool tip is Not displayed",
						MessageTypes.Fail);
			}
		}
	}

	public void selectDate(String type, String loc, String date) {
		NestUtils.waitForLoader();
		waitForVisible(type, 10000);
		try {
			click(type);
		} catch (Exception e) {
			waitForVisible(type, 10000);
			click(type);
		}
		NestUtils.waitForLoader();
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		try {
			element.click();
		} catch (Exception e) {
			JavascriptExecutor executor =
					(JavascriptExecutor) new WebDriverTestBase().getDriver();
			executor.executeScript(CommonPage.ARGUMENT, element);
		}
	}

	@QAFTestStep(description = "user select from date {0} from calender")
	public void selectDateCalender(String date) {

		selectDate("calender.from.date.loc", "datepicker.loc", date);

	}

	@QAFTestStep(description = "user select to date {0} from calender")
	public void selectToDate(String date) {

		selectDate("calender.to.date.loc", "datepicker.loc", date);
	}

	@QAFTestStep(description = "verify display error message {0}")
	public void verifyErrorMessage(String msg) {

		String error = new QAFExtendedWebElement("error.msg.calender.mypost.publisher")
				.getText();
		if (error.contains(msg)) {
			Reporter.logWithScreenShot("error msg display ", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("error msg is Not display ", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user navigate to My Preference Page")
	public void navigateMyPreferencePage() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("btn.mypreferences.post.publisher");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify titel {0} of the page")
	public void verifyTitelMyPreference(String titel) {
		verifyTitelOfPage(HEADTITLE, titel);
	}
	@QAFTestStep(description = "user verify Breadcrumb of {0} page")
	public void verifyBreadcrumbMypreferencesPage(String breadcrumb) {
		NestUtils.waitForLoader();
		verifyBreadcrumbs(HEADTITLE, breadcrumb);
	}

	@QAFTestStep(description = "user verify Privacy button should be present on right uper side of page")
	public void verifyPrivacyButton() {
		Validator.verifyThat("Privacy button is present",
				verifyPresent("btn.privecy.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify slider button should be present")
	public void verifySliderButton() {
		Validator.verifyThat("Slider button is present",
				verifyPresent("btn.slider.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify all platform should be present")
	public void verifyAllPlatform() {
		for (int i = 0; i < allPlatform().size(); i++) {
			allPlatform().get(i).verifyPresent();
		}
	}
	public List<QAFWebElement> allPlatform() {
		return NestUtils.getQAFWebElements("btn.dropdown.mypreference.post.publisher");
	}

	@QAFTestStep(description = "user select linkedin checkbox")
	public void selectLinkedin() {
		CommonPage
				.clickUsingJavaScript("checkedbox.linkedin.mypreferences.post.publisher");
	}

	@QAFTestStep(description = "user verify Buttons should be there")
	public void verifyButtonsOnMypreferencesPage() {
		verifyPresent(CommonPage.buttonTextContains("Save"));
		verifyPresent(BACK_BUTTON);
	}

	@QAFTestStep(description = "user click on privecy button")
	public void clickPrivecyButton() {
		CommonPage.clickUsingJavaScript("btn.privecy.mypreferences.post.publisher");
	}

	@QAFTestStep(description = "user verify the {0} Message")
	public void verifyAlertMessage(String message) {
		verifyPresent("privecy.alert.message.post.publisher");
		boolean flag = CommonPage
				.verifyLocContainsText("privecy.alert.message.post.publisher", message);

		if (flag) {
			Reporter.logWithScreenShot("Privecy Alert is displayed ", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Privecy Alert is not displayed",
					MessageTypes.Fail);
		}
	}
	public void verifyTitelOfPage(String loc, String titel) {
		NestUtils.waitForLoader();
		boolean flag = CommonPage.verifyLocContainsText(loc, titel);

		if (flag) {
			Reporter.logWithScreenShot("Titel is verified", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("titel is not verified", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify titel of MY Post {0} page")
	public void verifyTitelMYPostPage(String title) {

		verifyTitelOfPage(HEADTITLE, title);
	}

	@QAFTestStep(description = "user verify titel of Statistics {0} page")
	public void verifyTitelStatisticsPage(String title) {

		verifyTitelOfPage(HEADTITLE, title);
	}

	@QAFTestStep(description = "user verify breadcrump of Mypost page {0}")
	public void verifyBreadcrumpMypostPage(String breadCrumb) {

		verifyBreadcrumbs(HEADTITLE, breadCrumb);
	}

	@QAFTestStep(description = "user verify Pagination should be present")
	public void verifyPaginationMyPostPage() {
		Validator.verifyThat("Pagination is present",
				verifyPresent("pagination.mypost.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify filter icon should be on right uper side of page")
	public void verifyFilterIcon() {
		Validator.verifyThat("Filter icon is present",
				verifyPresent("btn.filter.mypost.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify table column header")
	public void verifyTableHeader() {
		for (int i = 0; i < tableHeader().size(); i++) {
			tableHeader().get(i).verifyPresent();
		}
		Validator.verifyThat("Title must be : ", tableHeader().get(0).getText(),
				Matchers.equalToIgnoringCase("Title"));
		Validator.verifyThat("Category must be : ", tableHeader().get(1).getText(),
				Matchers.equalToIgnoringCase("Category"));
		Validator.verifyThat("Posted On must be : ", tableHeader().get(2).getText(),
				Matchers.equalToIgnoringCase("Posted On"));
		Validator.verifyThat("Action must be : ", tableHeader().get(3).getText(),
				Matchers.equalToIgnoringCase("Action"));
	}
	public List<QAFWebElement> tableHeader() {
		return NestUtils.getQAFWebElements("table.header.mypost.publish");
	}

	@QAFTestStep(description = "user verify Action column should have edit and delete icons")
	public void verifyButtonEditAndDelete() {
		Validator.verifyThat("Edit icon is present",
				verifyPresent("btn.edit.action.mypost.publisher"),
				Matchers.equalTo(true));
		Validator.verifyThat("Delete icon is present",
				verifyPresent("btn.delete.action.mypost.publisher"),
				Matchers.equalTo(true));
	}

	public void selectFromList(String locOfDropdown, String locOfList, String option) {
		waitForVisible(locOfDropdown);
		CommonPage.clickUsingJavaScript(locOfDropdown);
		List<QAFWebElement> list = NestUtils.getQAFWebElements(locOfList);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().contains(option)) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript(CommonPage.ARGUMENT, list.get(i));
				break;
			}
		}
	}

	@QAFTestStep(description = "user select {0} from catagory Dropdown")
	public void selectDataCatagoryDropdown(String valueToSelect) {
		selectFromList(CATEGORY_DROPDOWN, "catagory.dropdown.newpost.publisher",
				valueToSelect);
	}

	@QAFTestStep(description = "verify date picker should be displayed")
	public void verifyDatePicker() {
		Validator.verifyThat("calender Start Date is present",
				verifyPresent(CommonPage.divTextEquals("Start Date")),
				Matchers.equalTo(true));
		Validator.verifyThat("calender End Date is present",
				verifyPresent(CommonPage.divTextEquals("Expiring On")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user fill {0} required details")
	public void fillRequiredDetailsNewPost(String key) {

		CreateNewPostBean post = new CreateNewPostBean();
		post.fillFromConfig(key);
		CommonPage.clickUsingJavaScript(CommonPage.inputNameContains("Title"));
		sendKeys(post.getTitle(), CommonPage.inputNameContains("Title"));
		CommonPage.clickUsingJavaScript(CommonPage.inputNameContains("postUrl"));
		sendKeys(post.getPostUrl(), CommonPage.inputNameContains("postUrl"));
		CommonPage.clickUsingJavaScript("textbox.description.newpost.publisher");
		sendKeys(post.getDescription(), "textbox.description.newpost.publisher");
	}

	@QAFTestStep(description = "user select {0} from location Dropdown")
	public void selectLocationDropdown(String valueToSelect) {
		selectFromList("btn.dropdown.location.newpost",
				"catagory.dropdown.newpost.publisher", valueToSelect);
	}

	@QAFTestStep(description = "user verify Buttons given below are present")
	public void verifyButtonPresent() {
		Validator.verifyThat("Buttons given below are present",
				verifyPresent(SUBMIT_BUTTON), Matchers.equalTo(true));
		Validator.verifyThat("Buttons given below are present",
				verifyPresent(BACK_BUTTON), Matchers.equalTo(true));
	}
	public List<QAFWebElement> searchField() {
		return NestUtils.getQAFWebElements("table.row.catagory.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify Search results should be according to selected filters")
	public void verifySearchResultsFilters() {
		boolean flag = false;
		for (int i = 0; i < searchField().size(); i++) {
			if (searchField().get(i).getText().contains("Blog")) {
				flag = true;
			} else {
				flag = false;
				break;
			}
		}
		if (flag) {
			Reporter.logWithScreenShot(
					"Search results should be according to selected filters",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Search results should Not according to selected filters",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on {0}")
	public void clickOnButton(String btn) {
		QAFExtendedWebElement element =
				new QAFExtendedWebElement(CommonPage.buttonTextContains(btn));
		NestUtils.waitForLoader();
		element.waitForVisible(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript(CommonPage.ARGUMENT, element);
		NestUtils.waitForLoader();
	}
	@QAFTestStep(description = "user checks the asterisk sign in the mandatory {0} fields {1}")
	public void userChecksTheAsterisksignInTheMandatoryFields(String fieldsLocator,String fieldTexts) {

		String[] fieldList = fieldTexts.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));
		System.out.println("String Received");
		for (int i = 0; i < 2; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString(fieldsLocator),fieldList[i])).isPresent(), Matchers.equalTo(true));
		}
		Reporter.logWithScreenShot(
				"Asterisk key is present in the Mandatory fields of airline membership",
				MessageTypes.Pass);
	}
	@QAFTestStep(description = "user clicks on {0} delete button in airline membership")
	public void clicksOnDeleteButtonAirlineMemberShip(String btn)
	{
		String deleteLocator=String.format(ConfigurationManager.getBundle().getString("icons.with.title"),btn);
		click(deleteLocator);
	}
	
	@QAFTestStep(description = "user navigate to postdetails page")
	public void navigateToPostDetailsPage() {
		CommonPage.clickUsingJavaScript("title.post.table.mypost.publisher");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify title of postDetails {0} page")
	public void verifyTitlePostDetailsPage(String title) {
		NestUtils.waitForLoader();
		verifyTitelOfPage(BREADCRUMB_HEAD, title);
	}

	@QAFTestStep(description = "user verify breadcrump of post details page {0}")
	public void verifyBreadcrumpOfPostDetailsPage(String breadCrumb) {
		verifyBreadcrumbs(HEADTITLE, breadCrumb);
	}

	@QAFTestStep(description = "user verify post details page")
	public void verifyPostDetailsPage() {
		List<QAFWebElement> list =
				NestUtils.getQAFWebElements("postdetails.all.data.publisher");
		for (QAFWebElement element : list) {
			element.verifyPresent();
		}
	}

	@QAFTestStep(description = "user verify Edit details page having filled all details")
	public void verifyRediractionAndAllField() {

		List<QAFWebElement> list =
				NestUtils.getQAFWebElements("textField.list.mypost.publisher");
		for (QAFWebElement element : list) {
			if (!element.getAttribute("value").isEmpty()) {
				Reporter.logWithScreenShot(element.getAttribute("value"),
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"Filled details are not displayed in edit post details page",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user verify its redirected to the Edit details page")
	public void verifyRediractionToEditDetailsPage() {
		NestUtils.waitForLoader();
		verifyTitelOfPage(BREADCRUMB_HEAD, "Edit Post");
	}

	@QAFTestStep(description = "user verify Breadcrumb should be {0}")
	public void verifyBreadcrumbNewPost(String breadcrumb) {
		verifyBreadcrumbs(BREADCRUMB_HEAD, breadcrumb);
	}

	@QAFTestStep(description = "user click on any checkbox in postpage")
	public void clickCheckBoxPostPage() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("checkbox.post.publisher");
	}

	@QAFTestStep(description = "user verify All the platforms button should be enabled")
	public void verifyPlatformsButton() {
		Validator.verifyThat("Platforms Buttons are Enabled",
				verifyEnabled("btn.platform.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify lable of datepicker")
	public void verifyDatepickers() {
		Validator.verifyThat("To Date datePicker", verifyEnabled("calender.to.date.loc"),
				Matchers.equalTo(true));
		Validator.verifyThat("From Date datePicker",
				verifyEnabled("calender.from.date.loc"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify Check box for Published post")
	public void verifyCheckBoxPublishedPost() {
		Validator.verifyThat("Check box for Published is present",
				verifyPresent("checkbox.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify platform icon should be present")
	public void verifyPlatformIcon() {
		Validator.verifyThat("platform icon is present",
				verifyPresent("btn.platform.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify Social media column have icon of all platforms")
	public void verifySocialMediaColumnIcon() {
		List<QAFWebElement> list =
				NestUtils.getQAFWebElements("icon.table.post.publisher");
		for (QAFWebElement element : list) {
			Validator.verifyThat("Social Media icon is present", element.verifyPresent(),
					Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify every row in table has check box")
	public void verifyRowCheckBox() {
		List<QAFWebElement> listRow =
				NestUtils.getQAFWebElements("table.row.post.publisher");
		List<QAFWebElement> listCheckBox =
				NestUtils.getQAFWebElements("checkbox.table.list.post.publisher");
		if (listRow.size() == listCheckBox.size()) {
			Reporter.logWithScreenShot("checkBox are display for each Row",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("checkBox are NOT display for each Row",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify all the header columns of the table")
	public void verifyHeaderColumnsOfTable() {
		List<QAFWebElement> tableHeader =
				NestUtils.getQAFWebElements("table.header.post.publisher");
		Validator.verifyThat("Header of Title must be : ", tableHeader.get(0).getText(),
				Matchers.equalToIgnoringCase("Title"));
		Validator.verifyThat("Header of Category must be : ",
				tableHeader.get(1).getText(), Matchers.equalToIgnoringCase("Category"));
		Validator.verifyThat("Header of Posted On must be : ",
				tableHeader.get(2).getText(), Matchers.equalToIgnoringCase("Posted On"));
		Validator.verifyThat("Header of Social Media must be : ",
				tableHeader.get(3).getText(),
				Matchers.equalToIgnoringCase("Social Media"));
		Validator.verifyThat("Header of Status must be : ", tableHeader.get(4).getText(),
				Matchers.equalToIgnoringCase("Status"));
	}

	@QAFTestStep(description = "user verify breadcrump of post page {0}")
	public void verifyBreadcrumbPostPage(String breadCrumb) {
		verifyBreadcrumbs(BREADCRUMB_HEAD, breadCrumb);
	}

	@QAFTestStep(description = "user verify title of post {0} page")
	public void verifyTitlePostPage(String title) {
		NestUtils.waitForLoader();
		verifyTitelOfPage(BREADCRUMB_HEAD, title);
	}

	@QAFTestStep(description = "user verify mypreference button")
	public void verifyMyPreferenceButton() {
		Validator.verifyThat("mypreference button is present",
				verifyPresent("btn.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify {0} {1} given below")
	public void verifyDropdown(String type, String value) {
		switch (value) {
			case "dropdown" :
				QAFExtendedWebElement element1 =
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("dropdown.all.publisher"), value));
				String result = element1.getAttribute("class");
				if (result.contains(value)) {
					Reporter.logWithScreenShot(type + "Dropdown is displayed",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(type + "Dropdown is not displayed",
							MessageTypes.Fail);
				}

				break;
			case "Textbox" :
				QAFExtendedWebElement element =
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("textbox.all.publisher"), type));
				String attribute = element.getAttribute("placeholder");
				if (attribute.contains(type)) {
					Reporter.logWithScreenShot(value + " is displayed",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(value + " is not displayed",
							MessageTypes.Fail);
				}
				break;
			default :
				Reporter.logWithScreenShot("This is neither a Textbox nor a Dropdown");
		}
	}

	@QAFTestStep(description = "user performs action on filter icon")
	public void clickOnFilterIcon() {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("btn.filter.mypost.publisher");
	}

	@QAFTestStep(description = "user verify filter options are not visible")
	public void verifyFilterOptions() {
		List<QAFWebElement> calenderOption =
				NestUtils.getQAFWebElements("filter.calender.mypost.publisher");
		for (int i = 0; i < calenderOption.size(); i++) {
			Validator.verifyThat("filter options are not visible",
					calenderOption.get(i).verifyNotVisible(), Matchers.equalTo(true));
		}
		List<QAFWebElement> button = NestUtils.getQAFWebElements(BUTTON_NOT_PRESENT);
		for (int j = 0; j < button.size(); j++) {
			Validator.verifyThat("search and reset buttons are not visible",
					button.get(j).verifyNotVisible(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify posts count")
	public void verifyPostMadePostPendingCount() {
		List<QAFWebElement> postCount =
				NestUtils.getQAFWebElements("count.postsmade.statistics.publisher");
		for (QAFWebElement count : postCount) {
			Validator.verifyThat("Posts Made and Posts pending count are present",
					count.verifyPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify filter options are not present")
	public void verifyFilters() {
		List<QAFWebElement> calenderOption =
				NestUtils.getQAFWebElements("filter.calender.mypost.publisher");
		for (int i = 0; i < calenderOption.size(); i++) {
			Validator.verifyThat("filter options are not visible",
					calenderOption.get(i).verifyNotPresent(), Matchers.equalTo(true));
		}
		List<QAFWebElement> button = NestUtils.getQAFWebElements(BUTTON_NOT_PRESENT);
		for (int j = 0; j < button.size(); j++) {
			Validator.verifyThat("search and reset buttons are not visible",
					button.get(j).verifyNotPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify {0} Button")
	public void verifyAllButton(String btn) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("button.contains.loc.common"),
				btn));
		element.waitForVisible(5000);
		Validator.verifyThat("Button is present", element.verifyPresent(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify asterick sign for {0} mandatory Fields")
	public void verifyMandatoryFieldsAsterickSign(String mandatoryFields) {

		String[] fieldList = mandatoryFields.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));
		for (int i = 0; i < fieldList.length; i++) {
			Validator.verifyThat(new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("cmmon.asterick.sign"),
					fieldList[i])).isPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify data should not save after click on back button")
	public void verifyData() {
		boolean flag = CommonPage.verifyLocContainsText(
				"title.post.table.mypost.publisher", "NoRequirement");
		if (flag) {
			Reporter.logWithScreenShot("data is present ", MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot("data is not present", MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user verify navigated to the page according to the Pagination number")
	public void verifyPaginationNumber() {
		List<QAFWebElement> paginationNumber =
				NestUtils.getQAFWebElements("common.pagination.number.list");
		CommonPage.scrollUpToElement("common.pagination.number.list");
		for (int i = 0; i < paginationNumber.size() - 1; i++) {
			List<QAFWebElement> tablerow =
					NestUtils.getQAFWebElements("common.table.row.list");
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript(CommonPage.ARGUMENT, paginationNumber.get(i));
			if (tablerow.size() == 10) {
				Reporter.logWithScreenShot("user navigate to page "
						+ paginationNumber.get(i).getText() + "according to page number",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("user not navigate to page "
						+ paginationNumber.get(i).getText() + "according to page number",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user verify fields are empty after Reset")
	public void verifyFieldIsEmpty() {
		boolean flag = CommonPage.verifyLocContainsText("btn.dropdown.catagory.newpost",
				"Select");
		if (flag) {
			Reporter.logWithScreenShot("field is Empty ", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("field is NOT Empty", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify titel of Statistics on page")
	public void verifyTitelStatistics() {

		Validator.verifyThat("Statistics text",
				verifyPresent("btn.privecy.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user click post from table")
	public void clickPosts() {

		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("table.post.publisher");
	}

	@QAFTestStep(description = "user fill required details for statistics")
	public void fillStatistics() {

		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript("keyword.textbox.publisher");
		sendKeys("test", "keyword.textbox.publisher");
		CommonPage.clickUsingJavaScript("dropdown.button.publisher");
	}

	@QAFTestStep(description = "user click count link on statistics")
	public void countLink(String linkname) {

		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript("countlink.statistics.publisher");
		Reporter.logWithScreenShot("link is display and click");
	}

	@QAFTestStep(description = "user click on count of {0}")
	public void userClickOnCount(String lable) {
		CommonPage.getWebElementFromList("link.count.statistics.publisher", lable)
				.click();
	}

	@QAFTestStep(description = "user should verify {0} dropdown")
	public void userShouldVerifyDropdown(String lableOfDropdown) {
		verifyPresent(CommonPage.getStringLocator("dropdown.statistic.publisher",
				lableOfDropdown));
		click(CommonPage.getStringLocator("dropdown.statistic.publisher",
				lableOfDropdown));
		CommonStep.verifyPresent("List.countryInLocation.publisher");

	}
	@QAFTestStep(description = "user verify hide icon")
	public void userVerifyHideIcon() {
		verifyPresent("icon.hide.publisher");
		click("icon.hide.publisher");
		CommonStep.verifyNotPresent(
				CommonPage.getStringLocator("dropdown.statistic.publisher", "Location"));
	}
	@QAFTestStep(description = "user edit title")
	public void userEditTitle() {
		sendKeys("Edited","txtBox.title.myPost.publisher");
	}

}
