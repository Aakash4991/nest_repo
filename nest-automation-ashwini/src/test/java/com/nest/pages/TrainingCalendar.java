package com.nest.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

import org.hamcrest.Matchers;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class TrainingCalendar {

	public static final String SUBMIT_BUTTON = CommonPage.buttonTextContains("Submit");
	public static final String ATTENDANCE_BTN = CommonPage.buttonTextContains("Submit");
	PublisherPage publisherpage = new PublisherPage();
	RR_RequestPage requests = new RR_RequestPage();

	@QAFTestStep(description = "user clicks on training he has attended previously")
	public void ClikOnPreviouslyAttendedTraining() {

		QAFExtendedWebElement priviousTraining = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("training.calendar.attendedTraining"), "abc pqr"));
		priviousTraining.click();
	}

	@QAFTestStep(description = "user clicks on {0} file button")
	public void clikOnBrowseFileButton(String value) {
		QAFWebElement browseFile = CommonPage.getWebElementFromList("div.contains.loc.common", value);
		browseFile.waitForEnabled();
		browseFile.click();

	}

	@QAFTestStep(description = "user selects file {0} to upload")
	public void userSelectsFileToUpload(String relativePath) throws AWTException {

		/* getting the systems working directory */
		String workingDir = System.getProperty("user.dir");

		/* Creating a absolute path for a file to be uploaded */
		File file = new File(workingDir + relativePath);
		String absolutePath = file.getAbsolutePath();
		Reporter.log("Absolute Path: "+absolutePath);		
		/* Copy the absolute file path in clipboard */
		StringSelection stringSelection = new StringSelection(absolutePath);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);

		/* Perform cnt+v OR Paste operation and click on upload/open */
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}

	@QAFTestStep(description = "user click on Submit button on training calender page")
	public void userClickOnButtonOnTrainingCalenderPage() throws InterruptedException {
		NestUtils.waitForLoader();
		CommonStep.waitForEnabled(SUBMIT_BUTTON);
		NestUtils.clickUsingJavaScript(SUBMIT_BUTTON);

	}

	@QAFTestStep(description = "user click on Cross icon {0} on training calender page")
	public void clickOnCrossIconOnTrainingCalenderPage(String value) {

		CommonStep.waitForEnabled(String.format(ConfigurationManager.getBundle().getString("icons.with.title"), value));

		NestUtils.clickUsingJavaScript(
				String.format(ConfigurationManager.getBundle().getString("icons.with.title"), value));

	}

	@QAFTestStep(description = "user cliks on button {0} on Training Calendar page")
	public void clickOnAttendanceButton(String value) {
		NestUtils.waitForLoader();

		QAFExtendedWebElement attendaceButton = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("button.contains.loc.common"), value));

		CommonPage.scrollUpToElement(
				String.format(ConfigurationManager.getBundle().getString("button.contains.loc.common"), value));

		attendaceButton.click();
	}

	@QAFTestStep(description = "user verify training calendar attendace Breadcrumbs should be {0}")
	public void userVerifyTrainingCalendarAttendaceBreadcrumbsShouldBe(String breadcrumb) {
		NestUtils.waitForLoader();
		publisherpage.verifyBreadcrumbs("training.calendar.attendace.breadcum", breadcrumb);
	}

	@QAFTestStep(description = "user schedules a training for {0} month")
	public void schedulesTrainingForMonth(String monthName) {

		if (monthName.equalsIgnoreCase("next")) {
			selectDate();
		}
	}

	private void selectDate() {
		
		NestUtils.scrollUpToElement(requests.getElement(RnRPage.BUTTON_NAME, "Add"));
		click("training.date");
		click("training.calender.right.month");
	}
	
	@QAFTestStep(description = "user verifies Breadcrumbs should be {0} {1}")
	public void userVerifiesBreadcrumbsShouldBe(String breadcrumb, String loc) {
		NestUtils.waitForLoader();
		publisherpage.verifyBreadcrumbs(loc, breadcrumb);
	}
	
	@QAFTestStep(description = "user verifies the UI elements {0} {1} {2} {3} {4}")
	public void userVerifiesTheUIElements(String pageText, String attributes, String loc, String text, String attribute ) 
	{
		String locator=String.format(ConfigurationManager.getBundle().getString("title.training.addtrainingvenue"),pageText,pageText);
		CommonStep.verifyPresent(locator);
		Reporter.logWithScreenShot("title of the page is present",MessageTypes.Pass);
		String[] titles=attributes.split(",");
		for(int i=0;i<titles.length;i++)
		{
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString(loc),titles[i])).isPresent(), Matchers.equalTo(true));
		}
		Reporter.logWithScreenShot("table elements are present",MessageTypes.Pass);
		Validator.verifyThat(new QAFExtendedWebElement(CommonPage.buttonTextContains(text)).isPresent(), Matchers.equalTo(true));
		Reporter.logWithScreenShot("add button is present",MessageTypes.Pass);
		Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("class.contains.common"),attribute)).isPresent(), Matchers.equalTo(true));
		Reporter.logWithScreenShot("pagination is present",MessageTypes.Pass);
		
	}

	@QAFTestStep(description = "user clicks on any upcoming training to be taken")
	public void clickOnUpcomingTraining() {
		NestUtils.clickUsingJavaScript("training.calender.time");
		NestUtils.waitForLoader();
	}
}
