package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyText;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.nest.beans.EditContactDetailsBean;
import com.nest.beans.ProfessionalExperienceBean;
import com.nest.components.CalendarComponent;
import com.nest.components.OtherRequestsComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class UserHomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	@FindBy(locator = "other.requests.component")
	private List<OtherRequestsComponent> otherRequests;

	LeavePage leave = new LeavePage();
	RR_RequestPage rrPage = new RR_RequestPage();
	CalendarComponent calendar = new CalendarComponent();
	ProfessionalExperienceBean professionalExperience = new ProfessionalExperienceBean();
	public static final String LINK_VIEWALL = "link.ViewAll.Requests.userhomepage";
	public static final String VIEWALL = "button.viewall.userhomepage";
	public static final String EVENT_VIEWALL = "button.viewall.event.userhomepage";
	public static final String REQUEST_TABS = "list.requests.tabs.userhomepage";
	public static final String LINKHOLIDAYS = CommonPage.elementTextEquals("View Holidays");
	public static final String LEAVETYPE = "type.leave.userhomepage";
	public static final String TEXT_INPUT = "input.name.equals.loc.common";
	public static final String DATE_FORMAT = "dd-MMMM-yyyy";
	public static final String WINDOW_SCROLLTO = "window.scrollTo(0, document.body.scrollHeight)";

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user should see home page")
	public void verifyHomePage() {
		verifyPresent("button.logout.userhomepage");
		verifyPresent("icon.expand.leftmenu.userhomepage");
	}

	@QAFTestStep(description = "user click on View Training Calendar link")
	public void clickOnViewTrainingCalendarLink() throws AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);

		NestUtils.waitForLoader();
		CommonStep.waitForEnabled("link.viewTrainingCalender.userHomepage", 600000);
		click("link.viewTrainingCalender.userHomepage");

	}

	@QAFTestStep(description = "user should see trainig calender")
	public void verifyTrainigCalenderPage() {
		CommonStep.waitForVisible("title.TrainingCalendar.userhomepage");
		CommonStep.assertVisible("title.TrainingCalendar.userhomepage");
	}

	@QAFTestStep(description = "user open any training listed in calendar of home page")
	public String openAnyTraining() {
		String nearesttraining = null;
		try {
			List<QAFWebElement> avltrainings = NestUtils.getQAFWebElements("list.trainings.userhome");
			nearesttraining = avltrainings.get(0).getText();
			NestUtils.clickUsingJavaScript(avltrainings.get(0));

		} catch (Exception e) {
			Reporter.log("No training listed in available screen " + e.getMessage());
			searchIncomingTraining();
		}
		return nearesttraining;
	}

	private void searchIncomingTraining() {
		if (!new QAFExtendedWebElement("list.trainings.userhome").isPresent()) {
			do {
				NestUtils.clickUsingJavaScript("arrow.navigateRight.userHomepage");
				NestUtils.waitForLoader();
			} while (!new QAFExtendedWebElement("list.trainings.userhome").isPresent());
			openAnyTraining();
		}

	}

	@QAFTestStep(description = "user should see the list of leave requests")
	public void shouldSeeLeaveRequest() {
		leave.verifyLeaveRequests();
		Reporter.logWithScreenShot("User is on leave Request Page");
	}

	@QAFTestStep(description = "user click on view all link for Expense requests")
	public void iClickOnViewAllLinkForExpenseRequests() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		NestUtils.waitForLoader();
		CommonStep.waitForEnabled(LINK_VIEWALL);
		leave.expenseRequests().click();
		CommonPage.clickUsingJavaScript(LINK_VIEWALL);
		Reporter.logWithScreenShot("User is on Expense Request Page");
	}

	@QAFTestStep(description = "user should see the list of expense request")
	public void iShouldSeeTheListOfExpenseRequest() {
		leave.verifyExpenseRequests();
		Reporter.logWithScreenShot("User is on expense Request Page");
	}

	@QAFTestStep(description = "user click on view all Travel requests")
	public void iClickOnViewAllTravelRequests() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		NestUtils.waitForLoader();
		CommonStep.waitForEnabled(LINK_VIEWALL);
		leave.travelRequests().click();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		CommonPage.clickUsingJavaScript(LINK_VIEWALL);
		Reporter.logWithScreenShot("User is on Travel Request Page");
	}

	@QAFTestStep(description = "user should see the list of Travel requests")
	public void iShouldSeeTheListOfTravelRequests() {
		leave.verifyTravelRequests();
	}

	@QAFTestStep(description = "user should see the Holidays List section with dates")
	public void verifyHolidaysListSectionWithDates() {
		leave.verifySection();
	}

	@QAFTestStep(description = "user Click on a Leave Request row")
	public void ClickOnLeaveRequestRow() {

		NestUtils.waitForLoader();
		verifyPresent("row.leave.request.details.userhomepage");
		CommonPage.clickUsingJavaScript("row.leave.request.details.userhomepage");

		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user click on a travel Request row")
	public void clickOnTravelRequestRow() {
		CommonPage.clickUsingJavaScript("link.row.travel.request.details.userhomepage");
		NestUtils.waitForLoader();
	}

	public int CalculateDiffBetwnDates(String startDateLoc, String endDateLoc) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		final String firstInput = new QAFExtendedWebElement(startDateLoc).getText();
		final String secondInput = new QAFExtendedWebElement(endDateLoc).getText();
		final LocalDate firstDate = LocalDate.parse(firstInput, formatter);
		final LocalDate secondDate = LocalDate.parse(secondInput, formatter);
		final int days = (int) ChronoUnit.DAYS.between(firstDate, secondDate);
		return days;
	}

	@QAFTestStep(description = "user observe {0} section on HomePage")
	public void observeTrainingCalendarSection(String sectionName) {
		NestUtils.waitForLoader();
		CommonPage.scrollUpToElement("section.calender.userhomepage");
		boolean flag = CommonPage.verifyLocContainsText("section.calender.userhomepage", sectionName);
		if (flag) {
			Reporter.logWithScreenShot("calender section display in HomePage", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("calender section is not display in HomePage", MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> calenderDays() {
		return NestUtils.getQAFWebElements("days.calender.userhomepage");
	}

	@QAFTestStep(description = "user verify Training Calendar should be displayed with one week range")
	public void verifyTrainingCalendarDisplayedOneWeekRange() {

		int diffDates = CalculateDiffBetwnDates("date.start.range.calender.homepage",
				"date.end.range.calender.homepage");
		int daysCountCalender = calenderDays().size() - 2;
		if (diffDates == daysCountCalender) {
			Reporter.logWithScreenShot("Training Calendar should be displayed with one week range", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Training Calendar should not be displayed with one week range",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify There should be option to navigate to next week")
	public void verifyOptionToNavigateNext_week() {

		if (verifyEnabled("btn.next.week.calender.homepage") && verifyPresent("btn.next.week.calender.homepage")) {
			Reporter.logWithScreenShot("There should be option to navigate to next week", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("There should not be option to navigate to next week", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on {0} request view all")
	public void clickOnViewAll(String requestType) {
		selectRequest(requestType);
		click(VIEWALL);
	}

	@QAFTestStep(description = "user verify the view {0}")
	public void verifyView(String viewName) {
		verifyText("label.view.userhomepage", viewName);
	}

	@QAFTestStep(description = "user click on {0} event view all")
	public void clickOnEventViewAll(String eventType) {
		selectEvent(eventType);
		click(EVENT_VIEWALL);
	}

	@QAFTestStep(description = "user navigate back to previous page")
	public void navigateBack() {
		driver.navigate().back();
	}

	@QAFTestStep(description = "user verify {0} requests are displayed on screen")
	public void verifyRequests(String requests) {
		String[] arrRequests = requests.split(",");
		for (String request : arrRequests) {
			boolean status = false;
			for (QAFWebElement tab : NestUtils.getQAFWebElements(REQUEST_TABS)) {
				if (tab.getText().trim().contains(request.trim())) {
					Reporter.log(request + " request tab is found.", MessageTypes.Pass);
					status = true;
					break;
				}
			}
			if (!status)
				Reporter.log(request + " request tab is not found.", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify {0} request is selected by default")
	public void verifyActiveRequest(String request) {
		boolean status = false;
		for (QAFWebElement tab : NestUtils.getQAFWebElements(REQUEST_TABS)) {
			if (tab.getText().trim().contains(request)) {
				Reporter.log(request + " request tab is found.", MessageTypes.Pass);
				if (tab.getAttribute("class").contains("active"))
					Reporter.log(request + " request tab is selected by default.", MessageTypes.Pass);
				status = true;
				break;
			}
		}
		if (!status)
			Reporter.log(request + " request tab is not found.", MessageTypes.Fail);

	}

	@QAFTestStep(description = "user verify holidays list section")
	public void verifyHolidaysList() {
		verifyPresent(LINKHOLIDAYS);
		verifyPresent("module.holidayslist.userhomepage");
		verifyPresent("title.holidays.userhomepage");
	}

	@QAFTestStep(description = "user perform action on view holidays")
	public void clickOnViewholidays() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		CommonStep.waitForVisible(LINKHOLIDAYS);
		click(LINKHOLIDAYS);
	}

	@QAFTestStep(description = "user navigate to holiday module")
	public void verifyNavigationToHolidayModule() {
		NestUtils.waitForLoader();
		String holiday = driver.getCurrentUrl();
		if (holiday.contains("holiday")) {
			Reporter.log("holiday module page is open", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("holiday module page is not open", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user navigate to travel request details page")
	public void verifyNavigationToTravelDtls() {
		NestUtils.waitForLoader();
		String travel = driver.getCurrentUrl();
		if (travel.contains("mytravelrequests")) {
			Reporter.log("travel request details page is open", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("travel request details page is not open", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify leave balance section")
	public void verifyLeaveBalanceType() {
		verifyPresent("section.leave.userhomepage");
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(LEAVETYPE), "PTO"));
		element.verifyPresent();
		element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(LEAVETYPE), "Comp Off"));
		element.verifyPresent();
		element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(LEAVETYPE), "Floating"));
		element.verifyPresent();
	}

	@QAFTestStep(description = "user click on View All link in Confirmation Review section of Manager view.")
	public void clickOnVeiwAllConfirmationReview() {

		NestUtils.waitForLoader();

		CommonPage.clickUsingJavaScript("link.viewAll.userhomepage");

	}

	@QAFTestStep(description = "user click on left arrow on training calender")
	public void clickOnLeftArrow() {

		NestUtils.waitForLoader();
		waitForVisible("icon.leftarrow.training.calender.userhomepage");
		QAFWebElement element = new QAFExtendedWebElement("icon.leftarrow.training.calender.userhomepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user click on right arrow on training calender")
	public void clickOnRightArrow() {
		NestUtils.waitForLoader();
		waitForVisible("icon.rightarrow.training.calender.userhomepage");
		QAFWebElement element = new QAFExtendedWebElement("icon.rightarrow.training.calender.userhomepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user should see last week calender")
	public void getlastWeekCalender() {
		verifyPresent("text.training.calender.userhomepage");
	}

	@QAFTestStep(description = "user should see next week calender")
	public void getNextWeekCalender() {
		verifyPresent("text.training.calender.userhomepage");
	}

	@QAFTestStep(description = "user Click on a Expense Request tab")
	public void ClickOnExpenseRequestRow() {

		NestUtils.waitForLoader();
		verifyPresent("tab.expense.requests.userhomepage");
		WebElement element = new QAFExtendedWebElement("tab.expense.requests.userhomepage");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user should be navigated to Expense details page")
	public void verifyNavigationToExpensesDetailsPage() {
		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("myexpense")) {
			Reporter.logWithScreenShot("user navigated to expense details page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user not navigated to expense details page", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see Employee confirmation review page.")
	public void verifyEmployeeConfirmationReviewPage() {
		NestUtils.waitForLoader();
		if (NestUtils.getCurrentURL().contains("confirmationfeedback"))
			Reporter.logWithScreenShot("Confirmation Review Page is displayed.",
					MessageTypes.Pass);

		else
			Reporter.logWithScreenShot("Confirmation Review Page is not displayed.",
					MessageTypes.Fail);
	}

	@QAFTestStep(description = "user click on Apply leave button")
	public void clickOnApplyLeaveButton() {
		NestUtils.waitForLoader();
		click("button.apply.leave.userhomepage");
	}

	@QAFTestStep(description = "user clicked on Manager View slide bar")
	public void clickedOnManagerViewSlideBar() {
		waitForPageToLoad();
		click("slider.userhomepage");

	}

	@QAFTestStep(description = "user should see Manager View")
	public void seeManagerView() {
		verifyPresent("text.manager.view.userhomepage");

	}

	@QAFTestStep(description = "user should see Planned leaves for this month section")
	public void seePlannedLeavesForThisMonthSection() {
		verifyPresent("number.of.days.planned.leaves.userhomepage");
		verifyPresent("text.employee.name.planeed.leaves.list.userhomepage");
		verifyPresent("leave.range.planned.leaves.userhomepage");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		CommonPage.scrollUpToElement("button.plannedleaves.view.all.userhomepage");
		verifyPresent("button.plannedleaves.view.all.userhomepage");

	}

	@QAFTestStep(description = "user click on view all button of {0}")
	public void clickOnViewAl(String requestType) {
		NestUtils.waitForLoader();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (requestType.toLowerCase().contains("manager view")) {
			viewAll(requestType.split(" : ")[0].trim(), requestType.split(" : ")[1].trim());
		} else
			viewAll(null, requestType.trim());
	}

	private void viewAll(String viewType, String requestType) {
		if (viewType == null) {
			if (requestType.contentEquals(TabNames.LEAVE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.EXPENSE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.TRAVEL_REQUEST.getTab())) {
				selectRequest(requestType);
				waitForPageToLoad();
				NestUtils.waitForLoader();
				CommonPage.waitForLocToBeClickable(VIEWALL);
				click(VIEWALL);
			} else if (requestType.contentEquals(TabNames.TRAINING_CALENDAR.getTab())) {
				NestUtils.scrollUpToElement(new QAFExtendedWebElement("link.viewTrainingCalender.userHomepage"));
				click("link.viewTrainingCalender.userHomepage");
			} else if (requestType.contentEquals(TabNames.HOLIDAYS_LIST.getTab())) {
				click(LINKHOLIDAYS);
			} else
				Reporter.log(requestType + " is not found.", MessageTypes.Fail);
		} else {
			if (requestType.contentEquals(TabNames.LEAVE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.EXPENSE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.TRAVEL_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.OTHER_REQUEST.getTab())) {
				selectRequestManager(requestType);
				click(LINK_VIEWALL);
			} else if (requestType.contentEquals(TabNames.CONFIRMATION_REVIEW.getTab())) {
				click("link.viewAll.userhomepage");
			} else if (requestType.contentEquals(TabNames.UPCOMING_BIRTHDAYS.getTab())
					|| requestType.contentEquals(TabNames.UPCOMING_ANNIVERSARIES.getTab())) {
				selectEvent(requestType);
				CommonPage.scrollUpToElement(EVENT_VIEWALL);
				click(EVENT_VIEWALL);
			} else
				Reporter.log(requestType + " is not found.", MessageTypes.Fail);
		}

	}

	private enum TabNames {
		TRAINING_CALENDAR("Training Calendar"), LEAVE_REQUEST("Leave Request"), EXPENSE_REQUEST(
				"Expense Request"), TRAVEL_REQUEST("Travel Request"), HOLIDAYS_LIST("Holiday Request"), OTHER_REQUEST(
						"Other Request"), CONFIRMATION_REVIEW("Confirmation Review"), UPCOMING_BIRTHDAYS(
								"Upcoming Birthdays"), UPCOMING_ANNIVERSARIES(
										"Upcoming Anniversaries"), PLANNED_LEAVES("Planned Leaves");
		private final String tab;

		private TabNames(String tabName) {
			tab = tabName;
		}

		public String getTab() {
			return tab;
		}
	}

	public void selectRequest(String requestType) {
		NestUtils.waitForLoader();
		for (QAFWebElement tab : NestUtils.getQAFWebElements(REQUEST_TABS)) {
			if (tab.getText().toLowerCase().contains(requestType.split(" ")[0].toLowerCase())) {
				tab.click();
				NestUtils.waitForLoader();
				return;
			} else
				continue;
		}
		Reporter.log(requestType + " tab is not found", MessageTypes.Fail);
	}

	public void selectRequestManager(String requestType) {
		NestUtils.waitForLoader();
		for (QAFWebElement tab : NestUtils.getQAFWebElements("list.requests.tabs.manager.userhomepage")) {
			if (tab.getText().toLowerCase().contains(requestType.toLowerCase())) {
				tab.click();
				NestUtils.waitForLoader();
				return;
			} else
				continue;
		}
		Reporter.log(requestType + " tab is not found", MessageTypes.Fail);
	}

	public void selectEvent(String eventType) {
		NestUtils.waitForLoader();
		for (QAFWebElement tab : NestUtils.getQAFWebElements("list.events.tabs.userhomepage")) {
			if (tab.getText().toLowerCase().contains(eventType.toLowerCase())) {
				NestUtils.scrollUpToElement(tab);
				((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 150);");
				tab.click();
				NestUtils.waitForLoader();
				break;
			}
		}
	}

	@QAFTestStep(description = "user click on {0} Request Tab")
	public void selectRequestTabs(String reqTabs) {
		NestUtils.waitForLoader();
		List<QAFWebElement> tabs = NestUtils.getQAFWebElements("tab.requests.heading.userhomepage");
		for (int i = 0; i < tabs.size(); i++) {
			String actValue = tabs.get(i).getText();
			if (actValue.contains(reqTabs)) {
				tabs.get(i).click();
				NestUtils.waitForLoader();
				break;
			}
		}
		NestUtils.waitForLoader();
	}

	public List<QAFWebElement> getExpenseRequesticons() {
		return NestUtils.getQAFWebElements("list.expenserow.viewdetails.userhomepage");
	}

	@QAFTestStep(description = "user click on expense Request row icon")
	public void clickOnExpenseRowIcon() {
		NestUtils.waitForLoader();
		Actions action = new Actions(driver);
		action.moveToElement(getExpenseRequesticons().get(0));
		action.perform();

		getExpenseRequesticons().get(0).click();
	}

	@QAFTestStep(description = "user verify navigation to Travel details page")
	public void verifyTravelDetailsPage() {

		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("travelrequestdetails")) {
			Reporter.logWithScreenShot("user navigate to Travel details page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user NOT navigate to Travel details page", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify navigation to Leave details page")
	public void verifyNavigationToLeaveDetailsPage() {

		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("myleavelist")) {
			Reporter.logWithScreenShot("User navigate to leave details page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("User NOT navigate to leave details page", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see the floating holidays List section")
	public void verifyFloatingHolidaysListSection() {
		verifyPresent("section.floatingHolidays.userhomepage");
	}

	@QAFTestStep(description = "user verifies the breadcrumb on tranning page {0}")
	public void verifyBreadcrumbtranning(String title) {
		NestUtils.waitForLoader();
		String breadcrumb = new QAFExtendedWebElement("txt.tranning.tranningpage").getText();

		if (breadcrumb.contains(title)) {
			Reporter.log("breadcrumb is present as per menu navigation", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);

		}
	}

	@QAFTestStep(description = "user verify Approve ,Reject,View options are displayed for leave request.")
	public void userVerifyApproveRejectViewOptionsAreDisplayed() {
		if (verifyPresent("button.viewleaverequest.managerViewPage")
				&& verifyPresent("button.approveleaverequest.managerViewPage")
				&& verifyPresent("button.rejectleaverequest.managerViewPage")) {
			Reporter.logWithScreenShot("Approve ,Reject,View options are present in leave request tab",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Approve ,Reject,View options are Not present in leave request tab",
					MessageTypes.Info);
		}
	}

	@QAFTestStep(description = "user verify View options are displayed for expense.")
	public void userVerifyApproveRejectViewOptionsAreDisplayedForExpense() {
		if (verifyPresent("button.viewexpenserequest.managerViewPage")) {
			Reporter.logWithScreenShot("View options are present in expense request tab", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("View options are Not present in expense request tab", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify eventList has date and description for respective event.")
	public void userVerifyEventListHasDateAndDescriptionForRespectiveEvent() {
		waitForVisible("img.loader.usauserhomepage");
		if (new QAFExtendedWebElement("lable.eventlist.usauserhomepage").getText() == null) {
			Reporter.logWithScreenShot("Event list doesnt have description", MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot("Event list is with description", MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user verify all Side Bar navigation")
	public void UserVerifyAllSideBarNavigation() {
		@SuppressWarnings("rawtypes")
		Iterator enuKeys = ConfigurationManager.getBundle().getKeys("my.");
		String prevURL = NestUtils.getCurrentURL();
		while (enuKeys.hasNext()) {
			String key = (String) enuKeys.next();
			if (key.contains("my.")) {
				String pathToNavigate = ConfigurationManager.getBundle().getString(key);
				Reporter.log(pathToNavigate);
				if (pathToNavigate.contains(":")
						&& !(pathToNavigate.contains("locator") || pathToNavigate.contains("desc"))) {
					try {
						NavigationPage.menuNavigation(pathToNavigate);
					} catch (InterruptedException IE) {
						Reporter.log(pathToNavigate + " is not a valid navigation path", MessageTypes.Fail);
					}

					NestUtils.waitForLoader();
					if (NestUtils.getCurrentURL().equals(prevURL)) {
						Reporter.logWithScreenShot("User is not  navigated to " + pathToNavigate + " page",
								MessageTypes.Fail);
					} else {
						Reporter.logWithScreenShot("User navigated to " + pathToNavigate + " page", MessageTypes.Pass);
					}
				} else {
					continue;
				}

			}
		}
	}

	@QAFTestStep(description = "{0} applied leave with leave data {1}")
	public void testDataCreationForLeaveRequest(String str0, String str1) throws InterruptedException {
		LandingPage landingPage = new LandingPage();
		landingPage.userAmOnNestLandingPage();
		landingPage.loginDetails(str0);
		NavigationPage.menuNavigation(ConfigurationManager.getBundle().getString("my.leave.applyleave"));
		LeavePage leavePage = new LeavePage();
		leavePage.userApplyForOneFullDayLeave(str1);
		landingPage.loggedOut();
		Reporter.log("Employee Logged out successfully.");
	}

	@QAFTestStep(description = "user clicks on view all link for {0}", stepName = "userClicksOnViewAllLinkFor")
	public void userClicksOnViewAllLinkFor(String moduleName) {
		NestUtils.scrollUpToElement(rrPage.getElement("other.section.view.all.link", moduleName));
		NestUtils.clickUsingJavaScript(rrPage.getElement("other.section.view.all.link", moduleName));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "verify user is navigated to {0} page")
	public void verifyUserIsNavigatedToBirthdayCalenderPage(String pageName) {
		NestUtils.waitForLoader();
		if (pageName.equalsIgnoreCase("birthday calender")) {
			Validator.verifyThat("User is navigated to " + pageName + " page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("birthday.calender.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("team leave list")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("team.leave.list.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("R & R Requests")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("rnr_requets.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("user")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("user.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("home")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("home.page.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("training details")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("training.detail.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("training calender")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("training.calender.url")),
					Matchers.equalTo(true));
		} else if (pageName.equalsIgnoreCase("Add/View Members")) {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("training.members")),
					Matchers.equalTo(true));
		} else {
			Validator.verifyThat("User is navigated to " + pageName + "page!",
					driver.getCurrentUrl().equalsIgnoreCase(CommonPage.appendWithBaseURL("attendance.url")),
					Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user clicks on {0} tab")
	public void clicksOnTab(String tabName) {
		rrPage.getElement("home.section.label", tabName).click();
	}

	@QAFTestStep(description = "verify UI of manager view")
	public void verifyUIOfManagerView() {
		verifySections();
		verifyViewAllLinkForSections();
		verifyDefaultSections();
		verifyVisible("logged.in.employee.name");
		verifyVisible("button.logout.userhomepage");
	}

	private void verifyDefaultSections() {
		Validator.verifyThat("Leave requests is default active tab!",
				rrPage.getElement("home.section.label", "Leave").getAttribute("class").contains("active"),
				Matchers.equalTo(true));
		Validator.verifyThat("Upcoming birthdays is default active tab!",
				rrPage.getElement("home.section.label", "Birthdays").getAttribute("class").contains("active"),
				Matchers.equalTo(true));
	}

	private void verifyViewLink() {
		verifyVisible("view.all.link");
		NestUtils.scrollUpToElement(rrPage.getElement("home.section.label", "Expense"));
		NestUtils.clickUsingJavaScript(rrPage.getElement("home.section.label", "Expense"));
		verifyVisible("view.all.link");
		NestUtils.clickUsingJavaScript(rrPage.getElement("home.section.label", "Travel"));
		verifyVisible("view.all.link");
	}

	private void verifyViewAllLinkForSections() {
		verifyViewLink();
		NestUtils.clickUsingJavaScript(rrPage.getElement("home.section.label", "Other"));
		verifyPresent("view.all.link");
		verifyViewAllLink("Confirmation");
		verifyViewAllLink("Birthday");
		NestUtils.clickUsingJavaScript(rrPage.getElement("home.section.label", "Anniversaries"));
		verifyViewAllLink("Birthday");
		verifyViewAllLink("teamLeaveList");
	}

	private void verifySections() {
		rrPage.getElement("home.section.label", "Leave").verifyVisible("Leave Requests");
		rrPage.getElement("home.section.label", "Expense").verifyVisible("Expense Requests");
		rrPage.getElement("home.section.label", "Travel").verifyVisible("Travel Requests");
		rrPage.getElement("home.section.label", "Other").verifyVisible("Other Requests");
		rrPage.getElement("home.section.label", "Birthdays").verifyVisible("Upcoming Birthdays");
		rrPage.getElement("home.section.label", "Anniversaries").verifyVisible("Upcoming Anniversaries");
		rrPage.getElement("other.section.label", "Confirmation Reviews").verifyVisible("Confirmation Reviews");
		rrPage.getElement("other.section.label", "Planned Leaves This Month")
				.verifyVisible("Planned Leaves This Month");
	}

	public void verifyViewAllLink(String moduleName) {
		rrPage.getElement("other.section.view.all.link", moduleName).verifyVisible(moduleName);
	}

	@QAFTestStep(description = "user clicks on {0} request row")
	public void userClicksOnRequestRow(String rowName) {
		int numberOfRequests = otherRequests.size();
		for (int i = 0; i < numberOfRequests; i++) {
			Reporter.log("gsagadgsgd" + otherRequests.get(i).getRequestsLabel().get(0).getText());
			if (otherRequests.get(i).getRequestsLabel().get(i).getText().contains(rowName)) {
				NestUtils.clickUsingJavaScript(otherRequests.get(i).getViewDetailsIcon().get(i));
				break;
			}
		}
	}

	@QAFTestStep(description = "user clicks on employee name link")
	public void clickOnLink() {
		click("logged.in.employee.name");
	}

	@QAFTestStep(description = "user verifies contact details are edited successfully")
	public void userVerifiesContactDetailsAreEditedSuccessfully() {
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		CommonStep.click(CommonPage.buttonTextContains("Edit"));
		EditContactDetailsBean contactBean = new EditContactDetailsBean();
		contactBean.fillRandomData();
		CommonStep.sendKeys(contactBean.getPermanentAddress(),
				CommonPage.getStringLocator("text.Address.myProfilePage", "per_address"));
		CommonStep.sendKeys(contactBean.getPercity(), CommonPage.getStringLocator(TEXT_INPUT, "per_city"));
		CommonStep.sendKeys(contactBean.getPerState(), CommonPage.getStringLocator(TEXT_INPUT, "per_state"));
		CommonStep.sendKeys(contactBean.getPerPostalCode(), CommonPage.getStringLocator(TEXT_INPUT, "per_postalCode"));
		if (CommonStep.verifyVisible(CommonPage.getStringLocator("text.Address.myProfilePage", "cur_address"))) {

			CommonStep.sendKeys(contactBean.getCurAddress(),
					CommonPage.getStringLocator("text.Address.myProfilePage", "cur_address"));
			CommonStep.sendKeys(contactBean.getCurcity(), CommonPage.getStringLocator(TEXT_INPUT, "cur_city"));
			CommonStep.sendKeys(contactBean.getCurState(), CommonPage.getStringLocator(TEXT_INPUT, "cur_state"));
			CommonStep.sendKeys(contactBean.getCurPostalCode(),
					CommonPage.getStringLocator(TEXT_INPUT, "cur_postalCode"));
		}
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));

	}

	@QAFTestStep(description = "user verifies {0} message on profile page")
	public void userVerifiesContactDetailsMessage(String message) {
		waitForVisible("label.status.common");
		NestUtils.verifyMessagePresent("label.status.common", message);
		NestUtils.closeAllStatusBars("label.status.common");
	}

	@QAFTestStep(description = "user verifies Emergency Contacts information present")
	public void userVerifiesEmergencyContactsInformationPresent() {
		CommonPage.commonDisplayedLocatorClick("button.edit.myProfilePage");
		NestUtils.waitForLoader();
		Validator.verifyThat("validating name",
				CommonPage.getWebElementFromList(TEXT_INPUT, "eec_name").getAttribute("value"),
				Matchers.notNullValue());
		Validator.verifyThat("validating mobile number",
				CommonPage.getWebElementFromList(TEXT_INPUT, "eec_mobile").getAttribute("value"),
				Matchers.notNullValue());
		Validator.verifyThat("validating home number",
				CommonPage.getWebElementFromList(TEXT_INPUT, "eec_home_no").getAttribute("value"),
				Matchers.notNullValue());
		Validator.verifyThat("validating work number",
				CommonPage.getWebElementFromList(TEXT_INPUT, "eec_office_no").getAttribute("value"),
				Matchers.notNullValue());
	}

	@QAFTestStep(description = "user selects holiday from dropdown")
	public void clickOnDropdown() {
		click("icon.dropdown.holiday");
		ConfigurationManager.getBundle().setProperty("SELECT_HOLIDAY", CommonStep.getText("dropdown.holiday.menu"));
		click("dropdown.holiday.menu");
	}

	@QAFTestStep(description = "verify user should be able to see the list of selected holidays type")
	public void verifyHolidaysListIsSelected() {
		Validator.verifyThat("selected holiday is visible!",
				CommonStep.getText("holiday.title").toString()
						.contains(ConfigurationManager.getBundle().getProperty("SELECT_HOLIDAY").toString()),
				Matchers.equalTo(true));
	}

	private void verifyHomePageView() {
		verifyBlocks();
		NestUtils.waitForLoader();
		verifyApplyLeaveLink();
		NestUtils.waitForLoader();
		verifyViewLink();
		NestUtils.waitForLoader();
		verifyVisible("link.viewTrainingCalender.userHomepage");
		NestUtils.waitForLoader();
		rrPage.getElement("view.all.link", "View Holidays").verifyVisible("View Holidays");
		NestUtils.waitForLoader();
		clickOnDropdown();
		NestUtils.waitForLoader();
		rrPage.getElement("view.all.link", "View Floating Holidays").verifyVisible("View Holidays");
		NestUtils.waitForLoader();
		rrPage.getElement("navigation.arrow.calendar", "left").verifyVisible("left arrow");
		NestUtils.waitForLoader();
		rrPage.getElement("navigation.arrow.calendar", "right").verifyVisible("right arrow");
		NestUtils.waitForLoader();
		verifyVisible("icon.dropdown.holiday");
		NestUtils.waitForLoader();
		verifyVisible("logged.in.employee.name");
		NestUtils.waitForLoader();
		verifyVisible("button.logout.userhomepage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "verify UI of {0} view for {1} month")
	public void verifyUIOfViewForMonth(String pageName, String month) {
		if (pageName.equalsIgnoreCase("Training Calender")) {
			verifyTrainingCalendarView(month);
		}
	}

	@QAFTestStep(description = "verify UI of {0} view")
	public void verifyUIOfHomeview(String pageName) {

		verifyHomePageView();
	}

	private String getMonth(String monthName) {

		Formatter fmt = new Formatter();
		Calendar cal = Calendar.getInstance();
		String month = null;
		if (monthName.equalsIgnoreCase("current")) {
			month = (fmt.format("%tB %<tY", cal)).toString();
		} else if (monthName.equalsIgnoreCase("previous")) {
			cal.add(Calendar.MONTH, -1);
			month = (fmt.format("%tB %<tY", cal)).toString();
		} else {
			cal.add(Calendar.MONTH, 1);
			month = (fmt.format("%tB %<tY", cal)).toString();
		}
		fmt.close();
		return month;
	}

	private void verifyTrainingCalendarView(String month) {
		NavigationPage naviagtion = new NavigationPage();
		naviagtion.userVerifyTextIs("training.calender.breadcrumb", "Training Calender");
		verifyVisible("training.calender.left.month");
		verifyVisible("training.calender.right.month");
		verifyVisible("training.calender.current.month");
		if (month.equalsIgnoreCase("current")) {
			Validator.verifyThat("Current month is displayed!",
					CommonStep.getText("training.calender.current.month").equalsIgnoreCase(getMonth(month)),
					Matchers.equalTo(true));
		} else if (month.equalsIgnoreCase("previous")) {
			Validator.verifyThat("previous month is displayed!",
					CommonStep.getText("training.calender.current.month").equalsIgnoreCase(getMonth(month)),
					Matchers.equalTo(true));
		} else {
			Validator.verifyThat("next month is displayed!",
					CommonStep.getText("training.calender.current.month").equalsIgnoreCase(getMonth(month)),
					Matchers.equalTo(true));
		}
		verifyVisible("training.calender.time");
		verifyVisible("training.name");
	}

	private void verifyApplyLeaveLink() {
		verifyVisible("button.apply.leave.userhomepage");
	}

	private void verifyBlocks() {
		int size = NestUtils.getQAFWebElements("homepage.header.blocks").size();
		if (size > 0) {
			Validator.verifyThat("Leave Credits visible",
					NestUtils.getQAFWebElements("homepage.header.blocks").get(0).getText().contains("Leave Credits"),
					Matchers.equalTo(true));
			Validator.verifyThat("Training Calendar visible", NestUtils.getQAFWebElements("homepage.header.blocks")
					.get(1).getText().contains("Training Calendar"), Matchers.equalTo(true));
			Validator.verifyThat("Holidays List visible",
					NestUtils.getQAFWebElements("homepage.header.blocks").get(2).getText().contains("Holidays List"),
					Matchers.equalTo(true));
		}
		rrPage.getElement("home.section.label", "Leave").verifyVisible("Leave Requests");
		rrPage.getElement("home.section.label", "Expense").verifyVisible("Expense Requests");
		rrPage.getElement("home.section.label", "Travel").verifyVisible("Travel Requests");

	}

	@QAFTestStep(description = "user verifies Emergency Contacts are edited successfully")
	public void userVerifiesEmergencyContactsAreEditedSuccessfully() {
		CommonPage.clickUsingJavaScript(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Edit"));
		EditContactDetailsBean contactBean = new EditContactDetailsBean();
		contactBean.fillRandomData();
		if (CommonStep.verifyVisible(CommonPage.getStringLocator(TEXT_INPUT, "eec_name"))) {
			CommonStep.sendKeys(contactBean.getPercity(), CommonPage.getStringLocator(TEXT_INPUT, "eec_name"));
			CommonStep.sendKeys(contactBean.getPerPostalCode(), CommonPage.getStringLocator(TEXT_INPUT, "eec_mobile"));
			CommonStep.sendKeys(contactBean.getPerPostalCode(), CommonPage.getStringLocator(TEXT_INPUT, "eec_home_no"));
			CommonStep.sendKeys(contactBean.getPerPostalCode(),
					CommonPage.getStringLocator(TEXT_INPUT, "eec_office_no"));

		}
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));

	}

	@QAFTestStep(description = "user verifies contact is deleted after clicking on delete")
	public void userVerifiesEmergencyContactDeletedSuccessfully() {
		NestUtils.waitForLoader();
		NestUtils.scrollToElement(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Delete"));
		click(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Delete"));
		CommonPage.clickUsingJavaScript(CommonPage.buttonTextEquals("Yes"));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user add new contact with relation {0}")
	public void userAddNewContact(String valueToSelect) {
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("New Contact"));
		EditContactDetailsBean contactBean = new EditContactDetailsBean();
		contactBean.fillRandomData();
		if (CommonStep.verifyPresent(CommonPage.getStringLocator(TEXT_INPUT, "eec_name"))) {
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator(TEXT_INPUT, "eec_name"),
					contactBean.getPercity());
			selectRelationship(valueToSelect);
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator(TEXT_INPUT, "eec_mobile"),
					contactBean.getPerPostalCode());
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator(TEXT_INPUT, "eec_home_no"),
					contactBean.getPerPostalCode());
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator(TEXT_INPUT, "eec_office_no"),
					contactBean.getPerPostalCode());

		}
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));

	}

	private void selectRelationship(String valueToSelect) {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick("dropdown.relationship.myProfilePage");
		List<QAFWebElement> relationship = NestUtils.getQAFWebElements("dropdown.relationValue.myprofilePage");
		for (int k = 0; k < relationship.size(); k++) {
			if (relationship.get(k).isDisplayed()) {
				if (relationship.get(k).getText().contains(valueToSelect)) {
					relationship.get(k).click();
					CommonPage.commonDisplayedLocatorClick("dropdown.relationship.myProfilePage");
					break;
				}
			}
		}

	}

	@QAFTestStep(description = "user verifies cancel option for {0}")
	public void userVerifiesCancelOption(String option) {
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains(option));
		click(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Cancel"));
		Validator.verifyThat("Validating add emergency conatct getting cancelled or not",
				verifyPresent(CommonPage.buttonTextContains(option)), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user add new Member with relation {0}")
	public void userAddNewFamilyMember(String valueToSelect) {
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("New Member"));
		EditContactDetailsBean contactBean = new EditContactDetailsBean();
		contactBean.fillRandomData();
		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Name"))) {
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator("textbox.all.publisher", "Name"),
					contactBean.getPercity());
			selectRelationship(valueToSelect);
			CommonPage.commonDisplayedLocatorSendKeys(
					CommonPage.getStringLocator("textbox.all.publisher", "Enter Occupation"),
					contactBean.getCurCountry());
		}
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));

	}

	@QAFTestStep(description = "user verifies Family Member are edited successfully")
	public void userVerifiesFamilyMemberAreEditedSuccessfully() {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Edit"));
		EditContactDetailsBean contactBean = new EditContactDetailsBean();
		contactBean.fillRandomData();
		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Name"))) {
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator("textbox.all.publisher", "Name"),
					contactBean.getPercity());
			CommonPage.commonDisplayedLocatorSendKeys(
					CommonPage.getStringLocator("textbox.all.publisher", "Enter Occupation"),
					contactBean.getCurCountry());
		}
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user clicks on {0} month")
	public void clickOnPreviousMonth(String monthType) {
		if (monthType.equalsIgnoreCase("previous"))
			click("training.calender.left.month");
		else
			click("training.calender.right.month");
	}

	@QAFTestStep(description = "user clicks on {0} link")
	public void clickOnHomepageLink() {
		click("page.title");
	}

	@QAFTestStep(description = "user verifies Family member is deleted after clicking on delete")
	public void userVerifiesFamilyMemberDeletedSuccessfully() {
		NestUtils.waitForLoader();
		CommonPage
				.commonDisplayedLocatorClick(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Delete"));
		CommonPage.clickUsingJavaScript(CommonPage.buttonTextEquals("Yes"));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies Family Member are edited successfully with date of birth as {0}")
	public void userVerifiesFamilyMemberAreEditedDateOfBirth(String dateOfBirth) {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Edit"));
		CommonPage.commonDisplayedLocatorClick("icon.calendar.from");
		NestUtils.waitForLoader();
		calendar.selectDate(dateOfBirth);
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user add new Relationship with employee {0} as {1}")
	public void userAddNewRelationship(String employee, String valueToSelect) {
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains(" New Relationship"));
		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Name"))) {
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator("textbox.all.publisher", "Name"),
					employee);
			CommonPage.getWebElementFromList("textbox.all.publisher", "Name").sendKeys(Keys.TAB);
			selectRelationship(valueToSelect);
		}
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));

	}

	@QAFTestStep(description = "user verifies Relationship is edited successfully with {0} as {1}")
	public void userEditRelationship(String employee, String valueToSelect) {
		CommonPage.commonDisplayedLocatorClick(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Edit"));
		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Name"))) {
			CommonPage.getWebElementFromList("textbox.all.publisher", "Name").clear();
			CommonPage.commonDisplayedLocatorSendKeys(CommonPage.getStringLocator("textbox.all.publisher", "Name"),
					employee);
			CommonPage.getWebElementFromList("textbox.all.publisher", "Name").sendKeys(Keys.TAB);
			selectRelationship(valueToSelect);
		}

		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user clicks on training date")
	public void clickOnTrainingDate() {
		waitForVisible("training.calender.time");
		NestUtils.clickUsingJavaScript("training.calender.time");
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "verify UI of {0} page")
	public void verifyUIofPage(String pageName) {
		CommonStep.verifyText("common.breadcrumb", pageName);
		if (pageName.equalsIgnoreCase("Add / View Members List")) {
			verifyAddViewMemeberListPage();
		} else {
			verifyTrainingDetailPage();
		}
	}

	private void verifyAddViewMemeberListPage() {
		verifyVisible("txt.trainingname.trainingdetails");
		rrPage.getElement("training.members.headers", "Training Details").verifyVisible("Training Details");
		rrPage.getElement("training.members.headers", "Add/View Members").verifyVisible("Add/View Members");
		rrPage.getElement(RnRPage.BUTTON_NAME, "Register").verifyVisible("register button");
		rrPage.getElement(RnRPage.BUTTON_NAME, "Back").verifyVisible("back button");
	}

	private void verifyTrainingDetailPage() {
		verifyVisible("training.name.id");
		List<QAFWebElement> headerElements = NestUtils.getQAFWebElements("training.detail.page.headers");
		for (QAFWebElement header : headerElements) {
			header.verifyVisible(header.getText());
		}
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back"));
	}

	@QAFTestStep(description = "user verify {0} and {1} is visible")
	public void userVerifyAndIsVisible(String repMngr, String revMngr) {
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForLoader();
		QAFExtendedWebElement repMngrheading = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("div.contains.loc.common"), repMngr));
		repMngrheading.verifyVisible();
		QAFExtendedWebElement revMngrheading = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("div.contains.loc.common"), revMngr));
		revMngrheading.verifyVisible();
		List<QAFWebElement> mangrNames = NestUtils.getQAFWebElements("ess.myprofile.reportingmangernames");
		for (int i = 0; i < mangrNames.size(); i++) {
			mangrNames.get(i).verifyVisible();
			if (!(mangrNames.get(i).getText().isEmpty())) {
				Reporter.logWithScreenShot("manager names are  present", MessageTypes.Pass);

			} else {
				Reporter.logWithScreenShot("manager names are not present", MessageTypes.Fail);
			}

		}
	}

	public static void sendKeysUsingJavaScript(String loc) {
		JavascriptExecutor executor = (JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", new QAFExtendedWebElement(loc));
	}

	public void uploadFileWithRobot(String filePath) {
		StringSelection stringSelection = new StringSelection(filePath);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);

		Robot robot = null;

		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	@QAFTestStep(description = "user uploads document {0} on training page")
	public void uploadDocument(String filename) {
		uploadFileWithRobot(filename);
		rrPage.getElement(RnRPage.BUTTON_NAME, "Submit").waitForEnabled();
		NestUtils.clickUsingJavaScript(rrPage.getElement(RnRPage.BUTTON_NAME, "Submit"));
	}

	@QAFTestStep(description = "verify document is uploaded successfully")
	public void verifyDocumentIsUploaded() {
		waitForVisible("success.msg");
		Validator.verifyThat("Attachment added sucessfully!",
				CommonStep.getText("success.msg").trim().equalsIgnoreCase("Attachment(s) added successfully"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify color of {0} tab")
	public void userVerifyColorOfTab(String str0) {
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForLoader();
		String tab = CommonPage.getStringLocator("link.tabs.configureLeavePage", str0);
		String tabcolor = new QAFExtendedWebElement(tab).getCssValue("color");
		Validator.verifyThat("Selected tab be orange",
				tabcolor.equals(ConfigurationManager.getBundle().getString("orange.colorcode")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies checkBox is present and clickable")
	public void verifyCheckBox() {
		if (new QAFExtendedWebElement("checkbox.relationshipdeclaration.myProfilePage")
				.isPresent()) {
			Reporter.log("Check box is present", MessageTypes.Pass);
		} else {
			while (!new QAFExtendedWebElement(
					"checkbox.relationshipdeclaration.myProfilePage").isPresent()) {
				NestUtils.waitForLoader();
				CommonPage.clickUsingJavaScript(CommonPage
						.getStringLocator("button.editDelete.myProfilePage", "Delete"));
				CommonPage.clickUsingJavaScript(CommonPage.buttonTextEquals("Yes"));
				NestUtils.waitForLoader();
			}
			if (new QAFExtendedWebElement(
					"checkbox.relationshipdeclaration.myProfilePage").isPresent()) {
				Reporter.log("Check box is present", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("Check box is not present", MessageTypes.Fail);
			}
		}
				
	}

	@QAFTestStep(description = "verify all mandatory fields are auto filled by admin")
	public void verifyAllMandatoryFieldAreAutoFilledByAdmin() {
		((JavascriptExecutor) driver).executeScript("scroll(0,250);");
		QAFExtendedWebElement dropDown = new QAFExtendedWebElement("dd.mandatorydropdown.myprofilePage");
		String selectedoption = dropDown.getText();
		Reporter.log(selectedoption);
		QAFExtendedWebElement textField = new QAFExtendedWebElement("txt.empworkemail.myprofilePage");
		textField.waitForVisible();

		if ((textField.getAttribute("value")).isEmpty() && selectedoption.isEmpty()) {
			Reporter.logWithScreenShot("dropdown and text fields not filled by admin", MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot("dropdown and text fields filled by admin", MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "Verify all drop down icon is not clickable in Professional information page")
	public void verifyAllDropDownIconIsNotClickableInProfessionalInformationPage() {

		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		List<QAFWebElement> listofDD = NestUtils.getQAFWebElements("dd.dropdownfields.myprofilePage");
		for (QAFWebElement element : listofDD) {
			if (element.isEnabled()) {
				Reporter.logWithScreenShot("Dropdown is working and Enabled", MessageTypes.Fail);
			} else {
				Reporter.logWithScreenShot("Dropdown is Not Active", MessageTypes.Pass);
			}
		}
	}

	public int CountTablerows(String tabname) {
		String rowElements = CommonPage.getStringLocator("ess.professionalqualification.commonrows", tabname);
		List<QAFWebElement> locator = NestUtils.getQAFWebElements(rowElements);
		return locator.size();

	}

	@QAFTestStep(description = "user verify {0} button of {1} tab")
	public void userVerifyButtonOfTab(String btnName, String tabName) {

		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForLoader();
		int rowcount = CountTablerows(tabName);
		Reporter.log("row count= : " + rowcount, MessageTypes.Pass);
		NestUtils.waitForLoader();
		String parentloc = CommonPage.getStringLocator("tab.control.common", tabName);
		String chiladdbtn = CommonPage.getStringLocator("text.equals.loc.common", btnName);
		QAFWebElement ele = NestUtils.getChildElement(parentloc, chiladdbtn);
		NestUtils.clickUsingJavaScript(ele);
		NestUtils.waitForLoader();
		if (CountTablerows(tabName) == rowcount + 1)
			Reporter.log("row is added= : " + CountTablerows(tabName), MessageTypes.Pass);
		else
			Reporter.log("row is not added= : " + CountTablerows(tabName), MessageTypes.Fail);

	}

	@QAFTestStep(description = "user add {0} data for {1} tab")
	public void userAddDataForTab(Object userDetails, String tabname) throws InterruptedException {

		if (userDetails instanceof String) {
			professionalExperience.fillFromConfig((String) userDetails);
		} else
			professionalExperience.fillData(userDetails);
		String tabledata = CommonPage.getStringLocator("ess.professionaexp.edittablecells", tabname);
		List<QAFWebElement> locator = NestUtils.getQAFWebElements(tabledata);
		Reporter.log("size " + locator.size(), MessageTypes.Pass);
		NestUtils.waitForLoader();
		locator.get(0).waitForEnabled();
		locator.get(0).sendKeys(professionalExperience.getComapnyName());
		locator.get(1).sendKeys(professionalExperience.getJobTitle());
		locator.get(2).click();
		NestUtils.waitForLoader();
		calendar.selectDate(professionalExperience.getFromDate());
		locator.get(3).click();
		NestUtils.waitForLoader();
		calendar.selectDate(professionalExperience.getToDate());
		NestUtils.waitForLoader();
		locator.get(4).sendKeys(professionalExperience.getComment());

		String parentloc = CommonPage.getStringLocator("tab.control.common", tabname);
		String chiladdbtn = CommonPage.getStringLocator("text.equals.loc.common", "Save");
		QAFWebElement ele = NestUtils.getChildElement(parentloc, chiladdbtn);
		NestUtils.clickUsingJavaScript(ele);
	}

	@QAFTestStep(description = "user {0} new language in {1} as {2}")
	public void userAddNewLangauge(String btnName, String tabName, String language) {
		NestUtils
				.clickUsingJavaScript(CommonPage.getStringLocator("form.containsButton.profilepage", tabName, btnName));

		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Select Language"))) {

			CommonPage.commonDisplayedLocatorSendKeys(
					CommonPage.getStringLocator("textbox.all.publisher", "Select Language"), language);
			CommonPage.getWebElementFromList("textbox.all.publisher", "Select Language").sendKeys(Keys.TAB);
		}
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(CommonPage.buttonTextContains("Save"));
	}

	@QAFTestStep(description = "user verify calender icon Experience tab for entering dates as {0} and {1}")
	public void userVerifyCalenderForExpTab(String fromDate, String toDate) throws InterruptedException {
		List<QAFWebElement> locator = NestUtils
				.getQAFWebElements(CommonPage.getStringLocator("ess.professionaexp.edittablecells", "Experience"));
		locator.get(2).click();
		calendar.selectDate(fromDate);
		locator.get(3).click();
		calendar.selectDate(toDate);

	}

	@QAFTestStep(description = "user verify calender icon Qualification tab for entering dates as {0} and {1}")
	public void userVerifyCalenderForQualTab(String fromDate, String toDate) throws InterruptedException {
		List<QAFWebElement> locator = NestUtils
				.getQAFWebElements(CommonPage.getStringLocator("ess.professionaexp.edittablecells", "Qualification"));
		locator.get(3).click();
		calendar.selectDate(fromDate);
		locator.get(4).click();
		calendar.selectDate(toDate);

	}

	@QAFTestStep(description = "user verifies cancel option for {0} in {1} tab")
	public void userVerifiesCancelOptionforPI(String option, String tabName) {
		CommonPage.clickUsingJavaScript(CommonPage.getStringLocator("button.editDelete.myProfilePage", "Cancel"));
		Validator.verifyThat("Validating add Professional " + tabName + "  getting cancelled or not",
				NestUtils.getChildElement(CommonPage.getStringLocator("tab.control.common", tabName),
						CommonPage.buttonTextContains(option)).isDisplayed(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user edit {0} data for {1} tab")
	public void userEditDataForTab(Object userDetails, String tabName) throws InterruptedException {
		NestUtils.waitForLoader();
		NestUtils.scrollToElement(CommonPage.getStringLocator("form.PI.myProfilePage", tabName, "Edit"));
		NestUtils.clickUsingJavaScript(CommonPage.getStringLocator("form.PI.myProfilePage", tabName, "Edit"));
		NestUtils.waitForLoader();
		professionalExperience.fillData(userDetails);
		List<QAFWebElement> locator = NestUtils
				.getQAFWebElements(CommonPage.getStringLocator("ess.professionaexp.edittablecells", tabName));
		locator.get(0).sendKeys(professionalExperience.getComapnyName());
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(
				NestUtils.getChildElement(CommonPage.getStringLocator("tab.control.common", tabName),
						CommonPage.getStringLocator("text.equals.loc.common", "Save")));
	}

	@QAFTestStep(description = "user click {0} button of {1} tab")
	public void userClickAddForTab(String btnName, String tabName) {
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForLoader();
		int rowcount = CountTablerows(tabName);
		Reporter.log("row count= : " + rowcount, MessageTypes.Pass);
		NestUtils.waitForLoader();
		NestUtils
				.clickUsingJavaScript(CommonPage.getStringLocator("form.containsButton.profilepage", tabName, btnName));
		NestUtils.waitForLoader();
		if (CountTablerows(tabName) == rowcount + 1)
			Reporter.log("row is added= : " + CountTablerows(tabName), MessageTypes.Pass);
		else
			Reporter.log("row is not added= : " + CountTablerows(tabName), MessageTypes.Fail);

	}

	@QAFTestStep(description = "user add data for {0} tab as degree {1} in {2}")
	public void userAddNewQulaification(String tabName, String degree, String year) {

		if (CommonStep.verifyPresent(CommonPage.getStringLocator("textbox.all.publisher", "Select Degree"))) {
			CommonPage.commonDisplayedLocatorSendKeys(
					CommonPage.getStringLocator("textbox.all.publisher", "Select Degree"), degree);
			CommonPage.getWebElementFromList("textbox.all.publisher", "Select Degree").sendKeys(Keys.TAB);
			CommonPage.commonDisplayedLocatorSendKeys(
					CommonPage.getStringLocator("textbox.all.publisher", "Enter Passing Year"), year);
		}
		NestUtils.waitForLoader();
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(CommonPage.getStringLocator("form.containsButton.profilepage", tabName, "Save"));
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "user {0} data for {1} tab")
	public void userDataForTab(String btnname, String tabname) {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		NestUtils.waitForLoader();
		String newloc = String.format(ConfigurationManager.getBundle().getString("ess.tabs.button.common"), tabname,
				btnname);
		List<QAFWebElement> listWebElement = NestUtils.getQAFWebElements(newloc);
		int j = listWebElement.size();
		NestUtils.scrollToViewElement((QAFExtendedWebElement) listWebElement.get(j - 1));
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(listWebElement.get(j - 1));
		NestUtils.waitForLoader();
		if (btnname.contains("Edit")) {
			String tabledata = CommonPage.getStringLocator("ess.professionaexp.edittablecells", tabname);
			List<QAFWebElement> locator = NestUtils.getQAFWebElements(tabledata);
			locator.get(4).clear();
			locator.get(4).sendKeys(" Test Comment edited");
			String parentloc = CommonPage.getStringLocator("tab.control.common", tabname);
			String chiladdbtn = CommonPage.getStringLocator("text.equals.loc.common", "Save");
			QAFWebElement ele = NestUtils.getChildElement(parentloc, chiladdbtn);
			NestUtils.waitForLoader();
			NestUtils.clickUsingJavaScript(ele);
			NestUtils.waitForLoader();
		} else if (btnname.equalsIgnoreCase("Delete")) {

			CommonPage.getWebElementFromList("button.contains.loc.common", "Yes").click();
			NestUtils.waitForLoader();
		}
	}

}
