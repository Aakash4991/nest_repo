package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.nest.beans.AddTravelRequestBean;
import com.nest.beans.AirlineMemberShipFieldsData;
import com.nest.components.CalendarComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class TravelPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	public static final String ARGUMENT = "arguments[0].click();";
	public static final String MY_NEW_TRAVEL_REQUEST_BTN =
			"btn.new.travel.request.mytravelrequestpage";
	public static final String PAGE_URL = "mytravelrequests";
	public static final String PURPOSE_OF_TRAVEL_TXTBOX =
			"txtbox.purpose.of.travel.addtravelrequestpage";
	public static final String JOURNEY_FROM_TXTBOX =
			"txtbox.journey.from.addtravelrequestpage";
	public static final String JOURNEY_TO_TXTBOX =
			"txtbox.journey.to.addtravelrequestpage";
	public static final String JOURNEY_START_DATE =
			"date.journey.start.date.addtravelrequestpage";
	public static final String DATEPICKER_DATE = "datepicker.date.addtravelrequestpage";
	public static final String JOURNEY_END_DATE =
			"date.journey.end.date.addtravelrequestpage";
	public static final String PROJECT_NAME = "txtbox.project.name.addtravelrequestpage";

	public static final String SEARCH_BUTTON = "btn.search.travelrequestpage";
	public static final String BACKGROUND_COLOR = "background-color";
	public static final String SCROLL_TO =
			"window.scrollTo(0,document.body.scrollHeight)";
	public static final String TYPE_ADD_TRAVEL_BOOKING =
			"dropdown.typedropdown.addtravelbookings.travelrequestpage";
	public static final String FIELDS_ADD_TRAVEL_BOOKINGS =
			"fields.addtravelbookings.travelrequestpage";
	public static final String CHECK_IN_DATE = "checkInDate";
	public static final String CHECK_OUT_DATE = "checkOutDate";
	public static final String BOOKING_DATE = "bookingDate";
	public static final String AMMOUNT = "ammount";
	public static final String TAX_AMMOUNT = "taxAmmount";
	public static final String AGENT_ADD_TRAVEL_BOOKINGS =
			"dropdown.agent.addtravelbookings.travelrequestpage";
	public static final String AGENT_FEE = "agentFee";
	public static final String TICKET_NUMBER = "ticketNumber";
	public static final String ATTACHMENTS = "attachments";
	public static final String BORDER_COLOR = "border-color";

	public static final String TOOL_TIP = "tooltip.addtravelrequest.travelrequestpage";
	public static final String REQUEST_FROM_TABLE =
			"text.travelPurpose.list.mytravelrequest.travelrequestpage";

	public static final String RESET_BUTTON = CommonPage.buttonTextContains("Reset");
	public static final String BOOKING_BUTTON = CommonPage.buttonTextContains("Bookings");

	public static final String AIRLINE_MEMBERSHIP_BREADCRUMB =
			"AIRLINE_MEMBERSHIP_BREADCRUMB";

	public static final String COMMON_DIV_TEXT =
			CommonPage.elementTextEquals("My Travel Requests");
	public static final String YESBUTTONINAIRLINEMEMBERSHIP =
			"text.yesbutton.airlinemembership";
	public static final String TEXT_DELETEMESSAGE = "txt.deletemessge.airlinemembership";
	public static final String EDIT_BUTTON_IN_AIRLINEMEMBERSHIP =
			"btn.edit.airlinemembership";
	public static final String MANDATORYFIELDS_AIRLINEMEMBERSHIP =
			"txt.mandatoryfields.airlinemembership.travelpage";
	public static final String SAVE_BUTTON_AIRLINEMEMBERSHIP =
			"btn.save.airlinemembership";
	public static final String yesButtonInAirlineMembership =
			"button.contains.loc.common";
	public static final String text_Message = "div.contains.loc.common";
	public static final String editButtonInAirlineMembership = "icons.with.title";
	public static final String mandatoryFieldsAirlineMembership =
			"input.name.contains.loc.common";
	public static final String saveButtonAirlineMembership = "button.contains.loc.common";

	public static final String NEW_TRAVEL_REQUEST_BTN =
			CommonPage.buttonTextEquals("New Travel Request");

	public static final String NEW_TRAVEL_REQUEST_PAGE_TITLE =
			CommonPage.divTextContains("Add Travel Request");

	public static List<QAFWebElement> listMgrBreadcumbNewtravelrequest =
			NestUtils.getQAFWebElements("list.mgr.breadcumb.newtravelrequest");

	public static List<QAFWebElement> listMgrRadioButtonsNewtravelrequest =
			NestUtils.getQAFWebElements("list.radiButtons.mgr.newTravelRequest");

	public static final String JOURNEY_MGR_TO_TXTBOX =
			"txtbox.journey.to.addtravelrequestpage";
	public static final String JOURNEY_MGR_FROM_TXTBOX =
			"txtbox.journey.from.addtravelrequestpage";

	public static final String CATEGORY_DROPDOWN =
			CommonPage.buttonTextContains("Selected");

	public static final String DROPDOWN_ROUNDTRIP_ADDTRAVELREQUESTPAGE =
			"dropdown.round.trip.adtravelrequestpage";
	public static final String FROM_DATE = "from.dates";
	public static final String TO_DATE = "to.dates";
	public static final String TEXTBOX_EXTRAINFO_ADDTRAVELREQUESTPAGE =
			"txtbox.extra.info.addtravelrequestpage";
	public static final String TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE =
			"text.newtravelrequest.breadcrumb.travelrequestpage";
	public static final String BUTTON_SUBMIT_ADDTRAVELREQUESTPAGE =
			"btn.submit.addtravelrequestpage";
	public static final String LINK_NEXTPAGE_TRAVELREQUESTPAGE =
			"lnk.nextpage.travelrequestpage";
	public static final String WINDOW_SCROLLTO =
			"window.scrollTo(0, document.body.scrollHeight)";
	public static final String TEXT_CONTAINS_LOC_COMMON = "text.contains.loc.common";
	public static final String BUTTON_ADD_NEW = "Add New";
	public static final String TEXTBOX_TRAVELTITLE_TRAVELMASTERPAGE =
			"txtbox.travel.title.travelmasterpage";
	public static final String TRAVELCATEGORYLEVEL = "travelCategoryLevel";
	public static final String BUTTON_MODEOFTRAVEL_TRAVELMASTERPAGE =
			"btn.mode.of.travel.travelmasterpage";
	public static final String BUTTON_STATUS_TRAVELMASTERPAGE =
			"btn.status.travelmasterpage";
	public static final String BUTTON_DELETE_POPUP_TRAVELMASTERPAGE =
			"btn.delete.category.popup.travelmasterpage";
	public static final String THIS_IS_REQUIRED_FIELD = "This is a required field.";
	public static final String TRAVELAGENTNAME = "travelAgentName";
	public static final String DROPDOWN_STATUS_TRAVELLOCATION_TRAVELPAGE =
			"dropdown.status.travellocationTravelPage";
	public static final String DROPDOWN_EMPLOYEE_AIRLINEMEMBERSHIP =
			"dropdown.employee.airlineMembershipPage";
	public static final String LIST_EMPLOYEENAME_AIRLINEMEMBERSHIP =
			"list.employeeName.airlineMembershipPage";
	public static final String TEXTBOX_SEARCHTITLE_TRAVELLOCATIONPAGE =
			"txtbox.search.title.travellocationpage";
	public static final String TEXT_INPUTTEXT_LOC_COMMON = "text.inputText.loc.common";
	Random random = new Random();

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// overriden method
	}

	PublisherPage publisherpage = new PublisherPage();
	LeavePage leaveobj = new LeavePage();
	CalendarComponent calendar = new CalendarComponent();
	AddTravelRequestBean addtravelrequestbean = new AddTravelRequestBean();
	AirlineMemberShipFieldsData airlineBean = new AirlineMemberShipFieldsData();

	@QAFTestStep(description = "user select the Travel menu and My Travel Requests submenu")
	public void selectTravelMenuSubmenu() {

		NestUtils.waitForLoader();

		click("menu.travel.userhomepage");
		click("submenu.mytravelrequest.userhomepage");
	}

	@QAFTestStep(description = "user navigate to add travel request page")
	public void userNavigateToAddTravelRequestPage() {

		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("addtravelrequest")) {
			Reporter.logWithScreenShot("User navigates to add Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User NOT navigate to Travel details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should navigate to my travel request page")
	public void userShouldNavigateToMyTravelRequestPage() {
		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains(PAGE_URL)) {
			Reporter.logWithScreenShot("User is navigated to my Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User NOT navigate to My Travel request page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see Travel details page")
	public void userShouldSeeTravelDetailsPage() {
		verifyPresent("submenu.mytravelrequest.userhomepage");
	}

	@QAFTestStep(description = "user fill {0} details and click on back button")
	public void userFillDetailsAndClickOnBackButton(String key) {

		addtravelrequestbean.fillFromConfig(key);

		NestUtils.waitForLoader();
		sendKeys(addtravelrequestbean.getTravelpurpose(), PURPOSE_OF_TRAVEL_TXTBOX);
		click("dropdown.trip.type.addtravelrequestpage");
		click(DROPDOWN_ROUNDTRIP_ADDTRAVELREQUESTPAGE);
		CommonStep.clear(JOURNEY_FROM_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyfrom() + Keys.TAB, JOURNEY_FROM_TXTBOX);
		CommonStep.clear(JOURNEY_TO_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyto() + Keys.TAB, JOURNEY_TO_TXTBOX);
		NestUtils.waitForLoader();
		CommonPage.waitForLocToBeClickable(JOURNEY_START_DATE);
		selectDate(JOURNEY_START_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(FROM_DATE));
		selectDate(JOURNEY_END_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(TO_DATE));
		sendKeys(addtravelrequestbean.getProjectname(), PROJECT_NAME);
		sendKeys(addtravelrequestbean.getExtrainfo(),
				TEXTBOX_EXTRAINFO_ADDTRAVELREQUESTPAGE);

		click("btn.clear.addtravelrequestpage");

		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains(PAGE_URL)) {
			Reporter.logWithScreenShot("User navigated to add Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User not navigate to Travel details page",
					MessageTypes.Fail);
		}

		Reporter.logWithScreenShot("Clear button is properly working");
	}

	@QAFTestStep(description = "user select filter {0} using requestid")
	public void selectFilterUsingRequestId(String requestId) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement travelRequestIdElement =
				creatElement("div.text.following.sib.input.common", "Travel Request ID");
		travelRequestIdElement.clear();
		travelRequestIdElement.sendKeys(requestId);

	}

	@QAFTestStep(description = "user should see data populated based on the applied filter requestid {0}")
	public void verifyFilterDataByRequestId(String reqId) {
		NestUtils.waitForLoader();
		String requestId =
				new QAFExtendedWebElement("text.requestid.fromtable.travelrequestpage")
						.getText();
		Validator.verifyThat(requestId, Matchers.containsString(reqId));

	}
	@QAFTestStep(description = "user clicks on reset")
	public void clickResetButton() {
		NestUtils.waitForLoader();
		click("btn.reset.travelrequestpage");

	}

	@QAFTestStep(description = "user should see all the filters {0} reset to default values")
	public void verifyResetFilters(String resetvalue) {

		NestUtils.waitForLoader();
		QAFExtendedWebElement travelRequestIdElement =
				creatElement("div.text.following.sib.input.common", "Travel Request ID");
		String requestidtxtboxval = travelRequestIdElement.getText();
		if (requestidtxtboxval.contains(""))
			Reporter.logWithScreenShot("The filter is reset");
		else Reporter.logWithScreenShot("The fileter is not reset");

	}
	@QAFTestStep(description = "user should see the travel details page with colors according to the theme of the application")
	public void verifyColor() {
		QAFExtendedWebElement searchButton = creatElement("all.text.common", "Search");
		String color = searchButton.getCssValue(BACKGROUND_COLOR);
		if (color.contains("rgba(235, 114, 3, 1)"))
			Reporter.logWithScreenShot(
					"Colors on Travel details page are according to the theme",
					MessageTypes.Pass);
		else
			Reporter.log("Colors on Travel details page are not according to the theme",
					MessageTypes.Fail);

	}

	@QAFTestStep(description = "the request should get {0} successfully")
	public void verifyApproved(String value) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement approvedText =
				creatElement("firsttablevalue.status.td.common", value);

		String status = approvedText.getText();
		Reporter.log(status);
		if (status.contains("Approved"))
			Reporter.logWithScreenShot("Request Approved", MessageTypes.Pass);
		else Reporter.logWithScreenShot("Request not Approved", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user selects {0} from the drop down menu for the request")
	public void selectApprove(String value) {
		NestUtils.waitForLoader();
		click("drdwn.select.travelrequestpage");

		QAFExtendedWebElement approvedOrRejectText =
				creatElement("text.firstrow.intable.travelrequest", value);
		Actions optionFromDropDown = new Actions(driver);
		optionFromDropDown.moveToElement(approvedOrRejectText);
		approvedOrRejectText.click();

		sendKeys("Request Approved", "text.commentarea.approvalcomments");
		NestUtils.waitForLoader();
		QAFExtendedWebElement submitApprovalComment =
				creatElement("button.contains.loc.common", "Submit");
		submitApprovalComment.click();
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(SCROLL_TO);
		jse.executeScript("scroll(0, -250)");
	}
	@QAFTestStep(description = "the request should get {0}")
	public void verifyRejected(String value) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement rejectText =
				creatElement("firsttablevalue.status.td.common", value);

		String status = rejectText.getText();
		if (status.contains("Rejected"))
			Reporter.logWithScreenShot("Request Rejected", MessageTypes.Pass);
		else Reporter.logWithScreenShot("Request not Rejected", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user navigates to travel request inside the travel requests list {0}")
	public void navigateToTravelRequest(String key) {
		NestUtils.waitForLoader();
		QAFExtendedWebElement travelPurpose = creatElement(REQUEST_FROM_TABLE, key);
		travelPurpose.click();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(SCROLL_TO);

	}
	@QAFTestStep(description = "User should be navigated to travel details page.")
	public void verifyTravelRequestDetailsPage() {
		NestUtils.waitForLoader();
		QAFExtendedWebElement travelRequestDetailsHeading =
				creatElement("all.text.common", "Travel Request Details");
		travelRequestDetailsHeading.verifyPresent();
	}

	@QAFTestStep(description = "user verifies the breadcrumb on travel request page {0}")
	public void verifyBreadcrumbOfTravelRequestPage(String title) {
		NestUtils.waitForLoader();

		QAFExtendedWebElement breadcrumb1 = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(
						TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE), 1));
		QAFExtendedWebElement breadcrumb2 = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(
						TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE), 2));
		String b1 = breadcrumb1.getText();
		String b2 = breadcrumb2.getText();
		String breadcrumb = b1 + " " + b2;
		Reporter.log("Received title is" + title);
		Reporter.log("Actual breadcrumb present is" + breadcrumb);

		if (breadcrumb.contains(title)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user clicks on bookings button on travel request details page")
	public void clickOnBookings() {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(SCROLL_TO);
		click("btn.bookings.travelrequestpage");

	}

	@QAFTestStep(description = "User should be navigated to Travel Bookings page")
	public void verifyTravelBookingsPage() {
		NestUtils.waitForLoader();
		verifyPresent("text.travelbookingstext.travelrequestpage");

	}

	@QAFTestStep(description = "user observe the ui of add travel request page")
	public void userObserveTheUiOfAddTravelRequestPage() {
		String color = new QAFExtendedWebElement(BUTTON_SUBMIT_ADDTRAVELREQUESTPAGE)
				.getCssValue(BACKGROUND_COLOR);
		Reporter.log(color);
		Validator.verifyThat(
				ConfigurationManager.getBundle().getString("orange.colorcode"),
				Matchers.containsString(color));
	}

	@QAFTestStep(description = "user observe the ui of my travel request page")
	public void userObserveTheUiOfMyTravelRequestPage() {
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("btn.all.mytravelrequestpage");
		for (QAFWebElement option : options) {
			String color = option.getCssValue(BACKGROUND_COLOR);
			Reporter.log(color);
			Validator.verifyThat(
					ConfigurationManager.getBundle().getString("orange.colorcode"),
					Matchers.containsString(color));
		}
	}

	public void selectDate(String type, String loc, String date) {

		NestUtils.waitForLoader();
		waitForVisible(type, 10000);
		try {
			click(type);
		} catch (Exception e) {
			waitForVisible(type, 10000);
			click(type);
		}
		NestUtils.waitForLoader();
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		try {
			element.click();
		} catch (Exception e) {
			JavascriptExecutor executor =
					(JavascriptExecutor) new WebDriverTestBase().getDriver();
			executor.executeScript(ARGUMENT, element);
		}
	}

	@QAFTestStep(description = "user verify Breadcrumbs should be {0}")
	public void userVerifyBreadcrumbsShouldBe(String breadcrumb) {
		NestUtils.waitForLoader();
		publisherpage.verifyBreadcrumbs("titel.mypreferences.post.publisher", breadcrumb);
	}

	@QAFTestStep(description = "user adds a new travel request using {0}")
	public String addTravelRequest(String key) {
		addtravelrequestbean.fillFromConfig(key);
		NestUtils.waitForLoader();
		new QAFExtendedWebElement("button.logout.userhomepage").isPresent();
		CommonPage.clickUsingJavaScript(MY_NEW_TRAVEL_REQUEST_BTN);

		addtravelrequestbean.fillFromConfig(key);

		NestUtils.waitForLoader();
		sendKeys(addtravelrequestbean.getTravelpurpose(), PURPOSE_OF_TRAVEL_TXTBOX);
		click("dropdown.trip.type.addtravelrequestpage");
		click(DROPDOWN_ROUNDTRIP_ADDTRAVELREQUESTPAGE);
		NestUtils.waitForLoader();
		CommonStep.clear(JOURNEY_FROM_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyfrom(), JOURNEY_FROM_TXTBOX);
		NestUtils.waitForLoader();
		QAFExtendedWebElement ele = new QAFExtendedWebElement(JOURNEY_FROM_TXTBOX);
		ele.sendKeys(Keys.ENTER);
		NestUtils.waitForLoader();
		CommonStep.clear(JOURNEY_TO_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyto(), JOURNEY_TO_TXTBOX);

		QAFExtendedWebElement ele1 = new QAFExtendedWebElement(JOURNEY_TO_TXTBOX);

		ele1.sendKeys(Keys.ENTER);
		NestUtils.waitForLoader();
		CommonPage.waitForLocToBeClickable(JOURNEY_START_DATE);
		selectDate(JOURNEY_START_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(FROM_DATE));
		selectDate(JOURNEY_END_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(TO_DATE));
		sendKeys(addtravelrequestbean.getProjectname(), PROJECT_NAME);
		sendKeys(addtravelrequestbean.getExtrainfo(),
				TEXTBOX_EXTRAINFO_ADDTRAVELREQUESTPAGE);

		NestUtils.waitForLoader();
		QAFExtendedWebElement submitButton =
				creatElement("button.contains.loc.common", "Submit");
		submitButton.click();
		NestUtils.waitForLoader();
		String travelRequestId =
				new QAFExtendedWebElement("text.requestid.fromtable.travelrequestpage")
						.getText();
		Reporter.log(travelRequestId);
		return travelRequestId;

	}

	@QAFTestStep(description = "user performs pagination")
	public void clickPageNumber() {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(SCROLL_TO);
		NestUtils.waitForLoader();
		click(LINK_NEXTPAGE_TRAVELREQUESTPAGE);
		if (new QAFExtendedWebElement("lnk.number.travelrequestpage").isEnabled())
			Reporter.logWithScreenShot("Pagination successful", MessageTypes.Pass);
		else Reporter.logWithScreenShot("Pagination failed", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user selects the record")
	public void clickBookingsButton() {
		CommonPage.clickUsingJavaScript("lnk.travelpurpose.travelrequestpage");
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript(SCROLL_TO);
		CommonPage.clickUsingJavaScript("btn.bookings.travelrequestpage");
	}

	@QAFTestStep(description = "user verifies bookings listing page")
	public void verifyBookingsListingPage() {

		CommonPage.appendWithBaseURL("travelbookings");

	}

	@QAFTestStep(description = "user fill {0} detail and click on submit button")
	public void userFillDetailAndClickOnSubmitButton(String key) {

		addtravelrequestbean.fillFromConfig(key);

		NestUtils.waitForLoader();
		sendKeys(addtravelrequestbean.getTravelpurpose(), PURPOSE_OF_TRAVEL_TXTBOX);
		click("dropdown.trip.type.addtravelrequestpage");
		click(DROPDOWN_ROUNDTRIP_ADDTRAVELREQUESTPAGE);
		NestUtils.waitForLoader();
		CommonStep.clear(JOURNEY_FROM_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyfrom(), JOURNEY_FROM_TXTBOX);
		NestUtils.waitForLoader();
		QAFExtendedWebElement ele = new QAFExtendedWebElement(JOURNEY_FROM_TXTBOX);
		ele.sendKeys(Keys.ENTER);
		NestUtils.waitForLoader();
		CommonStep.clear(JOURNEY_TO_TXTBOX);
		sendKeys(addtravelrequestbean.getJourneyto(), JOURNEY_TO_TXTBOX);

		QAFExtendedWebElement ele1 = new QAFExtendedWebElement(JOURNEY_TO_TXTBOX);

		ele1.sendKeys(Keys.ENTER);
		NestUtils.waitForLoader();
		CommonPage.waitForLocToBeClickable(JOURNEY_START_DATE);
		selectDate(JOURNEY_START_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(FROM_DATE));
		selectDate(JOURNEY_END_DATE, DATEPICKER_DATE,
				ConfigurationManager.getBundle().getString(TO_DATE));
		sendKeys(addtravelrequestbean.getProjectname(), PROJECT_NAME);
		sendKeys(addtravelrequestbean.getExtrainfo(),
				TEXTBOX_EXTRAINFO_ADDTRAVELREQUESTPAGE);

		click(BUTTON_SUBMIT_ADDTRAVELREQUESTPAGE);
		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains(PAGE_URL)) {
			Reporter.logWithScreenShot("User is navigated to add Travel request page",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("User not navigate to Travel details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user sends the data {0} to {1} fields")
	public void userSendsTheDataToMandatoryFields(String dataList,
			String attributeValue) {
		airlineBean.fillFromConfig(dataList);
		NestUtils.waitForLoader();
		String[] fields = NestUtils.dataSplitting(attributeValue);
		NestUtils.waitForLoader();
		CommonStep.clear(fields[0]);
		sendKeys(airlineBean.getAirLine(), fields[0]);
		CommonStep.clear(fields[1]);
		sendKeys(airlineBean.getMemberShip(), fields[1]);
		CommonStep.clear(fields[2]);
		sendKeys(airlineBean.getMemberShip(), fields[2]);
	}
	@QAFTestStep(description = "user verifies page navigation in travel page")
	public void userVerifiesPageNavigationInTravelRequestPage() {
		NestUtils.waitForLoader();
		List<QAFWebElement> pageNumber =
				NestUtils.getQAFWebElements("number.page.travelpage");

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(WINDOW_SCROLLTO);

		for (int i = 0; i < pageNumber.size(); i++) {
			pageNumber.get(i).click();
		}
	}

	public void verifyAddBooking(String loc, String key) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
		element.verifyPresent();

	}

	@QAFTestStep(description = "user should be able to see the forms depening upon the type of booking {0} {0}")
	public void verifyBookingForms(String bookingType, String key) {

		if (bookingType.contains("Hotel")) {
			CommonPage.clickUsingJavaScript(TYPE_ADD_TRAVEL_BOOKING);

			QAFExtendedWebElement hotelType =
					creatElement(TEXT_CONTAINS_LOC_COMMON, bookingType);
			hotelType.click();
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_IN_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_OUT_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, BOOKING_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AMMOUNT);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TAX_AMMOUNT);
			verifyPresent(AGENT_ADD_TRAVEL_BOOKINGS);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AGENT_FEE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TICKET_NUMBER);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, ATTACHMENTS);
		}
		if (bookingType.contains("Cab")) {
			CommonPage.clickUsingJavaScript(TYPE_ADD_TRAVEL_BOOKING);
			QAFExtendedWebElement cabType =
					creatElement(TEXT_CONTAINS_LOC_COMMON, bookingType);
			cabType.click();
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_IN_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_OUT_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, BOOKING_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AMMOUNT);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TAX_AMMOUNT);
			verifyPresent(AGENT_ADD_TRAVEL_BOOKINGS);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AGENT_FEE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, "locationPickup");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, "locationDrop");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TICKET_NUMBER);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, ATTACHMENTS);

		}

		if (bookingType.contains("Journey")) {

			CommonPage.clickUsingJavaScript(TYPE_ADD_TRAVEL_BOOKING);
			QAFExtendedWebElement journeyType =
					creatElement(TEXT_CONTAINS_LOC_COMMON, bookingType);
			journeyType.click();
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_IN_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_OUT_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, BOOKING_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AMMOUNT);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TAX_AMMOUNT);
			verifyPresent(AGENT_ADD_TRAVEL_BOOKINGS);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AGENT_FEE);
			verifyPresent("dropdown.modeoftravel.addtravelbookings.travelrequestpage");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, "journeyfrom");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, "journeyTo");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TICKET_NUMBER);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, ATTACHMENTS);

		}

		if (bookingType.contains("Travel Insurance")) {
			CommonPage.clickUsingJavaScript(TYPE_ADD_TRAVEL_BOOKING);
			QAFExtendedWebElement insuranceType =
					creatElement(TEXT_CONTAINS_LOC_COMMON, bookingType);
			insuranceType.click();
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_IN_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, CHECK_OUT_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, BOOKING_DATE);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, AMMOUNT);
			verifyPresent(AGENT_ADD_TRAVEL_BOOKINGS);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, TICKET_NUMBER);
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, "companyName");
			verifyAddBooking(FIELDS_ADD_TRAVEL_BOOKINGS, ATTACHMENTS);
		}

	}

	@QAFTestStep(description = "user should be able to navigate to desired page using pagination")
	public void verifyPaginationForTravelRequestPage() {
		NestUtils.waitForLoader();
		List<QAFWebElement> pageNumber = NestUtils.getQAFWebElements("number.page.expensepage");
		if (pageNumber.size() > 1) {
			for (int i = 0; i < pageNumber.size(); i++) {
				NestUtils.scrollUpToElement(pageNumber.get(i));
				pageNumber.get(i).click();
				CommonPage.clickUsingJavaScript("link.firstpage.visarequests.visa");
				CommonPage.clickUsingJavaScript("link.lastpage.visarequests.visa");
			}
		} else {
			Reporter.log("page number size" + pageNumber.size());
			Reporter.logWithScreenShot("multiple pages are not present",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user select from date {0} from calender of add travel request page")
	public void userSelectFromDateFromCalenderOfAddTravelRequestPage(String date) {
		selectDate(JOURNEY_START_DATE, "datepicker.loc", date);
	}

	@QAFTestStep(description = "user select to date {0} from calender  of add travel request page")
	public void userSelectToDateFromCalenderOfAddTravelRequestPage(String date) {
		selectDate(JOURNEY_END_DATE, "datepicker.loc", date);
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "User click on add new button of masters page")
	public void userClickOnAddNewButtonOfMastersPage() {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(
				CommonPage.buttonTextContains(BUTTON_ADD_NEW));
	}
	@SuppressWarnings("static-access")
	public String getRandomString(int count) {
		RandomStringUtils util = new RandomStringUtils();
		return util.randomAlphabetic(count);
	}
	@QAFTestStep(description = "user fill {0} required details of master page")
	public void userFillRequiredDetailsOfMasterPage(String key) {
		String comment = getRandomString(6);
		addtravelrequestbean.fillFromConfig(key);
		sendKeys(comment, TEXTBOX_TRAVELTITLE_TRAVELMASTERPAGE);
		sendKeys(comment, "txtbox.travel.description.travelmasterpage");
		sendKeys("2", CommonPage.inputNameContains(TRAVELCATEGORYLEVEL));
		NestUtils.waitForLoader();
		click(BUTTON_MODEOFTRAVEL_TRAVELMASTERPAGE);
		waitForVisible("category.modeoftravel.travelmasterpage");
		List<QAFWebElement> modeOfTravel =
				NestUtils.getQAFWebElements("category.modeoftravel.travelmasterpage");
		for (int i = 0; i < modeOfTravel.size(); i++) {
			if (modeOfTravel.get(i).isDisplayed()) {
				modeOfTravel.get(i).click();
				Reporter.logWithScreenShot("Mode of travel Selected is........" + i);
			}
		}
	}

	public void selectFromList(String locOfDropdown, String locOfList, String option) {
		waitForVisible(locOfDropdown);
		CommonPage.clickUsingJavaScript(locOfDropdown);
		List<QAFWebElement> list = NestUtils.getQAFWebElements(locOfList);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().contains(option)) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript(CommonPage.ARGUMENT, list.get(i));
				break;
			}
		}
	}

	@QAFTestStep(description = "user select {0} from  Dropdown of master page")
	public void userSelectFromDropdownOfMasterPage(String valueToSelect) {
		waitForVisible("btn.designation.travelmasterpage");
		click("btn.designation.travelmasterpage");
		waitForVisible("category.designation.travelmasterpage");
		List<QAFWebElement> modeOfTravel = NestUtils.getQAFWebElements("category.designation.travelmasterpage");
		for (int i = 0; i < modeOfTravel.size(); i++) {
			if (modeOfTravel.get(i).getText().contains(valueToSelect)) {
				modeOfTravel.get(i).sendKeys(Keys.ENTER);
				Reporter.logWithScreenShot("Selected is........" + i);
				break;
			}
		}
	}

	@QAFTestStep(description = "user select {0} from status Dropdown list of master page")
	public void userSelectFromStatusDropdownListOfMasterPage(String valueToSelect) {
		selectFromList(BUTTON_STATUS_TRAVELMASTERPAGE, "category.status.travelmasterpage",
				valueToSelect);
	}

	@QAFTestStep(description = "user select {0} from  Dropdown for mode of travel of master page")
	public void userSelectFromDropdownForModeOfTravelOfMasterPage(String valueToSelect) {
		selectFromList(BUTTON_MODEOFTRAVEL_TRAVELMASTERPAGE,
				"category.designation.travelmasterpage", valueToSelect);
	}

	public void verifyBorderColor(String field) {

		String color = new QAFExtendedWebElement(field).getCssValue(BORDER_COLOR);
		Reporter.log(color);
		if ((color.contains("rgb(255, 0, 0)")) || (color.contains("rgb(242, 53, 53)"))
				|| (color.contains("rgb(204, 204, 204)"))
				|| (color.contains("rgb(51, 51, 51)"))
				|| (color.contains("rgb(237, 237, 237)"))
				|| (color.contains("rgb(253, 7, 7)"))
				|| (color.contains("rgb(249, 26, 26)")))
			Reporter.log("The border color of the field is RED", MessageTypes.Pass);
		else

			Reporter.log("The border color of the field is not RED", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user delete pevious records")
	public void userDeletePeviousRecords() {
		waitForVisible("btn.delete.travel.category.travelmasterpage");
		CommonPage.clickUsingJavaScript("btn.delete.travel.category.travelmasterpage");
		NestUtils.waitForLoader();
		waitForVisible(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
		CommonPage.clickUsingJavaScript(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields using {0} and {1}")
	public void verifyToolTip(String fields, String loc) {

		String[] fieldList = fields.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));

		for (int i = 0; i < fieldList.length; i++) {
			QAFExtendedWebElement makeElement = new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString(loc), fieldList[i]));

			makeElement.sendKeys(Keys.TAB);

			Actions mouseHover = new Actions(driver);

			QAFExtendedWebElement toolTipElement =
					new QAFExtendedWebElement(fieldList[i]);
			mouseHover.moveToElement(toolTipElement).build().perform();

			QAFExtendedWebElement message = new QAFExtendedWebElement(TOOL_TIP);

			String toolTip = message.getText();
			Reporter.log(toolTip);

			if (toolTip.contains(THIS_IS_REQUIRED_FIELD))

				Reporter.logWithScreenShot("Tool Tip message is " + toolTip,
						MessageTypes.Pass);
			else

				Reporter.logWithScreenShot("Tool Tip message is not correct",
						MessageTypes.Fail);

		}
	}

	public void getToolTip(String toolTipFor) {

		Actions mouseHover = new Actions(driver);

		QAFExtendedWebElement toolTipElement = new QAFExtendedWebElement(toolTipFor);
		mouseHover.moveToElement(toolTipElement).build().perform();

		QAFExtendedWebElement message = new QAFExtendedWebElement(TOOL_TIP);

		String toolTip = message.getText();
		Reporter.log(toolTip);

		if (toolTip.contains(THIS_IS_REQUIRED_FIELD))

			Reporter.logWithScreenShot("Tool Tip message is :" + toolTip,
					MessageTypes.Pass);
		else

			Reporter.logWithScreenShot("ToolTip message is not correct",
					MessageTypes.Fail);

	}

	@QAFTestStep(description = "user verifies the breadcrumb on my travel request page {0}")
	public void verifyBreadcrumbOfMyTravelRequestPage(String pageTitle) {
		NestUtils.waitForLoader();
		publisherpage.verifyBreadcrumbs("breadcrumb.add.travel.detailspage", pageTitle);

	}

	@QAFTestStep(description = "user verifies the breadcrumb on my travel request page1 {0}")
	public void verifyBreadcrumbOfMyTravelRequestPage1(String title) {
		NestUtils.waitForLoader();

		QAFExtendedWebElement breadcrumb1 = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(
						TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE), 1));
		QAFExtendedWebElement breadcrumb2 = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(
						TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE), 2));
		QAFExtendedWebElement breadcrumb3 = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(
						TEXT_NEWTRAVELREQUEST_BREADCRUMB_TRAVELREQUESTPAGE), 3));

		String breadcrumb = breadcrumb1.getText();
		String breadc1 = breadcrumb2.getText();
		String breadc2 = breadcrumb3.getText();

		breadcrumb = breadcrumb + breadc1 + breadc2;
		Reporter.log(breadcrumb);
		Reporter.log(title);

		if ((breadcrumb.trim()).equals(title.trim())) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);
		}

	}
	@QAFTestStep(description = "user clicks on {0} yes button for delete confirmation")
	public void userClicksOnYesButtonForDeleteConfirmation(String btn) {
		NestUtils.waitForLoader();
		waitForVisible(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
		CommonPage.clickUsingJavaScript(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
	}

	@QAFTestStep(description = "user click on new travel request")
	public void newTravelRequest() {
		NestUtils.waitForLoader();
		click("text.newtravelrequest.travelrequestpage");
		NestUtils.waitForLoader();

	}

	@QAFDataProvider(key = "traveldata.travelrequest")
	@QAFTestStep(description = "user selects the travel request from the table using {0}")
	public void selectTravelRequstFromTable(String key) {

		QAFExtendedWebElement request = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(REQUEST_FROM_TABLE), key));
		String aaa = request.toString();
		Reporter.log(aaa);
		NestUtils.clickUsingJavaScript(request);
		NestUtils.waitForLoader();
	}

	public QAFExtendedWebElement creatElement(String loc, String key) {

		return new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
	}

	@QAFTestStep(description = "verify the title of page {0}")
	public void userShouldSeeMyTravelRequestPage(String pageTitle) {

		verifyPresent(COMMON_DIV_TEXT);
	}
	public void verifyToolTip(QAFWebElement mandatoryElement) {

		Actions mouseHover = new Actions(driver);

		mouseHover.moveToElement(mandatoryElement).build().perform();

		QAFExtendedWebElement message = new QAFExtendedWebElement(TOOL_TIP);

		String toolTip = message.getText();
		Reporter.log("Tooltip of mandatory field is :" + toolTip);

		if (toolTip.contains(THIS_IS_REQUIRED_FIELD))

			Reporter.logWithScreenShot("Tool Tip message is : " + toolTip,
					MessageTypes.Pass);
		else

			Reporter.logWithScreenShot("ToolTip Message is not correct",
					MessageTypes.Fail);

	}

	@QAFTestStep(description = "user click on edit button")
	public void userClickOnEditButton() {
		waitForVisible("btn.edit.travelmasterpage");
		CommonPage.clickUsingJavaScript("btn.edit.travelmasterpage");
	}

	@QAFTestStep(description = "user clicks on {0} button in airline membership")
	public void userClicksOnButton(String btn) {
		QAFExtendedWebElement ele =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("div.title.contains.common"), btn));
		NestUtils.waitForLoader();
		ele.waitForVisible(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript(ARGUMENT, ele);
		NestUtils.waitForLoader();
	}
	@QAFTestStep(description = "user clicks on {0} save button in airline membership")
	public void userClicksOnSaveButton(String btn) {
		String saveButton = String.format(
				ConfigurationManager.getBundle().getString(SAVE_BUTTON_AIRLINEMEMBERSHIP),
				btn);
		click(saveButton);
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user checks the {0} successful message")
	public void userChecksTheSuccessfulMessage(String messge) {
		QAFExtendedWebElement ele = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(text_Message), messge));
		assertEquals(ele.isDisplayed(), true);
		Reporter.logWithScreenShot("airline details updated successfully",
				MessageTypes.Pass);
	}

	@QAFTestStep(description = "user should checks the {0} deleted message confirmation")
	public void userShouldChecksTheDeletedMessage(String messge) {
		QAFExtendedWebElement ele = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(text_Message), messge));
		assertEquals(ele.isDisplayed(), true);
		Reporter.logWithScreenShot("airline details deleted successfully",
				MessageTypes.Pass);

	}

	@QAFTestStep(description = "user verifies the border color of all the mandatory fields of master page")
	public void userVerifiesTheBorderColorOfAllTheMandatoryFieldsOfMasterPage() {
		verifyBorderColor(TEXTBOX_TRAVELTITLE_TRAVELMASTERPAGE);
		verifyBorderColor(CommonPage.inputNameContains(TRAVELCATEGORYLEVEL));
		verifyBorderColor("btn.designation.travelmasterpage");
		verifyBorderColor(BUTTON_MODEOFTRAVEL_TRAVELMASTERPAGE);
		verifyBorderColor("category.status.travelmasterpage");
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields of master page")
	public void userVerifiesTheMessageWhenMouseHoveredOnTheFieldsOfMasterPage() {
		QAFExtendedWebElement ele =
				new QAFExtendedWebElement(BUTTON_STATUS_TRAVELMASTERPAGE);
		NestUtils.waitForElementToBeClickable(ele);
		getToolTip(BUTTON_STATUS_TRAVELMASTERPAGE);
		getToolTip(CommonPage.inputNameContains(TRAVELCATEGORYLEVEL));
		getToolTip(TEXTBOX_TRAVELTITLE_TRAVELMASTERPAGE);
		getToolTip(BUTTON_MODEOFTRAVEL_TRAVELMASTERPAGE);
		getToolTip(CommonPage.inputNameContains(TRAVELCATEGORYLEVEL));
	}

	@QAFTestStep(description = "User click on {0} button of masters page")
	public void userClickOnButtonOfMastersPage(String heading) {
		QAFExtendedWebElement element1 =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("tab.heading.travelmasterpage"), heading));
		NestUtils.clickUsingJavaScript(element1);

	}

	@QAFTestStep(description = "user fill {0} required details for mode of travel")
	public void userFillRequiredDetailsForModeOfTravel(String key) {
		String comment = getRandomString(20);
		addtravelrequestbean.fillFromConfig(key);
		CommonPage.commonDisplayedLocatorSendKeys(CommonPage.inputNameEquals("title"),
				comment);
		sendKeys(comment, "txtbox.description.mode.of.travel.masterpage");
	}

	@QAFTestStep(description = "User click on add new button on mode of travel of masters page")
	public void userClickOnAddNewButtonOnModeOfTravelOfMastersPage() {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(
				CommonPage.buttonTextContains(BUTTON_ADD_NEW));
	}

	@QAFTestStep(description = "user delete pevious records of mode of travel")
	public void userDeletePeviousRecordsOfModeOfTravel() {
		CommonPage.clickUsingJavaScript("btn.delete.mode.of.travel.masterpage");
		NestUtils.waitForLoader();
		waitForVisible(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
		CommonPage.clickUsingJavaScript(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
	}

	@QAFTestStep(description = "User click on submit button of mode of travel page")
	public void userClickOnSubmitButtonOfModeOfTravelPage() {
		CommonPage.clickUsingJavaScript("btn.submit.mode.of.travel.masterpage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies the border color of all the mandatory fields of mode of travel")
	public void userVerifiesTheBorderColorOfAllTheMandatoryFieldsOfModeOfTravel() {
		verifyBorderColor("txtbox.travel.title.modeoftravel.masterpage");
		verifyBorderColor("btn.status.mode.of.travel.masterpage");
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields of mode of travel")
	public void userVerifiesTheMessageWhenMouseHoveredOnTheFieldsOfModeOfTravel() {

		getToolTip("txtbox.travel.title.modeoftravel.masterpage");
		getToolTip("btn.status.mode.of.travel.masterpage");

	}

	@QAFTestStep(description = "user fill {0} required details for travel agent")
	public void userFillRequiredDetailsForTravelAgent(String key) {
		String comment = getRandomString(9);
		addtravelrequestbean.fillFromConfig(key);
		sendKeys(comment, CommonPage.inputNameContains(TRAVELAGENTNAME));
		sendKeys(comment, CommonPage.inputNameContains("travelAgentContactPerson"));
		sendKeys("0123456789", CommonPage.inputNameContains("travelAgentContactNumber"));
	}

	@QAFTestStep(description = "user verifies pagination for Travel locations")
	public void userVerifiesPaginationForTravelLocations() {
		List<QAFWebElement> pageNumber = NestUtils
				.getQAFWebElements("lable.pageNumberList.travelLocationTravelPage");

		if (pageNumber.size() > 1) {
			for (int i = 2; i < pageNumber.size(); i++) {
				String page = CommonPage.getStringLocator(
						"lable.pageNumber.travelLocationTarvelPage", Integer.toString(i));
				CommonPage.clickUsingJavaScript(page);

			}
			Reporter.logWithScreenShot("User able to navigate to through out the pages",
					MessageTypes.Pass);
		} else {
			Reporter.log("Sufficeint data is not present.Adding data to Travle location");
			for (int i = 1; i < 20; i++) {
				addNewTravelLocation(getRandomString(5), "Enable");
			}
			List<QAFWebElement> newpageNumber = NestUtils
					.getQAFWebElements("lable.pageNumber.travelLocationTravelPage");
			if (newpageNumber.size() > 1) {
				for (int i = 2; i < newpageNumber.size(); i++) {
					newpageNumber.get(i).click();
				}
				Reporter.logWithScreenShot(
						"User able to navigate to through out the pages",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"User is not able to navigate to through out the pages",
						MessageTypes.Fail);

			}
		}
	}

	private void addNewTravelLocation(String travelTitle, String str1) {
		NestUtils.waitForLoader();
		CommonPage.commonDisplayedLocatorClick(
				CommonPage.buttonTextContains(BUTTON_ADD_NEW));
		CommonPage.commonDisplayedLocatorSendKeys(CommonPage.inputNameEquals("title"),
				travelTitle);
		CommonStep.click(DROPDOWN_STATUS_TRAVELLOCATION_TRAVELPAGE);
		String status = CommonPage
				.getStringLocator("dropdown.selectStatus.travelLocationTravelpage", str1);
		CommonStep.click(status);
		CommonStep.click("button.submit.travelLocationTravelPage");
		NestUtils.waitForLoader();

	}

	@QAFTestStep(description = "User click on edit button on travel agent of masters page")
	public void userClickOnEditButtonOnTravelAgentOfMastersPage() {
		CommonPage.clickUsingJavaScript("btn.edit.travel.agent.masterpage");
	}

	@QAFTestStep(description = "user verifies heading on Airline Membership page")
	public void verifyHeadingAirlineMembership() {
		NestUtils.waitForLoader();
		verifyPresent(CommonPage.elementTextEquals("Airline Membership"));
	}

	@QAFTestStep(description = "user verifies the breadcrumb on Airline Membership page using {0}")
	public void verifyBreadcrumbOfAirlineMembershipPage(String title) {
		NestUtils.waitForLoader();
		publisherpage.verifyBreadcrumbs("breadcrumb.airline.membershippage", title);
	}

	@QAFTestStep(description = "User click on delete button on travel agent of masters page")
	public void userClickOnDeleteButtonOnTravelAgentOfMastersPage() {
		CommonPage.clickUsingJavaScript("btn.delete.travel.agent.masterpage");
		NestUtils.waitForLoader();
		waitForVisible(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
		CommonPage.clickUsingJavaScript(BUTTON_DELETE_POPUP_TRAVELMASTERPAGE);
	}

	@QAFTestStep(description = "User click on edit button on mode of travel of masters page")
	public void userClickOnEditButtonOnModeOfTravelOfMastersPage() {
		CommonPage.clickUsingJavaScript("btn.edit.mode.of.travel.masterpage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies the border color of all the mandatory fields of travel agents")
	public void userVerifiesTheBorderColorOfAllTheMandatoryFieldsOfTravelAgents() {

		verifyBorderColor(CommonPage.inputNameContains(TRAVELAGENTNAME));
		verifyBorderColor("btn.select.status.travel.agent.masterpage");
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields of travel agents")
	public void userVerifiesTheMessageWhenMouseHoveredOnTheFieldsOfTravelAgents() {
		getToolTip(CommonPage.inputNameContains(TRAVELAGENTNAME));
		getToolTip("btn.select.status.travel.agent.masterpage");
	}

	@QAFTestStep(description = "user verifies the message when mouse hover on the mandatory fields of travel using {0} and {1}")
	public void userVerifiesTheMessageWhenMouseHoverOnTheMandatoryFieldsOfTravelAgents(
			String fields, String loc) {

		String[] fieldList = fields.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));

		for (int i = 0; i < fieldList.length; i++) {
			QAFExtendedWebElement makeElement = new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString(loc), fieldList[i]));
			verifyToolTip(makeElement);
		}
	}

	@QAFTestStep(description = "User click on submit button of travel master")
	public void userClickOnSubmitButtonOfTravelMaster() {
		CommonPage.clickUsingJavaScript("btn.submit.travel.agent.masterpage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "User click on add new button of travel agents of masters page")
	public void userClickOnAddNewButtonOfTravelAgentsOfMastersPage() {
		CommonPage.clickUsingJavaScript("btn.add.new.travel.agents.masterpage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify filter icon")
	public void userVerifyFilterIcon() {
		Validator.verifyThat("validating employee dropdown",
				verifyPresent("icon.filter.airlineMembershipPage"), Matchers.is(true));
	}

	@QAFTestStep(description = "user verify Employee Drop down")
	public void userVerifyEmployeeDropDown() {
		Validator.verifyThat("validating employee dropdown",
				verifyPresent(DROPDOWN_EMPLOYEE_AIRLINEMEMBERSHIP), Matchers.is(true));
	}
	@QAFTestStep(description = "user selects {0} from employee Dropdown on Airline Membership page")
	public void userSelectsEmployee(String valueToSelect) {
		NestUtils.waitForLoader();
		click(DROPDOWN_EMPLOYEE_AIRLINEMEMBERSHIP);
		waitForVisible(LIST_EMPLOYEENAME_AIRLINEMEMBERSHIP);
		List<QAFWebElement> employee = NestUtils
				.getQAFWebElements("list.dropdownEmployeeName.airlineMembershipPage");
		for (int i = 0; i < employee.size(); i++) {
			if (employee.get(i).getText().contains(valueToSelect)) {
				employee.get(i).click();
				break;
			}
		}
	}
	@QAFTestStep(description = "user verify search result for employee as {0}")
	public void userVerifySearchResultForEmployeeAs(String str0) {
		NestUtils.waitForLoader();
		try {
			List<QAFWebElement> totalEmployee =
					NestUtils.getQAFWebElements(LIST_EMPLOYEENAME_AIRLINEMEMBERSHIP);
			for (int i = 0; i < totalEmployee.size(); i++) {

				if (totalEmployee.get(i).isDisplayed()) {
					if (!totalEmployee.get(i).getText().equalsIgnoreCase(str0)) {
						Reporter.logWithScreenShot("Search result is incorrect.Actual: "
								+ totalEmployee.get(i).getText() + "Expected: " + str0,
								MessageTypes.Fail);
					} else {
						Reporter.logWithScreenShot("Search result is correct.Actual: "
								+ totalEmployee.get(i).getText() + "Expected: " + str0,
								MessageTypes.Pass);
					}
				}
			}
		} catch (Exception e) {
			Reporter.log("No data found", MessageTypes.Warn);
		}
	}

	@QAFTestStep(description = "user fill the travel location form")
	public void userFillTheTravelLocationForm() {
		addNewTravelLocation(getRandomString(5), "Enable");
	}

	@QAFTestStep(description = "user search for {0} record")
	public void userSearchForRecord(String travelTitle) {
		NestUtils.waitForLoader();
		CommonStep.sendKeys(travelTitle, TEXTBOX_SEARCHTITLE_TRAVELLOCATIONPAGE);
	}

	@QAFTestStep(description = "user verify for {0} record")
	public void userVerifyForRecord(String titleToVerify) {
		NestUtils.waitForLoader();
		String travelTitle =
				new QAFExtendedWebElement(TEXTBOX_SEARCHTITLE_TRAVELLOCATIONPAGE)
						.getText();
		Validator
				.verifyThat(
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("record.to.search.travellocation"),
								titleToVerify)).getText().contains(travelTitle),
						Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user click on reset button of travel location")
	public void userClickOnResetButtonOfTravelLocation() {
		CommonStep.sendKeys("testData", TEXTBOX_SEARCHTITLE_TRAVELLOCATIONPAGE);
		NestUtils.waitForLoader();
		CommonStep.click(RESET_BUTTON);
		String title = new QAFExtendedWebElement(TEXTBOX_SEARCHTITLE_TRAVELLOCATIONPAGE)
				.getText();
		if (title.contains(""))
			Reporter.logWithScreenShot("The filter is reset");
		else Reporter.logWithScreenShot("The fileter is not reset");
	}

	@QAFTestStep(description = "user verify plus sign on page")
	public void userVerifySignOnPage() {
		Validator.verifyThat("validating +  sign on page",
				verifyPresent("sign.employeeName.airlineMembershipPage"),
				Matchers.is(true));

	}

	@QAFTestStep(description = "user verify search result")
	public void userVerifyResetSearchResult() {
		NestUtils.waitForLoader();
		try {
			List<QAFWebElement> totalEmployee =
					NestUtils.getQAFWebElements(LIST_EMPLOYEENAME_AIRLINEMEMBERSHIP);
			for (int i = 0; i < totalEmployee.size(); i++) {

				if (!totalEmployee.get(i).isDisplayed()) {
					Reporter.logWithScreenShot("Search result are Not displayed for "
							+ totalEmployee.get(i).getText(), MessageTypes.Fail);
				} else {
					Reporter.logWithScreenShot("Search result are displayed for"
							+ totalEmployee.get(i).getText(), MessageTypes.Pass);
				}
			}

		} catch (Exception e) {
			Reporter.log("No data found", MessageTypes.Warn);
		}

	}

	@QAFTestStep(description = "user verifies filter is hidden")
	public void userVerifyFilter() {
		NestUtils.waitForLoader();
		Validator.verifyThat("validating employee drop down",
				CommonStep.verifyNotVisible(DROPDOWN_EMPLOYEE_AIRLINEMEMBERSHIP),
				Matchers.is(true));
	}

	@QAFTestStep(description = "user clicks on filter icon on AM page")
	public void userClickOnFilterIcon() {
		CommonStep.click("icon.filter.airlineMembershipPage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify option of action buttons")
	public void verifyActionButtons() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript(WINDOW_SCROLLTO);
		Validator.verifyThat("verify option of action buttons",
				verifyPresent(CommonPage.buttonTextContains("Approve")),
				Matchers.equalTo(true));
		Validator.verifyThat("verify option of action buttons",
				verifyPresent(CommonPage.buttonTextContains("Reject")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify user should be able to take action using this action buttons")
	public void verifyActionButtonsEnable() {

		Validator.verifyThat("able to take action using this action buttons",
				CommonStep.verifyEnabled(CommonPage.buttonTextContains("Approve")),
				Matchers.equalTo(true));
		Validator.verifyThat("able to take action using this action buttons",
				CommonStep.verifyEnabled(CommonPage.buttonTextContains("Reject")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies pagination for Airline Membership")
	public void userVerifyPaginationForAMPage() {
		NestUtils.waitForLoader();

		if (new QAFExtendedWebElement(LINK_NEXTPAGE_TRAVELREQUESTPAGE).isEnabled()) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript(SCROLL_TO);
			NestUtils.waitForLoader();
			click(LINK_NEXTPAGE_TRAVELREQUESTPAGE);
			Reporter.logWithScreenShot("Pagination successful", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Pagination failed", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on {0} page")
	public void userClickOnPage(String pageNumber) {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) NestUtils.getDriver();
		jse.executeScript(WINDOW_SCROLLTO);
		QAFExtendedWebElement pagenumberLink =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("lnks.pagination.travelrequestpage"), pageNumber));
		pagenumberLink.click();
		NestUtils.waitForLoader();
		QAFExtendedWebElement activepagenumber =
				new QAFExtendedWebElement(
						String.format(
								ConfigurationManager.getBundle().getString(
										"lnk.activepagenumber.travelrequestpage"),
								pageNumber));
		activepagenumber.verifyPresent();

	}

	@QAFTestStep(description = "user verify {0} button invisible and {1} is present")
	public void userVerifyButtonInvisibleAndIsPresent(String btn, String table) {
		NestUtils.waitForLoader();
		CommonStep.verifyNotVisible(btn);
		CommonStep.verifyVisible(table);

	}

	@QAFTestStep(description = "user verify table column header of airline membership")
	public void userVerifyTableColumnHeaderOfAirlineMembership() {
		for (int i = 0; i < tableHeader().size(); i++) {
			tableHeader().get(i).verifyPresent();
		}
		Validator.verifyThat("Airline field must be : ", tableHeader().get(0).getText(),
				Matchers.equalToIgnoringCase("Airline"));
		Validator.verifyThat("Airline field must be : ", tableHeader().get(1).getText(),
				Matchers.equalToIgnoringCase("Category"));
		Validator.verifyThat("Airline field must be : ", tableHeader().get(2).getText(),
				Matchers.equalToIgnoringCase("Posted On"));
		Validator.verifyThat("Airline field must be : ", tableHeader().get(3).getText(),
				Matchers.equalToIgnoringCase("Action"));
	}
	public List<QAFWebElement> tableHeader() {
		return NestUtils.getQAFWebElements("table.header.mypost.publish");
	}

	@QAFTestStep(description = "user click on {0} button of master page")
	public void userClickOnButtonOfMasterPage(String button) {
		NestUtils.waitForLoader();
		CommonPage.clickUsingJavaScript(
				CommonPage.getStringLocator("btn.active.title.common.loc", button));
	}

	@QAFTestStep(description = "user verify deleted record of master")
	public void userVerifyDeletedRecordOfMaster() {
		userDeletePeviousRecordsOfModeOfTravel();
	}

	@QAFTestStep(description = "User click on {0} button of travel master page")
	public void userClickOnButtonOfTravelMasterPage(String button) {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(WINDOW_SCROLLTO);
		CommonPage.clickUsingJavaScript(
				CommonPage.getStringLocator("btn.active.text.common.loc", button));
	}

	@QAFTestStep(description = "user verifies the border color of all the mandatory fields of travel locations")
	public void userVerifiesTheBorderColorOfAllTheMandatoryFieldsOfTravelLocations() {
		verifyBorderColor(CommonPage.inputNameEquals("title"));
		verifyBorderColor(DROPDOWN_STATUS_TRAVELLOCATION_TRAVELPAGE);
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields of travel locations")
	public void userVerifiesTheMessageWhenMouseHoveredOnTheFieldsOfTravelLocations() {
		NestUtils.waitForLoader();
		getToolTip(CommonPage.inputNameEquals("title"));
		getToolTip(DROPDOWN_STATUS_TRAVELLOCATION_TRAVELPAGE);

	}
	@QAFTestStep(description = "user clicks on New Travel Request button")
	public void clickOnNewTravelRequestButton() {
		NestUtils.waitForLoader();
		CommonPage.scrollUpToElement(NEW_TRAVEL_REQUEST_BTN);
		CommonStep.click(NEW_TRAVEL_REQUEST_BTN);
	}

	@QAFTestStep(description = "user verifies the title on new travel request page {0}")
	public void verifyPageTitleOnNewTravelRequestPage(String title) {
		NestUtils.waitForLoader();
		CommonPage.verifyTitle(title);

	}

	@QAFTestStep(description = "user verify Journey To Textbox is present")
	public void verifyJourneyToTextboxIsPresent() {
		CommonStep.verifyPresent(JOURNEY_MGR_TO_TXTBOX);
	}

	@QAFTestStep(description = "user verifies navigated page {0}")
	public void verfyTheNavigatedPage(String key) {
		NestUtils.waitForLoader();
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains(key)) {
			Reporter.logWithScreenShot("User navigated to " + currentUrl + " page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User not navigated to " + currentUrl + " page",
					MessageTypes.Fail);
		}

	}

	@QAFTestStep(description = "user verify Journey From Textbox is present")
	public void verifyJourneyFromTextboxIsPresent() {
		CommonStep.verifyPresent(JOURNEY_MGR_FROM_TXTBOX);
	}

	@QAFTestStep(description = "user verify Enter Purpose of travel {0} Textbox is present")
	public void verifyPurposeOfTravelTextboxIsPresent(String value) {
		QAFExtendedWebElement puposeOfTravelTextBox =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(TEXT_INPUTTEXT_LOC_COMMON), value));

		puposeOfTravelTextBox.isPresent();
	}

	@QAFTestStep(description = "user verify Enter Project Name {0} Textbox is present")
	public void verifyProjectNameTextboxIsPresent(String value) {
		QAFExtendedWebElement projectNametextBox =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(TEXT_INPUTTEXT_LOC_COMMON), value));

		projectNametextBox.isPresent();
	}

	@QAFTestStep(description = "user verify Calender Journey Start date {0}  is present")
	public void verifyCalenderJourneyStartDateIsPresent(String startDate) {
		QAFExtendedWebElement calenderJourneyStartDate =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(TEXT_INPUTTEXT_LOC_COMMON), startDate));
		calenderJourneyStartDate.isPresent();
	}

	@QAFTestStep(description = "user verify Calender Journey End date {0} is present")
	public void verifyCalenderJourneyEndDateIsPresent(String endDate) {
		QAFExtendedWebElement calenderJourneyEndDate =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(TEXT_INPUTTEXT_LOC_COMMON), endDate));
		calenderJourneyEndDate.isPresent();
	}

	@QAFTestStep(description = "user verify radio button {0} is present")
	public void verifyRadioButtonIsPresent(String radioButtonName) {
		List<QAFWebElement> listRadioButton =
				NestUtils.getQAFWebElements(String.format(
						ConfigurationManager.getBundle()
								.getString("list.radiButtons.mgr.newTravelRequest"),
						radioButtonName));

		listRadioButton.get(0).isPresent();
		listRadioButton.get(1).isPresent();

	}

	@QAFTestStep(description = "user verify text area {0} is present")
	public void verifyTextAreaIsPresent(String value) {
		QAFExtendedWebElement textArea =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("textArea.mgr.newTravelRequest"), value));
		textArea.isPresent();
	}

	@QAFTestStep(description = "user verify browse file {0} is present")
	public void verifyBrowseFileIsPresent(String value) {

		QAFExtendedWebElement browseFileArea = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("div.title.contains.common"),
				value));
		browseFileArea.isPresent();

	}

	@QAFTestStep(description = "user verify submit button is present")
	public void verifySubmitButtonIsPresent() {
		CommonStep.verifyPresent(BUTTON_SUBMIT_ADDTRAVELREQUESTPAGE);
	}

	@QAFTestStep(description = "user verify back button is present")
	public void verifyCancelButtonIsPresent() {
		CommonStep.verifyPresent("btn.clear.addtravelrequestpage");
	}

	@QAFTestStep(description = "user select {0} from Dropdown catagory")
	public void userSelectFromDropdownCatagory(String valueToSelect) {
		NestUtils.waitForLoader();
		waitForVisible(CATEGORY_DROPDOWN);
		CommonPage.clickUsingJavaScript(CATEGORY_DROPDOWN);
		List<QAFWebElement> modeOfTravel =
				NestUtils.getQAFWebElements("catagory.dropdown.newpost.publisher");
		for (int i = 0; i < modeOfTravel.size(); i++) {
			if (modeOfTravel.get(i).isDisplayed()) {
				modeOfTravel.get(i).sendKeys(Keys.ENTER);
				Reporter.logWithScreenShot("Selected is........" + i);
				break;
			}
		}
	}

	@QAFTestStep(description = "user verifies the title of page should be {0}")
	public void userVerifiesTheTitleOfPageShouldBe(String title) {
		NestUtils.waitForLoader();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript(SCROLL_TO);
		jse.executeScript("scroll(0, -250)");
		String currentUrl = driver.getCurrentUrl();
		Reporter.log(currentUrl);
		if (currentUrl.contains(title)) {
			Reporter.logWithScreenShot("verify Titel of the page is verified",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("verify Titel of the page is not verified",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify datepicker label")
	public void userVerifyDatepickerLabel() {
		Validator.verifyThat("Check in Date datePicker",
				verifyEnabled("end.date.applyleavepage"), Matchers.equalTo(true));
		Validator.verifyThat("Check out Date datePicker",
				verifyEnabled("from.date.applyleavepage"), Matchers.equalTo(true));
		Validator.verifyThat("Booking date datePicker",
				verifyEnabled("booking.date.addTravelBookings"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify browse file")
	public void userVerifyBrowseFile() {
		Validator.verifyThat("Browse file :",
				verifyEnabled("attachment.browse.file.addTravelBookins"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies the message when mouse hovered on the fields of add travel request page")
	public void userVerifiesTheMessageWhenMouseHoveredOnTheFieldsOfAddTravelRequestPage() {
		QAFExtendedWebElement ele =
				new QAFExtendedWebElement("from.date.applyleavepage");
		NestUtils.waitForElementToBeClickable(ele);
		getToolTip(CommonPage.buttonTextContains("Select"));
		getToolTip("from.date.applyleavepage");
		
		getToolTip("end.date.applyleavepage");
	}

}
