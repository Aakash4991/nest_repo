package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.getText;

import org.hamcrest.Matchers;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;

public class MyProfilePage {
	RR_RequestPage rrrequest = new RR_RequestPage();
	private static final String REWARD = "reward.type";

	@QAFTestStep(description = "verify band of employee is {0}", stepName = "verifyBandOfEmployeeIs")
	public void verifyBandOfEmployeeIs(String bandnumber) {
		Validator.verifyThat("Band " + bandnumber + "is visible",
				getText("band.name").toString().trim().contains(bandnumber), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies options for nominations for {0} user")
	public void verifyOptionsForNominations(String bandName) {
		NestUtils.scrollUpToElement(rrrequest.getElement(REWARD, "IFS"));
		NestUtils.waitForLoader();
		if (bandName.equals("Band 6")) {
			rrrequest.getElement(REWARD, "You Made My Day").verifyVisible("You Made My Day");
			rrrequest.getElement(REWARD, "Pat On Back").verifyNotVisible("Pat On Back");
			rrrequest.getElement(REWARD, "Bright Spark").verifyNotVisible("Bright Spark");
		} else if (bandName.equals("Band 5")) {
			rrrequest.getElement(REWARD, "You Made My Day").verifyVisible("You Made My Day");
			rrrequest.getElement(REWARD, "Pat On Back").verifyVisible("Pat On Back");
			rrrequest.getElement(REWARD, "Bright Spark").verifyNotVisible("Bright Spark");
		} else {
			rrrequest.getElement(REWARD, "You Made My Day").verifyVisible("You Made My Day");
			rrrequest.getElement(REWARD, "Pat On Back").verifyVisible("Pat On Back");
			rrrequest.getElement(REWARD, "Bright Spark").verifyVisible("Bright Spark");
		}
	}

}
