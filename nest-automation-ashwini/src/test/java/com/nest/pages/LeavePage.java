package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.clear;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.nest.beans.ApplyLeaveBean;
import com.nest.beans.ApplyTeamLeaveBean;
import com.nest.beans.MyLeaveListBean;
import com.nest.beans.TeamLeaveListTableBean;
import com.nest.components.CalendarComponent;
import com.nest.components.TeamLeaveListTableRowComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LeavePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	CalendarComponent calendar = new CalendarComponent();
	static MyLeaveListBean myLeaveDataBean = new MyLeaveListBean();
	int incrementDate = 0;
	public static final String FROM_DATE = "date.from.common";
	public static final String TO_DATE = "date.to.common";
	public static final String VALUE = "value";
	public static final String LEAVE_REASON = "dropdown.leave.reason.applyleavepage";
	public static final String LEAVE_TYPE = "button.leave.type.applyteamleavepage";
	public static final String ASSIGN_LEAVE_BUTTON = "button.apply.applyteamleavepage";
	public static final String LIST_LEAVE_REASONS =
			"list.options.leavereason.applyteamleavepage";
	public static final String APPLY_LEAVE_BUTTON = CommonPage.buttonTextEquals("Apply");
	public static final String DATE_TYPES = "list.datetype.teamleavelistpage";
	public static final String TEAM_EMPLOYEE_NAME = "button.emp.name.teamleavelistpage";
	public static final String ACCOUNT_BUTTON = "button.account.teamleavelistpage";
	public static final String TEAM_LEAVE_TYPE = "button.leavetype.teamleavelistpage";
	public static final String LEAVE_STATUS = "button.leavestatus.teamleavelistpage";
	public static final String LEAVE_TABLE_ROW = "row.leavetable.teamleavelistpage";
	public static final String STATUS = "label.status.common";
	public static final String MESSAGE_ALREADY_LEAVE_APPLIED = ConfigurationManager
			.getBundle().getString("leave.already.applied.for.selected.date");
	public static final String MESSAGE_CANNOT_BE_LESS =
			ConfigurationManager.getBundle().getString("to.date.cannot.be.less");
	public static final String COMMENT = "input.comment.popup.common";
	public static final String SUBMIT = "button.submit.popup.common";
	public static final String POPUP_MESSAGE = "label.message.popup.common";
	public static final String LEAVE_DURATION = "Leave Duration";
	public static final String LEAVE_DAYS = "text.applyLeave.leaveDay";
	public static final String PRESENT = " is present";
	public static final String NOT_PRESENT = " is not present";
	public static final String ELEMENT_NOT_FOUND = "Element is not found";
	public static final String DROPDOWN_OPTIONS = "link.dropdown.options";
	public static final String HALFDAY_RADIO_BUTTON = "button.radio.dayType";
	public static final String SEARCH_BUTTON = "button.search.teamleavelistpage";
	public static final String RESET_BUTTON = "button.reset.teamleavelist";
	public static final String LEAVE_TYPE_DROPDOWN = "options.leaveTypes.dropdown";
	public static final String EMPLOYEE_NAME_TEXTBOX =
			"textBox.employeeName.LeaveBalance";
	public static final String SEARCH_BUTTON_LEAVE_BALANCE =
			"button.teamLeaveBalance.search";
	public static final String MESSAGE_LEAVE_APPLIED_SUCCESSFUL = ConfigurationManager
			.getBundle().getString("special.leave.applied.successfully");
	public static final String SPECIAL_LEAVE_MESSAGE =
			"special.leave.message";
	public static final String SEARCH_BUTTON_COMMON =CommonPage.buttonTextEquals("Search");
	public static final String LABLE_REQUESTPAGE ="link.RequestsLabel.requestpage";
	public static final String RESET_TEAM_LEAVE_BALANCE ="button.teamLeaveBalance.reset";
	public static final String FILTER_BUTTON ="button.filter.configureLeavePage";
	public static final String DROPDOWN_CONFIGLEAVE ="dropdown.search.configureLeavePage";
	public static final String LIST_CONFIGLEAVE ="list.dates.configureLeavePage";
	public static final String SELCTED_LEAVE="leaveBalance.selectedValues";
	public static final String DROPDOWN_LEAVETYPE="dropdown.leaveType";
	public static final String LEAVEBALANCE_GROUP="leaveBalace.groups";
	public static final String LOCATION_CONFIGLEAVE="checkbox.location.configureLeavePage";
	public static final String BUTTON_CONFIGHOLIDAY="button.addFloatingHoliday.configureLeavePage";
	public static final String CHECKBOX_CONFIGHOLIDAY="checkbox.addfloatingHoliday.configLeavePage";
	public static final String VERIFY_MESG_CONFIG=" is available on configure leave page";
	public static final String VERIFY_NOT_MESG_CONFIG=" is NOT available on configure leave page";
	public static final String LOCATION_AHMEDABAD="Ahmedabad";
	public static final String LOCATION_BENGALURU="Bengaluru";
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// overriden method
	}

	public void verifySection() {
		CommonStep.assertPresent("label.holidaysListSection.userhomepage");
		Reporter.logWithScreenShot("Holidays list section is present showing Holidays");
	}

	@QAFTestStep(description = "user verifies the fields present on Apply Team Leave page")
	public void verifyFieldsOnMyLeaveListPage() {
		verifyPresent("button.emp.name.applyteamleavepage");
		verifyPresent(LEAVE_TYPE);
		verifyPresent("label.leave.balance.applyteamleavepage");
		verifyPresent("button.leave.reason.applyteamleavepage");
		verifyPresent(FROM_DATE);
		verifyPresent(TO_DATE);
		verifyPresent("input.comment.applyteamleavepage");
		verifyPresent(ASSIGN_LEAVE_BUTTON);
	}

	@QAFTestStep(description = "user fill {0} details and assign leave")
	public void applyEmployeeLeave(String key) {
		ApplyTeamLeaveBean data = new ApplyTeamLeaveBean();
		data.fillFromConfig(key);
		click("button.emp.name.applyteamleavepage");
		sendKeys(data.getEmpName(), "input.empname.search.applyteamleavepage");
		click("option.empname.searchresult.applyteamleavepage");
		click(LEAVE_TYPE);
		NestUtils.clickUsingJavaScript(
				select("list.options.leavetype.applyteamleavepage", data.getLeaveType()));
		NestUtils.waitForLoader();
		String balance =
				new QAFExtendedWebElement("label.leave.balance.applyteamleavepage")
						.getAttribute(VALUE);

		Reporter.log("Available PTOs : " + balance, MessageTypes.Info);

		fillToDate(NestUtils.getLeaveDate(2 + Integer.parseInt(data.getDays())));
		fillFromDate(NestUtils.getLeaveDate(2));

		click("button.leave.reason.applyteamleavepage");
		NestUtils.clickUsingJavaScript(select(LIST_LEAVE_REASONS, data.getLeaveReason()));
		if (Integer.parseInt(data.getDays()) == 0)
			NestUtils.clickUsingJavaScript("radio.daytype.leave.applyteamleavepage",
					data.getDayType());
		sendKeys(data.getComment(), "input.comment.applyteamleavepage");
		click(ASSIGN_LEAVE_BUTTON);
		NestUtils.waitForLoader();
		applyLeaveAgain(data.getDays());
	}

	public void fillFromDate(String fromDate) {
		click("icon.calendar.from");
		calendar.selectDate(fromDate);
	}

	public void fillToDate(String toDate) {
		click("icon.calendar.to");
		calendar.selectDate(toDate);
	}

	private QAFWebElement select(String loc, String selOption) {
		QAFWebElement element = null;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().toLowerCase()
					.contentEquals(selOption.toLowerCase())) {
				element = option;
				
				break;
			}
		}
		return element;
	}

	@QAFTestStep(description = "user should fill the form and apply for leave")
	public void userShouldFillTheFormAndApplyForLeave() {
		verifyPresent("from.start.date.applyleavepage");
		verifyPresent("to.end.date.applyleavepage");

		fillFromDate(NestUtils.getLeaveDate(2));
		NestUtils.waitForLoader();
		fillToDate(NestUtils.getLeaveDate(2));
		NestUtils.waitForLoader();
		click(LEAVE_REASON);
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("options.leave.reasons.applyleavepage");
		for (QAFWebElement option : options) {
			if (option.getText().equalsIgnoreCase("")) {
				option.click();
			} else continue;
		}
		CommonPage.waitForLocToBeClickable(APPLY_LEAVE_BUTTON);
		NestUtils.scrollToAxis(0, 250);
		click(APPLY_LEAVE_BUTTON);

	}

	@QAFTestStep(description = "user verifies leaves list table of Team Leave List page")
	public void verifyTableHeadersTeamLeaveListPage() {
		String[] headers = ConfigurationManager.getBundle()
				.getString("teamleavelist.table.headers").split(":");
		for (String header : headers) {
			if (verifyTextPresent("list.headers.leavestable.teamleavelistpage",
					header.trim()))
				Reporter.log(header + PRESENT, MessageTypes.Pass);
			else Reporter.log(header + NOT_PRESENT, MessageTypes.Fail);
		}
	}

	private boolean verifyTextPresent(String loc, String verifyText) {
		boolean present = false;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().contains(verifyText)) {
				present = true;
				break;
			}
		}
		return present;
	}

	@QAFTestStep(description = "user verifies search related fields of Team Leave List page")
	public void verifySearchFieldsTeamLeaveListPage() {
		select(DATE_TYPES, ConfigurationManager.getBundle().getString("datetype"));
		verifyPresent(FROM_DATE);
		verifyPresent(TO_DATE);
		verifyPresent(TEAM_EMPLOYEE_NAME);
		verifyPresent(ACCOUNT_BUTTON);
		verifyPresent(TEAM_LEAVE_TYPE);
		verifyPresent(LEAVE_STATUS);
		verifyPresent(SEARCH_BUTTON);
		verifyPresent(RESET_BUTTON);
	}

	@QAFTestStep(description = "user searches with deatils {0}")
	public void fillSearchCriteriadetails(String searchKey) {
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		select(DATE_TYPES, leavesdatabean.getDateType()).click();
		selectEmployee(leavesdatabean.getEmpName());
		NestUtils.waitForLoader();
		selectProject(leavesdatabean.getProject());
		NestUtils.waitForLoader();
		selectLeaveType(leavesdatabean.getLeaveType());
		selectLeaveStatus(leavesdatabean.getLeaveStatus());
		click(SEARCH_BUTTON);
		NestUtils.waitForLoader();
	}

	public void selectEmployee(String empName) {
		click(TEAM_EMPLOYEE_NAME);
		CommonStep.clear("input.search.emp.name.teamleavelistpage");
		sendKeys(empName, "input.search.emp.name.teamleavelistpage");
		click("list.search.names.emp.name.teamleavelistpage");
	}

	public void selectProject(String projectName) {
		if (!projectName.isEmpty()) {
			click(ACCOUNT_BUTTON);
			sendKeys(projectName, "input.search.project.teamleavelistpage");
			click("list.projectnames.teamleavelistpage");
		}
	}

	private void selectLeaveType(String leaveType) {
		click(TEAM_LEAVE_TYPE);
		QAFWebElement element =
				select("list.options.leavetype.teamleavelistpage", leaveType);
		if (element != null)
			if (!checkIsSelectedAndClick((QAFExtendedWebElement) element))
				element.click();
			else Reporter.log(ELEMENT_NOT_FOUND);
	}

	private void selectLeaveStatus(String leaveStatus) {
		click(LEAVE_STATUS);
		QAFWebElement element =
				select("list.options.leavestatus.teamleavelistpage", leaveStatus);
		if (element != null)
			if (!checkIsSelectedAndClick((QAFExtendedWebElement) element))
				element.click();
			else Reporter.log(ELEMENT_NOT_FOUND);
	}

	private static boolean checkIsSelectedAndClick(QAFExtendedWebElement element) {
		return new QAFExtendedWebElement(element,
				ConfigurationManager.getBundle()
						.getString("option.selected.applyleavepage"))
								.getAttribute("class").contains("glyphicon-ok");
	}

	@QAFTestStep(description = "user verifies search results with {0}")
	public void verifySearchresults(String searchKey) {
		NestUtils.waitForLoader();
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		for (QAFWebElement element : NestUtils.getQAFWebElements(LEAVE_TABLE_ROW)) {

			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");

			Validator.verifyThat("Employee Name",
					tableData.getEmployeeNameAndId().getText(),
					Matchers.containsString(leavesdatabean.getEmpName()));

			if (leavesdatabean.getProject() != null)
				Validator.verifyThat("Project Name", tableData.getProject().getText(),
						Matchers.containsString(leavesdatabean.getProject()));

			Validator.verifyThat("Leave Type", tableData.getLeaveType().getText(),
					Matchers.containsString(leavesdatabean.getLeaveType()));

			Validator.verifyThat("Leave Status", tableData.getStatus().getText(),
					Matchers.containsString(leavesdatabean.getLeaveStatus()));
		}
	}

	@QAFTestStep(description = "user verifies reset functionality with {0}")
	public void resetFunctionality(String searchKey) {
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);

		String empName = CommonStep.getText(TEAM_EMPLOYEE_NAME);
		String projectName = CommonStep.getText(ACCOUNT_BUTTON);
		String leaveType = CommonStep.getText(TEAM_LEAVE_TYPE);
		String leaveStatus = CommonStep.getText(LEAVE_STATUS);

		Reporter.logWithScreenShot("Before editing the fields.");
		select(DATE_TYPES, leavesdatabean.getDateType()).click();
		selectEmployee(leavesdatabean.getEmpName());
		NestUtils.waitForLoader();

		selectProject(leavesdatabean.getProject());
		NestUtils.waitForLoader();
		selectLeaveType(leavesdatabean.getLeaveType());
		selectLeaveStatus(leavesdatabean.getLeaveStatus());
		Reporter.logWithScreenShot("After editing the fields.");
		click(RESET_BUTTON);
		NestUtils.waitForLoader();
		Reporter.logWithScreenShot("Fields after clicking on reset button");
		Validator.verifyThat(CommonStep.getText(TEAM_EMPLOYEE_NAME),
				Matchers.containsString(empName));
		Validator.verifyThat(CommonStep.getText(ACCOUNT_BUTTON),
				Matchers.containsString(projectName));
		Validator.verifyThat(CommonStep.getText(TEAM_LEAVE_TYPE),
				Matchers.containsString(leaveType));
		Validator.verifyThat(CommonStep.getText(LEAVE_STATUS),
				Matchers.containsString(leaveStatus));
	}

	@QAFTestStep(description = "user select Floating holidays")
	public void iSelectFloatingHolidays() {
		click("dropdownArrow.userhomepage");
	}

	public void applyLeaveAgain(String days) {
		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		int date = 0;
		while (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			fillToDate(NestUtils.getLeaveDate(((2 + date) + Integer.parseInt(days))));
			fillFromDate(NestUtils.getLeaveDate((2 + date)));
			click(ASSIGN_LEAVE_BUTTON);
			NestUtils.waitForLoader();
			status1 =
					NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
			status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
			date = date + 2;
		}
	}

	@QAFTestStep(description = "user perform action on leave request with given {0} data")
	public void searchAndPerformActionOnLeaveRequest(String searchKey) {
		NestUtils.waitForLoader();
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		QAFWebElement element = NestUtils.getQAFWebElements(LEAVE_TABLE_ROW).get(0);
		TeamLeaveListTableRowComponent tableData =
				new TeamLeaveListTableRowComponent((QAFExtendedWebElement) element, "");
		tableData.getActions().click();
		tableData.performAction(leavesdatabean.getAction());
		if (verifyVisible("label.title.popup.common")) {
			clear(COMMENT);
			sendKeys("Test : Cancel Leave Request", COMMENT);
			click(SUBMIT);
		}
		NestUtils.waitForLoader();
		click("button.submit.leaverequest.teamleavelistpage");
	}

	@QAFTestStep(description = "user approves all leave request with given {0} data")
	public void searchAndApproveAllLeaveRequest(String searchKey) {
		NestUtils.waitForLoader();
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);

		for (QAFWebElement element : NestUtils.getQAFWebElements(LEAVE_TABLE_ROW)) {

			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");
			tableData.getActions().click();
			tableData.performAction(leavesdatabean.getAction());
			if (verifyVisible("label.title.popup.common")) {
				clear(COMMENT);
				sendKeys("Test : Cancel Leave Request", COMMENT);
				click(SUBMIT);
			}
			click("button.approveall.leaverequests.teamleavelistpage");
		}
	}

	@QAFTestStep(description = "user selects {0} report and export it")
	public void exportLeaves(String exportType) {
		click("button.exportdropdown.teamleavelistpage");
		NestUtils.clickUsingJavaScript(
				select("list.options.exportdropdown.teamleavelistpage", exportType));
		click("button.export.teamleavelistpage");
	}

	@QAFTestStep(description = "user verifies exported report")
	public void verifyLeavesExported() {
		NestUtils.waitForLoader();
		boolean fileFound = false;
		File downloads = new File(System.getProperty("user.home") + "/downloads/");
		for (String file : downloads.list()) {
			if (verifyFilePresent(file)) {
				fileFound = true;
				Reporter.log("File name : " + file);
				if (new File(System.getProperty("user.home") + "/downloads/" + file)
						.delete())
					Reporter.log("File deleted successfully");
				break;
			}
		}
		if (fileFound)
			Reporter.log("File exported successfully.", MessageTypes.Pass);
		else Reporter.logWithScreenShot("File not exported.", MessageTypes.Fail);

	}

	private boolean verifyFilePresent(String fileName) {
		boolean isFileFound = false;
		String[] reportTypes = ConfigurationManager.getBundle()
				.getString("export.report.types").split(":");
		for (String reportType : reportTypes) {
			if (fileName.contains(reportType.trim())) {
				isFileFound = true;
				break;
			}
		}
		return isFileFound;
	}

	@QAFTestStep(description = "user should fill the form and apply for leave with data {0}")
	public void userApplyForOneFullDayLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		NestUtils.waitForLoader();
		fillToDate(
				NestUtils.getLeaveDate(2 + Integer.parseInt(applyLeave.getLeaveDays())));
		NestUtils.waitForLoader();
		fillFromDate(NestUtils.getLeaveDate(2));
		NestUtils.waitForLoader();

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));
		NestUtils.scrollToAxis(0, 250);
		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			setDayType(applyLeave.getDayType());
		}
		click(APPLY_LEAVE_BUTTON);

		applyAgainIfFails(applyLeave.getLeaveReason(), applyLeave.getLeaveDays(),
				applyLeave.getLeaveType(), applyLeave.getDayType(),
				applyLeave.getUserType());
	}

	public void applyAgainIfFails(String reason, String days, String leaveType,
			String dayType, String userType) {
		waitForVisible(STATUS);
		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		int date = 0;
		while (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			NestUtils.waitForLoader();
			fillToDate(NestUtils.getLeaveDate(((2 + date) + Integer.parseInt(days))));
			NestUtils.waitForLoader();
			fillFromDate(NestUtils.getLeaveDate((2 + date)));
			NestUtils.waitForLoader();

			click(LEAVE_REASON);
			NestUtils.clickUsingJavaScript(select(LIST_LEAVE_REASONS, reason));
			NestUtils.scrollToAxis(0, 250);
			if (!leaveType.isEmpty()) {
				setLeaveType(leaveType, userType);
			}
			if (!dayType.isEmpty()) {
				setDayType(dayType);
			}
			click(APPLY_LEAVE_BUTTON);
			NestUtils.waitForLoader();
			status1 =
					NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
			status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
			date = date + 2;
		}
	}

	@QAFTestStep(description = "user verifies leave request is not present in search results with {0}")
	public void verifySearchResult(String searchKey) {
		NestUtils.waitForLoader();
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		for (QAFWebElement element : NestUtils.getQAFWebElements(LEAVE_TABLE_ROW)) {
			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");
			tableData.getEmployeeNameAndId().verifyNotPresent();
		}
	}

	public void setLeaveType(String leaveType, String userType) {
		String from = new QAFExtendedWebElement(FROM_DATE).getAttribute(VALUE);
		String to = new QAFExtendedWebElement(TO_DATE).getAttribute(VALUE);

		if (from.equalsIgnoreCase(to)) {
			NestUtils.getChildElement("component.datebox.applyleavepage",
					"button.leavetype.applyleavepage").click();
			if (!userType.isEmpty())
				if (userType.equalsIgnoreCase("USA"))
					verifyLeaveTypes("us.user.leave.types");
				else if (userType.equalsIgnoreCase("UK"))
					verifyLeaveTypes("uk.user.leave.types");
			selectLeaveTypeInBox(leaveType);
		} else {
			// Code need to be develop for leaves more than 1 day
		}
	}

	public void verifyLeaveTypes(String dataKey) {
		String[] leaveTypes =
				ConfigurationManager.getBundle().getString(dataKey).split(":");
		for (String leaveType : leaveTypes) {
			if (verifyTextPresent("list.options.leavetype.applyleavepage",
					leaveType.trim()))
				Reporter.log(leaveType + PRESENT, MessageTypes.Pass);
			else Reporter.log(leaveType + NOT_PRESENT, MessageTypes.Fail);
		}
	}

	public void selectLeaveTypeInBox(String leaveType) {
		for (QAFWebElement option : NestUtils
				.getQAFWebElements("list.options.leavetype.applyleavepage")) {
			if (option.getText().trim().toLowerCase()
					.contentEquals(leaveType.toLowerCase())) {
				option.click();
				break;
			}
		}
	}

	public void setDayType(String dayType) {
		String from = new QAFExtendedWebElement(FROM_DATE).getAttribute(VALUE);
		String to = new QAFExtendedWebElement(TO_DATE).getAttribute(VALUE);
		if (from.equalsIgnoreCase(to)) {
			new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
					.getString("button.day.type.applyleavepage"), dayType)).click();
		} else {
			// Code need to be develop for leaves more than 1 day
		}
	}

	@QAFTestStep(description = "user should fill the form and apply for back dated leave with data {0}")
	public void userApplyForBackDatedLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		fillFromDate(NestUtils
				.getLeaveDate((-2 + Integer.parseInt(applyLeave.getLeaveDays()))));
		NestUtils.waitForLoader();
		fillToDate(NestUtils.getLeaveDate(-2));
		NestUtils.waitForLoader();
		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));
		NestUtils.scrollToAxis(0, 250);

		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			setDayType(applyLeave.getDayType());
		}
		click(APPLY_LEAVE_BUTTON);
		NestUtils.waitForLoader();
		applyAgainIfFails(applyLeave.getLeaveReason(), applyLeave.getLeaveDays(),
				applyLeave.getLeaveType(), applyLeave.getDayType(),
				applyLeave.getUserType());
	}

	@QAFTestStep(description = "user applies a special leave with data {0}")
	public void applySpecialLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		click("button.specialleave.applyleavepage");
		verifyVisible("button.close.popup.common");
		NestUtils.clickUsingJavaScript(selectSpecialLeave(
				"list.leave.type.popup.applyleavepage", applyLeave.getLeaveType()));
		fillToDate(
				NestUtils.getLeaveDate(2 + Integer.parseInt(applyLeave.getLeaveDays())));
		NestUtils.waitForLoader();
		fillFromDate(NestUtils.getLeaveDate(2));
		NestUtils.waitForLoader();
		click(SUBMIT);
		againApplySpecialLeave(applyLeave.getLeaveDays());
	}

	@QAFTestStep(description = "user verifies special leave message {0} is present")
	public void verifySpecialLeaveMessage(String message) {
		NestUtils.waitForLoaderWithoutCondition();
		if (String.valueOf(
				ConfigurationManager.getBundle().getProperty(SPECIAL_LEAVE_MESSAGE))
				.contains(message))
			Reporter.logWithScreenShot(message + PRESENT, MessageTypes.Pass);
		else Reporter.logWithScreenShot(message + NOT_PRESENT, MessageTypes.Fail);
	}

	private QAFWebElement selectSpecialLeave(String loc, String selOption) {
		QAFWebElement element = null;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().toLowerCase().contains(selOption.toLowerCase())) {
				element = option;
				break;
			}
		}
		return element;
	}
	@QAFTestStep(description = "user verifies leaves list table of My Leave List page")
	public void verifyTableHeadersMyLeaveListPage() {
		String[] headers = ConfigurationManager.getBundle()
				.getString("myLeaveList.table.headers").split(":");
		for (String header : headers) {
			if (verifyTextPresent("list.headers.leavestable.teamleavelistpage",
					header.trim()))
				Reporter.log(header + PRESENT, MessageTypes.Pass);
			else Reporter.log(header + NOT_PRESENT, MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies the fields present on My Leave List page")
	public void verifyFieldsOnTeamLeaveListPage() {
		verifyPresent(FROM_DATE);
		verifyPresent(TO_DATE);
		verifyPresent(LEAVE_TYPE);
		verifyPresent("button.myLeaveList.status");
	}

	@QAFTestStep(description = "user searches with criteria {0}")
	public void searchLeaveWithCriteria(String searchKey) {
		myLeaveDataBean.fillFromConfig(searchKey);
		setField(FROM_DATE, myLeaveDataBean.getFromDate());
		NestUtils.waitForLoader();
		setField(TO_DATE, myLeaveDataBean.getToDate());
		NestUtils.waitForLoader();
		selectValueFromdrop(LEAVE_TYPE, myLeaveDataBean.getLeaveType());
		NestUtils.waitForLoader();
		selectValueFromdrop("button.myLeaveList.status",
				myLeaveDataBean.getLeaveStatus());
		NestUtils.waitForLoader();
		click(SEARCH_BUTTON_COMMON);
		NestUtils.waitForLoader();
	}

	public void setField(String key, String value) {
		click(key);
		CommonStep.clear(key);
		sendKeys(value, key);
	}

	private void selectValueFromdrop(String key, String value) {
		NestUtils.waitForLoader();
		click(key);
		QAFWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(DROPDOWN_OPTIONS), value));
		// waitForVisible(String.format(
		// ConfigurationManager.getBundle().getString(DROPDOWN_OPTIONS), value));
		if (element.isDisplayed())
			element.click();
		else
			Reporter.log(
					"Button is not enabled, user might have applied for back dated leave.");
	}

	@QAFTestStep(description = "verify if user is able to search leave for the given criteria")
	public void verifyMyLeaveListTable() {
		List<QAFWebElement> listOfElement =
				NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle()
						.getString("list.myLeaveList.table.column"), "Status"));
		for (QAFWebElement element : listOfElement) {
			Validator.verifyThat(element.getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(myLeaveDataBean.getLeaveStatus()));
		}

	}

	@QAFTestStep(description = "user should see {0} page")
	public void verifyNavigation(String expectedValue) {
		String value = expectedValue;
		NestUtils.waitForLoader();
		verifyPresent("text.applyLeave.title");
		Validator.verifyThat(CommonStep.getText("text.applyLeave.title").trim(),
				Matchers.equalTo(value));
	}

	public void fillDate(String loc, String date) {
		click(loc);
		calendar.selectDate(date);
	}

	@QAFTestStep(description = "user apply leave for multiple days with data as {0}")
	public void applyLeaveForMultipleDays(String dataKey) {
		ArrayList<Integer> index = new ArrayList<>();
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);

		NestUtils.waitForLoader();

		fillToDate(NestUtils.getLeaveDate(
				(2 + incrementDate) + Integer.parseInt(applyLeave.getLeaveDays())));
		NestUtils.waitForLoader();

		fillFromDate(NestUtils.getLeaveDate((2 + incrementDate)));
		NestUtils.waitForLoader();

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));
		NestUtils.scrollToAxis(0, 250);

		if ((!applyLeave.getLeaveType().isEmpty())
				&& applyLeave.getLeaveType().equalsIgnoreCase("different")) {
			String[] leaveTypes = {"PTO", "Comp Off"};
			int count = 0;
			List<QAFWebElement> days = NestUtils.getQAFWebElements("text.applyLeave.Day");
			for (int i = 0; i < days.size(); i++) {
				if (days.get(i).getText().trim().matches("Sat|Sun")) {
					index.add(i);
				}
			}
			for (int i = 0; i < days.size(); i++) {
				if (!index.contains(i) && count < 2) {
					setDayType(leaveTypes[count++], i);
				}

			}
		} else if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
			List<QAFWebElement> days = NestUtils.getQAFWebElements("text.applyLeave.Day");
			for (int i = 0; i < days.size(); i++) {
				if (days.get(i).getText().trim().matches("Sat|Sun")) {
					index.add(i);
				}
			}
		}
		if (!applyLeave.getDayType().isEmpty()) {
			List<QAFWebElement> listOfElement = NestUtils.getQAFWebElements(String.format(
					ConfigurationManager.getBundle().getString(HALFDAY_RADIO_BUTTON),
					" Half  "));
			if (applyLeave.getDayType().equalsIgnoreCase("First-Half"))
				listOfElement.get(0).click();
			else listOfElement.get(listOfElement.size() - 1).click();
		}
		NestUtils.waitForLoader();
		click(APPLY_LEAVE_BUTTON);
		waitForVisible(STATUS);
		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		if (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			incrementDate = incrementDate + 2;
			applyLeaveForMultipleDays(dataKey);

		} else {
			incrementDate = 0;
		}

	}

	@QAFTestStep(description = "user cancel leave from My leave list page")
	public void cancelLeaveFromMyLeave() {
		String element = "button.myLeaveList.select";
		NestUtils.waitForLoader();
		CommonPage.iRefreshPage();
		NestUtils.waitForLoader();
		try {
			if (!CommonStep.verifyEnabled(element)) {
				Reporter.log("User applied for back dated leave, so can not cancel leave");
				return;
			}
			selectValueFromdrop("button.myLeaveList.select", "Cancel");

			if (CommonStep.verifyEnabled(CommonPage.buttonTextContains("Submit"))) {
				NestUtils.clickUsingJavaScript(CommonPage.buttonTextContains("Submit"));
				NestUtils.waitForLoader();
			}
		} catch (Exception e) {
			Reporter.log("Element not enabled",MessageTypes.Fail);
		}

	}

	@QAFTestStep(description = "user apply half day leave with data as {0}")
	public void applyHalfDayLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		NestUtils.waitForLoader();
		fillFromDate(NestUtils.getLeaveDate((incrementDate)));
		NestUtils.waitForLoader();

		fillToDate(NestUtils.getLeaveDate((incrementDate)));
		NestUtils.waitForLoader();

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));
		NestUtils.scrollToAxis(0, 250);

		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString(HALFDAY_RADIO_BUTTON),
					applyLeave.getDayType())).click();
		}
		click(APPLY_LEAVE_BUTTON);
		waitForVisible(STATUS);
		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		if (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			incrementDate = incrementDate + 1;
			applyHalfDayLeave(dataKey);
		}
	}

	public void verifyAppliedLeave(String columnName, String exptectedValues) {
		String strExpectedValue = exptectedValues;
		if (columnName.equalsIgnoreCase(LEAVE_DURATION)
				&& strExpectedValue.matches("Second-Half|First-Half")) {
			strExpectedValue = "0.50";
		} else if (columnName.equalsIgnoreCase(LEAVE_DURATION)
				&& !strExpectedValue.contains(".")) {
			strExpectedValue = strExpectedValue + ".00";
		}
		List<QAFWebElement> listOfElement =
				NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle()
						.getString("list.myLeaveList.table.column"), columnName));
		Validator.verifyThat(listOfElement.get(0).getText().toString().trim(),
				Matchers.equalToIgnoringWhiteSpace(strExpectedValue));
	}

	@QAFTestStep(description = "user select leave period having Holiday in between")
	public String[] selectLeavePeriodWithHoliday() {
		int increment = Integer.parseInt(ConfigurationManager.getBundle()
				.getString("applyLeave.includingHoliday"));
		String[] dateRange = new String[2];
		Calendar calendarHoliday = Calendar.getInstance();
		try {
			List<QAFWebElement> holidays =
					NestUtils.getQAFWebElements("section.holiday.listOfHolidays");
			String holiday = holidays.get(holidays.size() - 1).getText().toString();
			dateRange[0] = NestUtils.converDate(holiday);
			SimpleDateFormat format = new SimpleDateFormat("dd-MMMM-yyyy");
			Date date = format.parse(dateRange[0]);

			boolean flag = true;
			while (flag) {
				String today = format.format(date);
				calendarHoliday.setTime(format.parse(today));
				calendarHoliday.add(Calendar.DATE, increment);
				String newDate = format.format(calendarHoliday.getTime());
				Date dt1 = null;
				dt1 = format.parse(newDate);
				format.applyPattern("EEEE dd-MMMM-yyyy");
				String[] array = format.format(dt1).split(" ");
				if (array[0].equals("Saturday") || array[0].equals("Sunday")) {
					if (increment > 0)
						increment++;
					else increment--;
				} else {
					flag = false;
					dateRange[1] = array[1];
				}
			}
		} catch (Exception e) {
			Reporter.log("Error : " + e.getMessage());
		}
		return dateRange;
	}

	@QAFTestStep(description = "user applies for Leave including holiday {0}")
	public void appyLeaveIncludingHoliday(String[] holiday) {
		fillFromDate(holiday[0]);
		NestUtils.waitForLoader();

		fillToDate(holiday[1]);
		NestUtils.waitForLoader();

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(select(LIST_LEAVE_REASONS, "Family Function"));
		NestUtils.scrollToAxis(0, 250);
		verifyLeaveDay("Holiday", 0);
		click(APPLY_LEAVE_BUTTON);
	}

	public void verifyLeaveDay(String leaveDayTyep, int iteration) {
		if (leaveDayTyep.equalsIgnoreCase("Holiday")) {
			Validator.verifyThat(CommonStep.getText(LEAVE_DAYS), Matchers.equalTo("HD"));
		} else if (leaveDayTyep.equalsIgnoreCase("WeekOff")) {
			int count = 0;
			List<QAFWebElement> leaveDays = NestUtils.getQAFWebElements(LEAVE_DAYS);
			for (int i = 0; i < iteration; i++) {
				Validator.verifyThat(
						leaveDays.get(i).getText().toString().matches("WO|PTO"),
						Matchers.equalTo(true));
				if (leaveDays.get(i).getText().toString().equals("WO")) {
					count++;
				}
			}
			if (count <= 0) {
				Reporter.log("'WO' not present while applying leave including weekoff",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user applies for Leave including week-off with data as {0}")
	public void applyLeaveIncludingWeekOff(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);

		fillFromDate(NestUtils.getLeaveDate((2 + incrementDate)));
		NestUtils.waitForLoader();

		fillToDate(NestUtils.getLeaveDate(
				(2 + incrementDate) + Integer.parseInt(applyLeave.getLeaveDays())));
		NestUtils.waitForLoader();
		verifyLeaveDay("WeekOff", Integer.parseInt(applyLeave.getLeaveDays()));

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));
		NestUtils.scrollToAxis(0, 250);

		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			List<QAFWebElement> listOfElement = NestUtils.getQAFWebElements(String.format(
					ConfigurationManager.getBundle().getString(HALFDAY_RADIO_BUTTON),
					" Half  "));
			if (applyLeave.getDayType().equalsIgnoreCase("First-Half"))
				listOfElement.get(0).click();
			else listOfElement.get(listOfElement.size() - 1).click();
		}
		click(APPLY_LEAVE_BUTTON);
		NestUtils.waitForLoader();
		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		if (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			incrementDate = incrementDate + 2;
			applyLeaveIncludingWeekOff(dataKey);
		}
	}

	public void setDayType(String dayType, int index) {
		String strDayType = dayType;
		String from = new QAFExtendedWebElement(FROM_DATE).getAttribute(VALUE);
		String to = new QAFExtendedWebElement(TO_DATE).getAttribute(VALUE);
		if (from.equalsIgnoreCase(to)) {
			setDayType(strDayType);
		} else {
			List<QAFWebElement> days = NestUtils.getQAFWebElements(LEAVE_DAYS);
			days.get(index).click();
			NestUtils.waitForLoader();
			List<QAFWebElement> listOfLeaveType = NestUtils.getQAFWebElements(String
					.format(ConfigurationManager.getBundle().getString(DROPDOWN_OPTIONS),
							strDayType));
			listOfLeaveType.get(index).click();
			NestUtils.waitForLoader();
			days = NestUtils.getQAFWebElements(LEAVE_DAYS);
			if (strDayType.equalsIgnoreCase("Comp Off")) {
				strDayType = "CO";
			}
			Validator.verifyThat(days.get(index).getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(strDayType));
		}
	}

	@QAFTestStep(description = "user applies for Leave with different Leave Type with data as {0}")
	public void applyLeaveWithDifferentLeaveType(String dataKey) {
		applyLeaveForMultipleDays(dataKey);
	}
	
	//with customized exeption handling
	/*public void applyLeaveWithDifferentLeaveType(String dataKey)throws dataNotFoundException {
		if (isLeaveSufficientAvailable()) {
			applyLeaveForMultipleDays(dataKey);
		} else {
		
				throw new dataNotFoundException(CommonPage
						.getWebElementFromList("div.contains.class", "user_profile_name")
						.getText() + " doesnt have suffient leave balance"+"hello data not found");
		}
	}
	
	public boolean isLeaveSufficientAvailable() {
		return (Double.parseDouble(
				CommonPage.getWebElementFromList("div.contains.class", "comp_off")
				.getText()) > 0.00) && (Double.parseDouble(
						CommonPage.getWebElementFromList("div.contains.class", "pto")
						.getText()) > 0.00);
	}*/
	
	
	public void againApplySpecialLeave(String days) {
		boolean status = verifyTextPresent(POPUP_MESSAGE, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean messagePresent = status;
		String actualMessage = null;
		String message = null;
		while (!messagePresent) {
			message = CommonStep.getText(POPUP_MESSAGE).trim();
			messagePresent = (message.isEmpty() || message == null) ? false : true;
		}
		if (message.contains(MESSAGE_LEAVE_APPLIED_SUCCESSFUL)) {
			ConfigurationManager.getBundle().addProperty(SPECIAL_LEAVE_MESSAGE,
					message);
			return;
		}
		status = verifyTextPresent(POPUP_MESSAGE, MESSAGE_ALREADY_LEAVE_APPLIED);
		int date = 0;
		while (status) {
			NestUtils.waitForLoader();
			fillToDate(NestUtils.getLeaveDate(((2 + date) + Integer.parseInt(days))));
			NestUtils.waitForLoader();
			fillFromDate(NestUtils.getLeaveDate((2 + date)));
			NestUtils.waitForLoader();
			click(SUBMIT);
			try {
				int count = 0;
				while (count < 5) {
					try {
						NestUtils.textToBePresentInElement(
								new QAFExtendedWebElement(POPUP_MESSAGE),
								MESSAGE_LEAVE_APPLIED_SUCCESSFUL);
						actualMessage = CommonStep.getText(POPUP_MESSAGE).trim();
						ConfigurationManager.getBundle()
								.addProperty(SPECIAL_LEAVE_MESSAGE, actualMessage);
						count = count + 1;
						break;
					} catch (Exception e) {
						count = count + 1;
					}
				}
				if (!new QAFExtendedWebElement(POPUP_MESSAGE).isDisplayed())
					break;
			} catch (Exception e) {
				Reporter.log("Error in applying special leave : " + e.getMessage());
			}
			status = verifyTextPresent(POPUP_MESSAGE, MESSAGE_ALREADY_LEAVE_APPLIED);
			date = date + 2;
		}
	}

	public void scrollPage(int location) {
		((JavascriptExecutor) driver)
				.executeScript("window.scrollTo(0, " + location + ");");
	}

	@QAFTestStep(description = "user should see {0} page opened")
	public void verifyPageOpen(String pageName) {
		NestUtils.waitForLoader();
		if (pageName.trim()
				.equalsIgnoreCase(NestUtils.PageName.AVAILABLE_LEAVES.getName())) {
			Validator.verifyThat(pageName + " page must be opened",
					CommonStep.getText("label.available.leaves.header.availableleavepage")
							.trim().toLowerCase(),
					Matchers.containsString(pageName.trim().toLowerCase()));
		} else {
			Validator.verifyThat(pageName + " page must be opened",
					CommonStep.getText("common.breadcrumb").trim().toLowerCase(),
					Matchers.containsString(pageName.trim().toLowerCase()));
		}
	}

	public void verifyLeaveRequests() {
		CommonStep.assertVisible("lable.requestType.requests");
	}

	public void verifyExpenseRequests() {
		CommonStep.verifyText("lable.requestType.requests", "My Expense List");
	}

	public void verifyTravelRequests() {
		CommonStep.verifyText("lable.requestType.requests", "My Travel Requests");
	}

	public void verifyotherRequests() {
		CommonStep.verifyText("lable.requestType.requests", "Others");
	}

	public QAFExtendedWebElement LeavesRequest() {
		QAFExtendedWebElement leaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(LABLE_REQUESTPAGE ), "Leave "));
		return leaveRequest;
	}

	public QAFExtendedWebElement expenseRequests() {
		QAFExtendedWebElement leaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(LABLE_REQUESTPAGE), "Expense"));
		return leaveRequest;
	}

	public QAFExtendedWebElement travelRequests() {
		QAFExtendedWebElement leaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(LABLE_REQUESTPAGE), "Travel"));
		return leaveRequest;
	}

	@QAFTestStep(description = "user apply leave for more days than leave balance with data as {0}")
	public void applyLeaveForWithInsufficientData(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);

		NestUtils.waitForLoader();
		fillToDate(NestUtils.getLeaveDate(
				(2 + incrementDate) + Integer.parseInt(applyLeave.getLeaveDays())));
		NestUtils.waitForLoader();

		fillFromDate(NestUtils.getLeaveDate((2 + incrementDate)));
		NestUtils.waitForLoader();

		click(LEAVE_REASON);
		NestUtils.clickUsingJavaScript(
				select(LIST_LEAVE_REASONS, applyLeave.getLeaveReason()));

		NestUtils.scrollToAxis(0, 250);

		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			setDayType(applyLeave.getDayType());
		}
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(APPLY_LEAVE_BUTTON);
		waitForVisible(STATUS);

		boolean status1 =
				NestUtils.verifyMessageVisible(STATUS, MESSAGE_ALREADY_LEAVE_APPLIED);
		boolean status2 = NestUtils.verifyMessageVisible(STATUS, MESSAGE_CANNOT_BE_LESS);
		if (status1 || status2) {
			NestUtils.closeAllStatusBars(STATUS);
			incrementDate = incrementDate + 2;
			applyLeaveForWithInsufficientData(dataKey);

		} else {
			incrementDate = 0;
		}
	}

	@QAFTestStep(description = "user navigate to Team Leave Balance page")
	public void navigateToTeamLeaveBalance() {
		QAFExtendedWebElement teamLeaveBalanceLink =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString(DROPDOWN_OPTIONS), "Team Leave Balance"));

		teamLeaveBalanceLink.click();
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies the fields present on Team Leave Balance page")
	public void verifyFieldsOnTeamLeaveBalancePage() {
		String[] fields = ConfigurationManager.getBundle()
				.getString("teamLeave.balance.fields").split(":");
		for (String field : fields) {
			if (verifyTextPresent("list.leaveBalance.fields", field.trim()))
				Reporter.log(field + PRESENT, MessageTypes.Pass);
			else Reporter.log(field + NOT_PRESENT, MessageTypes.Fail);
		}
		verifyPresent(SEARCH_BUTTON_LEAVE_BALANCE);
		verifyPresent(RESET_TEAM_LEAVE_BALANCE);
	}

	@QAFTestStep(description = "user verify the page name as {0}")
	public void userVerifyThePageName() {
		Validator.verifyThat(CommonStep.getText("label.pageName.configureLeavePage"),
				Matchers.containsString("Configure Leave"));
	}
	@QAFTestStep(description = "user verify the breadcrumb on configure leave page {0}")
	public void userVerifyTheBreadcrumbOnConfigureLeavePage(String str0) {
		if (CommonStep.getText("label.breadcrumb.configureLeavePage")
				.contains(str0.substring(7))) {
			Reporter.logWithScreenShot("breadcrum is present  on configure leave page",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrum is NOT present on configure leave page",
					MessageTypes.Fail);
		}
	}
	@QAFTestStep(description = "user switches tabs as {0} , {1} , {2} and {3}")
	public void userSwitchesTabsAsAnd(String holidays, String floatingHolidays,
			String quarterEnd, String yearEnd) {
		String[] tabs = {floatingHolidays, quarterEnd, yearEnd,holidays};
		for (int i = 0; i < 4; i++) {
			QAFWebElement tab = CommonPage
					.getWebElementFromList("link.tabs.configureLeavePage", tabs[i]);
			tab.click();

		}
	}
	@QAFTestStep(description = "user verifies filter icon under holidays tab")
	public void userVerifiesFilterIconUnderHolidaysTab() {
		if (verifyPresent(FILTER_BUTTON)) {
			QAFWebElement searchButton = new QAFExtendedWebElement(SEARCH_BUTTON_COMMON);
			NestUtils.waitForLoader();
			CommonStep.click(FILTER_BUTTON);
			NestUtils.waitForLoader();
			if (searchButton.isDisplayed()) {
				Reporter.logWithScreenShot("Filter is NOT hidden on configure leave page",
						MessageTypes.Fail);
			} else {
				Reporter.logWithScreenShot("Filter is hidden on configure leave page",
						MessageTypes.Pass);
			}
		} else {
			Reporter.logWithScreenShot("Data not found", MessageTypes.Warn);
		}

	}
	@QAFTestStep(description = "user verify leave period dropdown under holidays tab")
	public void userVerifyLeavePeriodDropdownUnderHolidaysTab() {
		CommonStep.click("button.hiddenFilter.configureLeavePage");
		NestUtils.waitForLoader();
		CommonStep.click("dropdown.search.configureLeavePage");
		if (verifyVisible("link.periodDropdown.configureLeavePage")) {
			Reporter.logWithScreenShot(
					"Leave period is available on configure leave page",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Leave period is NOT available on configure leave page",
					MessageTypes.Fail);
		}

		CommonStep.click("dropdown.search.configureLeavePage");
	}
	@QAFTestStep(description = "user verify {0},{1},{2} buttons under holidays tab")
	public void userVerifyButtonsUnderHolidaysTab(String search, String add,
			String delete) {
		NestUtils.waitForLoader();
		String[] buttons = {search, add, delete};
		for (int i = 0; i < 3; i++) {
			QAFExtendedWebElement clickButton =
					new QAFExtendedWebElement(
							CommonPage.buttonTextContains(buttons[i]));
			if (clickButton.isDisplayed()) {
				Reporter.logWithScreenShot(
						buttons[i] + VERIFY_MESG_CONFIG,
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						buttons[i] + VERIFY_MESG_CONFIG,
						MessageTypes.Fail);
			}
		}
	}
	@QAFTestStep(description = "user verify table with checkbox,{0},{1},{2} under holidays tab")
	public void userVerifyTableWithCheckboxNameOfHolidayDateLocationUnderHolidaysTab(
			String nameOfHoliday, String date, String location) {

		String[] labels = {nameOfHoliday, date, location};
		for (int i = 0; i < 3; i++) {
			QAFWebElement clickButton = CommonPage.getWebElementFromList(
					"label.coulmnName.configureLeavePage", labels[i]);
			if (clickButton.isDisplayed()) {
				Reporter.logWithScreenShot(
						labels[i] + VERIFY_MESG_CONFIG,
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						labels[i] + VERIFY_NOT_MESG_CONFIG,
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user selects leave period as {0}")
	public void userSelectsLeavePeriodAs(String str0) {
		CommonStep.click("dropdown.search.configureLeavePage");
		QAFExtendedWebElement leavePeriod =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("dropdown.leaveperiod.configureLeavePage"), str0));
		leavePeriod.click();
		
		CommonStep.click(SEARCH_BUTTON_COMMON);
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verify search result for Holidays within leave period {0}")
	public void userVerifySearchResultForHolidays(String leavePeriod) {
		List<QAFWebElement> searchDates =
				NestUtils.getQAFWebElements(LIST_CONFIGLEAVE);
		Reporter.log("" + searchDates.size());
		for (int i = 0; i < 5; i++) {
			try {
				String value = searchDates.get(i).getText().substring(7);
				Reporter.log(searchDates.get(i).getText());
				if (value.contains(leavePeriod.substring(2, 5))
						|| value.contains(leavePeriod.substring(7))) {
					Reporter.logWithScreenShot(
							"Search results are within leave period" + leavePeriod,
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(
							"Search results are  NOT within leave period" + leavePeriod,
							MessageTypes.Fail);
				}
			} catch (Exception e) {
				Reporter.log("Number of Search results are less than 5 " + leavePeriod,
						MessageTypes.Pass);
			}

		}
	}

	@QAFTestStep(description = "user verifies Edit button functionality on Team Leave Balance page")
	public void verifyEditBtnFunctionality() {

		NestUtils.waitForLoader();
		String leaveBalanceDays = CommonStep.getText("text.leaveBalance.days");
		updateLeaveBalance("15");
		String leaveBalanceDaysUpdated = CommonStep.getText("text.leaveBalance.days");
		if (leaveBalanceDays.equals(leaveBalanceDaysUpdated)) {
			Reporter.log(
					"Leave Balance Days value is not getting updated after updating Leave count.",
					MessageTypes.Fail);
		}
		updateLeaveBalance("16");
	}

	public void updateLeaveBalance(String leaveDays) {
		click("button.expand.collapse");
		NestUtils.waitForLoader();
		NestUtils.scrollToAxis(160, 1300);
		waitForVisible("button.edit.leaveBalance");
		NestUtils.waitForLoader();
		click("button.edit.leaveBalance");
		NestUtils.scrollToAxis(0, 100);
		clear("text.employee.leaveBalance");
		sendKeys(leaveDays, "text.employee.leaveBalance");
		NestUtils.scrollToAxis(0, 1000);
		waitForVisible("button.leaveBalance.save");
		NestUtils.waitForLoader();
		click("button.leaveBalance.save");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies Reset button functionality on Team Leave Balance page")
	public void verifyResetButtonFunctionality() {
		String leaveType = "Comp Off";
		NestUtils.waitForLoader();
		String originalFinancialYear =
				NestUtils.getQAFWebElements(SELCTED_LEAVE).get(0)
						.getText().toString();
		selectValueFromdrop("dropdown.leavePeriod",
				ConfigurationManager.getBundle().getString("financialYear.leaveBalance"));
		NestUtils.waitForLoader();
		List<QAFWebElement> leaveBalanceDropdowns =
				NestUtils.getQAFWebElements(DROPDOWN_LEAVETYPE);
		leaveBalanceDropdowns.get(0).click();
		QAFWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				leaveType));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				leaveType));
		element.click();
		NestUtils.waitForLoader();

		leaveBalanceDropdowns.get(1).click();
		element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				ConfigurationManager.getBundle().getString(LEAVEBALANCE_GROUP)));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				ConfigurationManager.getBundle().getString(LEAVEBALANCE_GROUP)));
		element.click();
		NestUtils.waitForLoader();

		click(SEARCH_BUTTON_LEAVE_BALANCE);
		NestUtils.waitForLoader();
		click(RESET_TEAM_LEAVE_BALANCE);
		NestUtils.waitForLoader();

		leaveBalanceDropdowns =
				NestUtils.getQAFWebElements(SELCTED_LEAVE);
		Validator.verifyThat("Financail Year", leaveBalanceDropdowns.get(0).getText(),
				Matchers.equalTo(originalFinancialYear));
		Validator.verifyThat("LeaveType", leaveBalanceDropdowns.get(1).getText(),
				Matchers.equalTo("All"));
		Validator.verifyThat("Group", leaveBalanceDropdowns.get(2).getText(),
				Matchers.equalTo("All"));
	}

	@QAFTestStep(description = "user click on {0} button on Configure Leave page")
	public void userClickOnButtonOnConfigureLeavePage(String button) {
		String addButton = CommonPage.buttonTextContains(button);
		CommonPage.scrollUpToElement(addButton);
		CommonStep.click(addButton);
	}

	@QAFTestStep(description = "user verify {0} textbox,{1} datepicker,{2} checkbox on Configure Leave page")
	public void userVerifyTextboxDatepickerCheckboxOnConfigureLeavePage(String leaveName,
			String date, String location) {
		String[] labels = {leaveName, date, location};
		for (int i = 0; i < 3; i++) {
			QAFExtendedWebElement label =
					new QAFExtendedWebElement(
							String.format(
									ConfigurationManager.getBundle().getString(
											"label.leaveName.configureLeavePage"),
									labels[i]));
			if (label.isDisplayed()) {
				Reporter.logWithScreenShot(
						labels[i] + VERIFY_MESG_CONFIG,
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						labels[i] + VERIFY_NOT_MESG_CONFIG,
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user enters Leave Name as {0},Date as {1} ,Location as {2} on Configure Leave page")
	public void userEntersLeaveNameAsDateAsLocationAsOnConfigureLeavePage(String str0,
			String str1, String str2) {
		CommonStep.sendKeys(str0, "text.leaveName.configureLeavePage");
		click("icon.calendar.from");
		calendar.selectDate(str1);
		switch (str2) {
			case LOCATION_AHMEDABAD :
				QAFExtendedWebElement loc1 =
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle().getString(
										LOCATION_CONFIGLEAVE),
								LOCATION_AHMEDABAD));
				loc1.click();
				break;
			case "Pune" :
				QAFExtendedWebElement loc2 =
						new QAFExtendedWebElement(
								String.format(
										ConfigurationManager.getBundle().getString(
												LOCATION_CONFIGLEAVE),
										"Pune"));
				loc2.click();
				break;
			case LOCATION_BENGALURU :
				QAFExtendedWebElement loc3 =
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle().getString(
										LOCATION_CONFIGLEAVE),
								LOCATION_BENGALURU));
				loc3.click();
				break;
			default :
				Reporter.log("Location is invalid");

		}
	}

	@QAFTestStep(description = "user verify the {0} message on configure leave page")
	public void userVerifyHolidayIsSavedSuccessfully(String message) {
		CommonPage commonPage = new CommonPage();
		commonPage.verifyLeaveMessage(message);
	}
	@QAFTestStep(description = "user verify {0} Holiday is NOT saved in leave period {1}")
	public void userVerifyHolidayIsNOTSavedSuccessfully(String date, String leavePeriod) {
		List<QAFWebElement> searchDates =
				NestUtils.getQAFWebElements(LIST_CONFIGLEAVE);
		Reporter.log("" + searchDates.size());
		for (int i = 1; i < searchDates.size() - 1; i++) {
			try {
				String value = searchDates.get(i).getText();
				Reporter.log(searchDates.get(i).getText());
				if (value.contains(date)) {
					Reporter.logWithScreenShot(
							date + "Holiday is saved in " + leavePeriod,
							MessageTypes.Fail);
				} else {
					Reporter.logWithScreenShot(
							date + "Holiday is NOT saved in " + leavePeriod,
							MessageTypes.Pass);
				}
			} catch (Exception e) {
				Reporter.logWithScreenShot(
						"Additional search results rows are not displayed for "
								+ leavePeriod,
						MessageTypes.Pass);
			}
		}

	}
	@QAFTestStep(description = "user verify {0} is disabled")
	public void buttondisabled() {
		userClickOnButtonOnConfigureLeavePage("Save");
		QAFExtendedWebElement addButton =
				new QAFExtendedWebElement(CommonPage.buttonTextContains("Add"));
		if (addButton.isDisplayed()) {
			Reporter.logWithScreenShot("Holiday is saved with incorrect data ",
					MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot(
					"Holiday is NOT saved with incorrect data and displays error message ",
					MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user deletes holiday {0}")
	public void userDeletesHoliday(String leaveName) {

		List<QAFWebElement> searchLeaveName =
				NestUtils.getQAFWebElements("list.leaveName.configureLeavePage");
		for (int i = 0; i < searchLeaveName.size() - 1; i++) {
			try {
				String value = searchLeaveName.get(i).getText();
				Reporter.log(searchLeaveName.get(i).getText());
				if (value.contains(leaveName)) {
					QAFWebElement checkbox = CommonPage.getWebElementFromList(
							"checkbox.check.configureLeavePage", Integer.toString(i + 1));
					checkbox.click();
					userClickOnButtonOnConfigureLeavePage("Delete");
					Reporter.logWithScreenShot(leaveName + "Deleted Successfully",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(
							"Search Result doesnt have leave name " + leaveName,
							MessageTypes.Pass);
				}
			} catch (Exception e) {
				Reporter.logWithScreenShot(
						"Additional search results rows are not displayed  ",
						MessageTypes.Pass);
			}
		}
	}

	@QAFTestStep(description = "user verifies Export button functionality on Team Leave Balance page")
	public void verifyExportBtnFunctionality() {
		NestUtils.waitForLoader();
		click("button.export.LeaveBalancePage");
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "user verifies pagination in Team Leave Balance page")
	public void userVerifiesPaginationOnLeaveBalancePage() {
		List<QAFWebElement> pageNumber =
				NestUtils.getQAFWebElements("number.page.travelpage");
		NestUtils.scrollToAxis(0, 800);
		for (int i = 0; i < pageNumber.size(); i++) {
			pageNumber.get(i).click();
		}
	}

	@QAFTestStep(description = "user verifies Reset button functionality on My Leave List page")
	public void verifyResetButtonFunctionalityOnLeaveListPage() {

		String[] searchCriteria = ConfigurationManager.getBundle()
				.getString("leaveBalance.search").split(":");
		NestUtils.waitForLoader();
		clear("from.start.date.leaveList");
		sendKeys(searchCriteria[0], "from.start.date.leaveList");
		NestUtils.waitForLoader();
		clear("to.end.date.leaveList");
		sendKeys(searchCriteria[1], "to.end.date.leaveList");
		NestUtils.waitForLoader();
		List<QAFWebElement> leaveBalanceDropdowns =
				NestUtils.getQAFWebElements(DROPDOWN_LEAVETYPE);
		leaveBalanceDropdowns.get(0).click();
		QAFWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				searchCriteria[2].trim()));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				searchCriteria[2].trim()));
		element.click();
		NestUtils.waitForLoader();

		leaveBalanceDropdowns.get(1).click();
		element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				searchCriteria[3].trim()));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				searchCriteria[3].trim()));
		element.click();
		NestUtils.waitForLoader();

		click(SEARCH_BUTTON_LEAVE_BALANCE);
		NestUtils.waitForLoader();
		click(RESET_TEAM_LEAVE_BALANCE);
		NestUtils.waitForLoader();

		leaveBalanceDropdowns =
				NestUtils.getQAFWebElements(SELCTED_LEAVE);
		Validator.verifyThat("LeaveType", leaveBalanceDropdowns.get(0).getText(),
				Matchers.equalTo("All"));
		Validator.verifyThat("Status", leaveBalanceDropdowns.get(1).getText(),
				Matchers.equalTo("All"));
	}

	@QAFTestStep(description = "user verifies pagination in My Leave List page")
	public void userVerifiesPaginationOnLeaveListPage() {
		userVerifiesPaginationOnLeaveBalancePage();
	}

	@QAFTestStep(description = "user navigate to {0} tab")
	public void userNavigateToTab(String str0) {
		String tab = CommonPage.getStringLocator("link.tabs.configureLeavePage", str0);
		NestUtils.clickUsingJavaScript(tab);
	}
	@QAFTestStep(description = "user click on Add button under Floating Holiday on Configure Leave page")
	public void userClickOnButtonUnderFloatingHolidayOnConfigureLeavePage() {
		CommonPage.scrollUpToElement(BUTTON_CONFIGHOLIDAY);
		CommonStep.click(BUTTON_CONFIGHOLIDAY);
	}
	@QAFTestStep(description = "user verify {0} textbox,{1} datepicker,{2} checkbox for Floating Holiday tab on Configure Leave page")
	public void userVerifyTextboxDatepickerCheckboxForFloatingHolidayTabOnConfigureLeavePage(
			String floatingleaveName, String date, String location) {
		String[] labels = {floatingleaveName, date, location};
		for (int i = 0; i < 3; i++) {
			QAFWebElement label = CommonPage.getWebElementFromList(
					"label.addFloatingHoliday.configLeavePage", labels[i]);
			if (label.isDisplayed()) {
				Reporter.logWithScreenShot(labels[i]
						+ "is available under floating holiday tab on configure leave page",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(labels[i]
						+ "is NOT available under floating holiday tab on configure leave page",
						MessageTypes.Fail);
			}
		}
	}
	@QAFTestStep(description = "user enters Leave Name as {0},Date as {1} ,Location as {2} under foating holidays on Configure Leave page")
	public void enterDataForFloatingHoliday(String str0, String str1, String str2) {
		CommonStep.sendKeys(str0, "text.addFloatingHoliday.configureLeavePage");
		click("iconCalendar.floatingHolidays.configLeavePage");
		calendar.selectDate(str1);
		switch (str2) {
			case LOCATION_AHMEDABAD :
				QAFWebElement loc1 = CommonPage.getWebElementFromList(
						CHECKBOX_CONFIGHOLIDAY, LOCATION_AHMEDABAD);
				loc1.click();
				break;
			case "Pune" :
				QAFWebElement loc2 = CommonPage.getWebElementFromList(
						CHECKBOX_CONFIGHOLIDAY, "Pune");
				loc2.click();
				break;
			case LOCATION_BENGALURU:
				QAFWebElement loc3 = CommonPage.getWebElementFromList(
						CHECKBOX_CONFIGHOLIDAY, LOCATION_BENGALURU);
				loc3.click();
				break;
			default :
				Reporter.log("Location is invalid");

		}
	}
	@QAFTestStep(description = "user click on {0} button under Floating Holiday on Configure Leave page")
	public void userClickOnSaveButtonUnderFloatingHolidayOnConfigureLeavePage(
			String str0) {
		String floatingtabButtons = CommonPage
				.getStringLocator("button.savefloatingHoliday.configLeavePage", str0);
		CommonStep.click(floatingtabButtons);
	}
	@QAFTestStep(description = "user verify data is not saved under flaoting holiday tab")
	public void userVerifyDataIsNotSavedUnderFlaotingHolidayTab() {

		QAFExtendedWebElement addButton =
				new QAFExtendedWebElement(BUTTON_CONFIGHOLIDAY);
		if (addButton.isDisplayed()) {
			Reporter.logWithScreenShot(
					"Holiday is saved with incorrect data under floating holidays tab",
					MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot(
					"Holiday is NOT saved with incorrect data data under floating holidays tab and displays error message ",
					MessageTypes.Pass);
		}
	}
	@QAFTestStep(description = "user deletes holiday {0} under Floating Holiday")
	public void userDeletesHolidayUnderFloatingHoliday(String leaveName) {
		List<QAFWebElement> searchLeaveName =
				NestUtils.getQAFWebElements("list.floatingleaveName.configureLeavePage");
		for (int i = 0; i < searchLeaveName.size(); i++) {
			try {
				String value = searchLeaveName.get(i).getText();
				Reporter.log(searchLeaveName.get(i).getText());
				if (value.contains(leaveName)) {
					QAFWebElement checkbox = CommonPage.getWebElementFromList(
							"checkbox.floatingcheck.configureLeavePage",
							Integer.toString(i + 1));
					checkbox.click();
					CommonStep.click("button.floatingDelete.configLeavePage");
					Reporter.logWithScreenShot(leaveName + "Deleted Successfully",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(
							"Search Result doesnt have leave name " + leaveName,
							MessageTypes.Pass);
				}
			} catch (Exception e) {
				Reporter.logWithScreenShot(
						"Additional search results rows are not displayed  ",
						MessageTypes.Pass);
			}
		}
	}

	@QAFTestStep(description = "user cancel leave from My leave list page {0}")
	public void cancelLeaveFromMyLeaveList(String value) {
		NestUtils.waitForLoader();
		selectValueFromdrop("button.myLeaveList.select", "Cancel");
		scrollPage(300);
		CommonStep.waitForEnabled("button.myLeaveList.submit");
		click("button.myLeaveList.submit");
	}

	@QAFTestStep(description = "user verify the Filter button functionality on {0}")
	public void verifyFilterIconfunctionality(String pageName) {
		QAFWebElement searchButton = new QAFExtendedWebElement(SEARCH_BUTTON_COMMON);
		NestUtils.waitForLoader();
		CommonStep.click(FILTER_BUTTON);
		NestUtils.waitForLoader();
		if (searchButton.isPresent()) {
			Reporter.logWithScreenShot("Filter is NOT hidden on " + pageName,
					MessageTypes.Fail);
		} else {
			Reporter.logWithScreenShot("Filter is hidden on " + pageName,
					MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user verifies mandatory fields with {0} error message on leave page")
	public void verifyErrorMessageOnLeavePage(String message) {
		QAFExtendedWebElement element =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("text.tooltip.addexpensecategorypage"), message));
		List<QAFWebElement> title = NestUtils.getQAFWebElements(DROPDOWN_LEAVETYPE);
		for (int i = 0; i < title.size(); i++) {
			Validator.verifyThat("FieldBorderColor", title.get(i).getCssValue("color")
					.equals(ExpensePage.BORDERCOLOR_DROPDOWN), Matchers.equalTo(true));
			new Actions(driver).moveToElement(title.get(i)).build().perform();
			if (element.verifyPresent()) {
				Reporter.log("This is a required field is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("This is a required field is not displayed",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user click on Apply button on Apply Leave Page")
	public void clickOnApplyLeaveButtonforLeavePage() {
		NestUtils.scrollToAxis(0, 250);
		CommonPage.waitForLocToBeClickable(APPLY_LEAVE_BUTTON);
		click(APPLY_LEAVE_BUTTON);
	}

	@QAFTestStep(description = "user verify {0} Holiday is NOT saved under floating holiday tab")
	public void userVerifyHolidayIsNOTSavedInderFloatingHolidayTab(String date) {
		List<QAFWebElement> searchDates =
				NestUtils.getQAFWebElements(LIST_CONFIGLEAVE);
		Reporter.log("" + searchDates.size());
		for (int i = 0; i < searchDates.size() - 1; i++) {
			try {
				String value = searchDates.get(i).getText();
				Reporter.log(searchDates.get(i).getText());
				if (value.contains(date)) {
					Reporter.logWithScreenShot(date + "Holiday is saved ",
							MessageTypes.Fail);
				} else {
					Reporter.logWithScreenShot(date + "Holiday is NOT saved  ",
							MessageTypes.Pass);
				}
			} catch (Exception e) {
				Reporter.logWithScreenShot(
						"Additional search results rows are not displayed ",
						MessageTypes.Pass);
			}
		}
	}

	@QAFTestStep(description = "user verifies Search functionality for employee {0} and leave type {1}  on Team Leave Balance page")
	public void verifySearchtButtonFunctionality(String employeeName, String leaveType) {
		NestUtils.waitForLoader();

		selectValueFromdrop("dropdown.leavePeriod",
				ConfigurationManager.getBundle().getString("financialYear.leaveBalance"));
		NestUtils.waitForLoader();
		click("button.teamLeave.balance.EmployeeName");
		waitForVisible(EMPLOYEE_NAME_TEXTBOX);
		clear(EMPLOYEE_NAME_TEXTBOX);
		sendKeys(employeeName, EMPLOYEE_NAME_TEXTBOX);
		NestUtils.getQAFWebElements(EMPLOYEE_NAME_TEXTBOX).get(0).sendKeys(Keys.ENTER);
		NestUtils.waitForLoader();
		List<QAFWebElement> leaveBalanceDropdowns =
				NestUtils.getQAFWebElements(DROPDOWN_LEAVETYPE);
		leaveBalanceDropdowns.get(0).click();
		QAFWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				leaveType));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				leaveType));
		element.click();
		NestUtils.waitForLoader();

		leaveBalanceDropdowns.get(1).click();
		element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				ConfigurationManager.getBundle().getString(LEAVEBALANCE_GROUP)));
		waitForVisible(String.format(
				ConfigurationManager.getBundle().getString(LEAVE_TYPE_DROPDOWN),
				ConfigurationManager.getBundle().getString(LEAVEBALANCE_GROUP)));
		element.click();
		NestUtils.waitForLoader();

		click(SEARCH_BUTTON_LEAVE_BALANCE);
		NestUtils.waitForLoader();
		if (NestUtils.getQAFWebElements("text.employeeNames.LeaveBalance").size() > 0) {
			List<QAFWebElement> listOfElement =
					NestUtils.getQAFWebElements("text.team.leave.balance.type");
			for (QAFWebElement ele : listOfElement) {
				Validator.verifyThat(ele.getText().toString().trim(),
						Matchers.equalToIgnoringWhiteSpace(leaveType));
			}

			listOfElement =
					NestUtils.getQAFWebElements("text.employeeNames.LeaveBalance");
			for (QAFWebElement ele : listOfElement) {
				Validator.verifyThat(ele.getText().toString().trim(),
						Matchers.equalToIgnoringWhiteSpace(employeeName));
			}
		} else {
			Reporter.logWithScreenShot("Data not Found for employee " + employeeName
					+ " and Leave type " + leaveType, MessageTypes.Warn);
		}
	}
}
