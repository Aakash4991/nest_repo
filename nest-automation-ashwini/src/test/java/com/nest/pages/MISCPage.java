package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class MISCPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	public static final String CARD_LIST = "list.card.misc";
	public static final String MESSAGE_ON_CARD = "message.cardsection.misc";
	public static final String SELECT_BIRTHDAYCARD = "select.birthdaycard.misc";
	public static final String COMMENT_ON_CARD = "textArea.CommentSection.misc";
	public static final String POST_BUTTON = "btn.post.r&r";

	CommonPage commonPage = new CommonPage();
	PublisherPage publisherPage = new PublisherPage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// predefined function
	}

	@QAFTestStep(description = "user should see Birthday Calendar page")
	public void verifyBirthdayCalendarPage() {
		waitForNotVisible(CommonPage.WAITLOADER);
		verifyPresent(CommonPage.COMMON_BREADCRUMB);

	}

	@QAFTestStep(description = "user should see Employment Anniversary page")
	public void verifyEmploymentAnniversaryPage() {
		waitForNotVisible(CommonPage.WAITLOADER);
		verifyPresent(CommonPage.COMMON_BREADCRUMB);
	}
	@QAFTestStep(description = "user select any employee name")
	public void userSelectAnyEmployeeName() {

		waitForNotVisible(CommonPage.WAITLOADER);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonStep.waitForEnabled("list.EmpHavingbirthday.birthdaycalenderpage");

		List<QAFWebElement> empName = NestUtils
				.getQAFWebElements("list.EmpHavingbirthday.birthdaycalenderpage");
		NestUtils.scrollUpToElement(
				new QAFExtendedWebElement(CommonPage.COMMON_BREADCRUMB));
		if (empName.isEmpty()) {
			Reporter.log("No employee is having birthday in current month");
		}
		try {
			empName.get(2).click();
			CommonStep.waitForEnabled(CommonPage.COMMON_BREADCRUMB);
			Reporter.log("Clicked successfully");
			waitForPageToLoad();
		} catch (Exception ex) {
			Reporter.log("failed to click an emp");
			Reporter.log(ex.getMessage(), MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see the breadcrumbs")
	public void userShouldSeeTheBreadcrumbs() {

		String[] expected = {"Home", "MISC", "Birthday Calender"};
		for (int i = 0; i <= expected.length; i++) {
			QAFExtendedWebElement breadcrumb = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle()
							.getString("list.breadcrum.userhomepage"), expected[i]));
			if (breadcrumb.equals(expected[i])) {
				Reporter.log("breadcrumbs Verified");
			}
		}
	}

	@QAFTestStep(description = "user should see required fields {0} and {1} with asterisk sign")
	public void userShouldSeeRequiredFieldsAndWithAsteriskSign(String str0, String str1) {
		verifyPresent("symbol.mandatoryfield1.birthdayCalendar");
		verifyPresent("symbol.mandatoryfield2.birthdayCalendar");
	}

	@QAFTestStep(description = "user should verify Breadcrumb {0}")
	public void userShouldVerifyBreadcrumb(String breadcrumb) {
		publisherPage.verifyBreadcrumbs("breadcrumb.mypost.publisher", breadcrumb.trim());
	}

	@QAFTestStep(description = "User select a card")
	public void userSelectACard() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		CommonStep.waitForNotVisible("loader.userhomepage");
		NestUtils.scrollUpToElement(
				new QAFExtendedWebElement(CommonPage.COMMON_BREADCRUMB));

		try {

			NestUtils.waitForElementToBeClickable(new QAFExtendedWebElement(CARD_LIST));
			CommonStep.waitForEnabled(CARD_LIST);
		} catch (Exception e) {
			Reporter.log("card is not loaded" + e.getMessage());
		}

		List<QAFWebElement> cards = NestUtils.getQAFWebElements(CARD_LIST);
		NestUtils.waitForElementToBeClickable(new QAFExtendedWebElement(CARD_LIST));

		cards.get(2).click();
	}
	@QAFTestStep(description = "card should be highlighted")
	public void cardShouldBeHighlighted() {
		verifyPresent("img.highlightedcard.misc");
		Reporter.log("Seleted card is highlighted");

	}

	@QAFTestStep(description = "user should see {0} cards and {1} drop downs and message box")
	public void userShouldSeeCards(long numberOfCards) {
		NestUtils.scrollUpToElement(
				new QAFExtendedWebElement(CommonPage.COMMON_BREADCRUMB));
		List<QAFWebElement> cards = NestUtils.getQAFWebElements(CARD_LIST);
		if (cards.size() == numberOfCards) {
			Reporter.log("Number of cards are 5");
		}
		CommonStep.waitForVisible(COMMENT_ON_CARD);
		if (verifyPresent(COMMENT_ON_CARD)) {
			Reporter.log("Message box is present");
		}
		if (verifyPresent(CommonPage.buttonTextContains("Post"))) {
			Reporter.log("Post button is Present");
		}
		if (verifyPresent(CommonPage.buttonTextContains("Preview"))) {
			Reporter.log("Preview button is Present");
		}
		if (verifyPresent(CommonPage.buttonTextContains("Cancel"))) {
			Reporter.log("Cancel button is Present");
		}

	}

	@QAFTestStep(description = "user write Message and click on preview")
	public void userWriteMessageAndClickOnPreview() {
		waitForNotVisible(CommonPage.WAITLOADER);
		CommonStep.sendKeys("\n" + "Enjoy!!!", COMMENT_ON_CARD);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		CommonPage.clickUsingJavaScript(CommonPage.buttonTextContains("Preview"));
	}

	@QAFTestStep(description = "user should see preview of card")
	public void userShouldSeePreviewOfCard() {
		CommonStep.waitForEnabled("img.cross.birthdayCard.misc");
		verifyPresent("img.cross.birthdayCard.misc");
		verifyPresent("label.Preview.birthdayCard.misc");

		Reporter.log("Preview can be seen");
	}
	@QAFTestStep(description = "user text messagebox on Employment Anniversary page")
	public void messagebox() {

		waitForPageToLoad();
		verifyPresent(MESSAGE_ON_CARD);
		CommonStep.sendKeys(
				"Wish you a Happy Work Anniversary! Congratulations on completion of your 7th year at Infostretch.",
				MESSAGE_ON_CARD);
	}

	@QAFTestStep(description = "user select card on birthday calender page")
	public void selectCards() {
		waitForPageToLoad();

		verifyPresent(SELECT_BIRTHDAYCARD);
		Validator.verifyThat("Card is present", verifyPresent(SELECT_BIRTHDAYCARD),
				Matchers.equalTo(true));
		CommonPage.clickUsingJavaScript(SELECT_BIRTHDAYCARD);

	}
	@QAFTestStep(description = "user text messagebox on birthday calender page")
	public void textmessagebox() {

		waitForVisible(MESSAGE_ON_CARD);
		verifyPresent(MESSAGE_ON_CARD);
		CommonStep.sendKeys("Wish you a very Happy Birthday!", MESSAGE_ON_CARD);
	}
	@QAFTestStep(description = "user click post button")
	public void selectPost() {

		QAFWebElement postButton = new QAFExtendedWebElement(POST_BUTTON);
		NestUtils.scrollUpToElement(postButton);
		waitForVisible(POST_BUTTON);
		Validator.verifyThat("button is present", verifyPresent(POST_BUTTON),
				Matchers.equalTo(true));
		NestUtils.clickUsingJavaScript(postButton);

	}
	@QAFTestStep(description = "user select employee name on Employment Anniversary page")
	public void verifyEmployeenamePage() {

		NestUtils.getQAFWebElements("list.EmpAnniversary.misc");
		List<QAFWebElement> empName =
				NestUtils.getQAFWebElements("list.EmpAnniversary.misc");

		empName.get(1).click();
	}
	@QAFTestStep(description = "user select card on Employment Anniversary page")
	public void card() {
		verifyPresent("select.Aniversarycard.misc");
		Validator.verifyThat("Card is present",
				verifyPresent("select.Aniversarycard.misc"), Matchers.equalTo(true));
		CommonPage.clickUsingJavaScript("select.Aniversarycard.misc");

	}

	@QAFTestStep(description = "user should verify {0} Page")
	public void userShouldVerifyPage(String header) {
		waitForNotVisible(CommonPage.WAITLOADER);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		String s = CommonStep.getText(CommonPage.COMMON_BREADCRUMB).trim();
		if (s.equalsIgnoreCase(header)) {
			Reporter.logWithScreenShot("User is on" + " Page");
		} else Reporter.logWithScreenShot("Failed to reach the page");

	}
}
	

