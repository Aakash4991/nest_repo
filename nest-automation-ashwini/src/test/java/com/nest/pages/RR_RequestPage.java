package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.awt.AWTException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.RandomStringUtils;
import org.hamcrest.Matchers;

import com.nest.components.RRRequestComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RR_RequestPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	public static final String TO_DATE_RNR_REQUESTPAGE = "date.to.rnrrequestpage";
	public static final String SELECT_NOMINEE = "Select Nominee";
	public static final String SELECT_PROJECT = "Select Project";
	public static final String MANAGER_STATUS = "Manager Status";
	public static final String HR_STATUS = "HR Status";
	public static final String PENDING = "Pending";
	public static final String HR_ADMIN = "HR/Admin";
	public static final String PATON_BACK = "Pat On Back";
	public static final String BRIGHT_SPARK = "Bright Spark";
	public static final String PAGINATION_LIST = "pagination.number.list";

	static Calendar calender = Calendar.getInstance();
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	static final String AVAILABLE_FIELDS_BUTTON = "button.avlfields.nominatepages";
	static final String INPUT_TEXTAREA = "imput.textarea";
	static final String LINK_POB = "link.PatOnBack.MyRewardPageListPage.rnr";

	CommonPage commonPage = new CommonPage();
	RnRPage rnrPage = new RnRPage();
	@FindBy(locator = "rr.requests.table")
	private List<RRRequestComponent> requestList;

	@FindBy(locator = PAGINATION_LIST)
	private List<QAFWebElement> paginations;

	public QAFExtendedWebElement getElement(String loc, String text) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), text));
		return element;
	}

	@QAFTestStep(description = "user select {0} where status is {1}")
	public void userSelectWhereStatusIs(String filterLabel, String filterOption) {
		NestUtils.clickUsingJavaScript(getElement("filter.label", filterLabel));
		List<QAFWebElement> filterValues = NestUtils
				.getQAFWebElements(ConfigurationManager.getBundle().getString("filter.value.list"));
		for (QAFWebElement filterByValue : filterValues) {
			String username = filterOption.replace(".", " ");
			if (filterByValue.getText().toString().equals(username)) {
				if (filterLabel.equalsIgnoreCase(SELECT_NOMINEE) || filterLabel.equalsIgnoreCase(SELECT_PROJECT)) {
					waitForVisible("input.search.box");
					sendKeys(username, "input.search.box");
				}
				NestUtils.scrollUpToElement(filterByValue);
				NestUtils.clickUsingJavaScript(filterByValue);
				break;
			}
		}
	}

	@QAFTestStep(description = "user verifies R&R Requests where {0} is {1}")
	public void RRRequestsPage(String filtername, String filterValue) {
		verifyTableColumnsForRRRequests();
		verifyFilter(filtername, filterValue);
	}

	private void verifyFilter(String filtername, String filterValue) {
		List<QAFWebElement> statusList = NestUtils.getQAFWebElements(String
				.format(ConfigurationManager.getBundle().getString("request.list.table.column.values"), filtername));
		for (QAFWebElement status : statusList) {
			Validator.verifyThat("status is " + filterValue, status.getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(filterValue));
		}
	}

	public void verifyTableColumnsForRRRequests() {
		Validator.verifyThat("Nominator column is visible", getElement("table.column", "Nominee").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward Name column is visible", getElement("table.column", "Location").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Nominee column is visible", getElement("table.column", "Reward").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Department column is visible", getElement("table.column", "Posted Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Manager Status column is visible",
				getElement("table.column", MANAGER_STATUS).isDisplayed(), Matchers.equalTo(true));
		Validator.verifyThat("HR Status column is visible", getElement("table.column", HR_STATUS).isDisplayed(),
				Matchers.equalTo(true));
	}

	public void showAllButtons() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Approve"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Reject"));
	}

	private void showApprovebutton() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Approve"));
	}

	public void showOnlyBackbutton() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back"));
		verifyNotVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Approve"));
		verifyNotVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Reject"));

	}

	public boolean isApproveButtonVisible() {
		NestUtils.waitForLoader();
		NestUtils.scrollUpToElement(getElement(RnRPage.BUTTON_NAME, "Back"));
		NestUtils.waitForElementToBeClickable(getElement(RnRPage.BUTTON_NAME, "Back"));
		NestUtils.waitForLoader();
		if (getElement(RnRPage.BUTTON_NAME, "Approve").isPresent()) {
			return true;
		} else {
			NestUtils.waitForLoader();
			NestUtils.clickUsingJavaScript(getElement(RnRPage.BUTTON_NAME, "Back"));
			NestUtils.waitForLoader();
		}
		return false;
	}

	public void showButtonsAccordingtoStatus(String userrole, String statusName, String columnName) {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back")));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString(RnRPage.BUTTON_NAME), "Back"));

		if (userrole.equals("manager") && (columnName.equals(MANAGER_STATUS)) && statusName.equals(PENDING)) {
			showAllButtons();
		} else if (userrole.equals(HR_ADMIN) && (columnName.equals(MANAGER_STATUS)) && statusName.equals("Approved")) {
			showAllButtons();
		} else if (userrole.equals("manager") && (columnName.equals(MANAGER_STATUS)) && statusName.equals("Approved")) {
			showOnlyBackbutton();
		} else if (userrole.equals(HR_ADMIN) && (columnName.equals(MANAGER_STATUS)) && statusName.equals(PENDING)) {
			showOnlyBackbutton();
		} else if (userrole.equals(HR_ADMIN) && (columnName.equals(HR_STATUS)) && statusName.equals(PENDING)) {
			showApprovebutton();
		}

	}

	@QAFTestStep(description = "user verifies R&R Request list for {0} having staus {1} for {2}")
	public void vereifyRRRequestsList(String userType, String status, String fieldName) {
		showButtonsAccordingtoStatus(userType, status, fieldName);

	}

	@QAFTestStep(description = "user verify {page_name} of nominate module with all fileds")
	public void verifyNominatePages(String pageName) {

		CommonStep.verifyPresent("tab.cardselection.nominatepages");
		int totalCards = NestUtils.getQAFWebElements("cards.cardselection.nominatepages").size();
		switch (pageName) {
		case "You Made My Day":
			Validator.verifyThat("Select Card Block must have 5 cards.", totalCards == 5, Matchers.equalTo(true));
			CommonStep.verifyPresent("imput.comment.youmademydaypage");
			break;

		case "Pat On Back":
			Validator.verifyThat("Select Card Block must have 3 cards.", totalCards == 3, Matchers.equalTo(true));

			Validator
					.verifyThat("Enter Contributions text area should be present",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString(INPUT_TEXTAREA), "Enter Contributions"))
											.isPresent(),
							Matchers.equalTo(true));

			Validator.verifyThat("Select Project dropdown should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), SELECT_PROJECT))
									.isPresent(),
					Matchers.equalTo(true));
			break;

		case "Bright Spark":
			Validator.verifyThat("Select Card Block must have 3 cards.", totalCards == 3, Matchers.equalTo(true));
			Validator.verifyThat("Enter challenging situations text area should be present",
					new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString(INPUT_TEXTAREA),
							"Enter challenging situations")).isPresent(),
					Matchers.equalTo(true));
			Validator.verifyThat("Enter solutions provided text area should be present",
					new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString(INPUT_TEXTAREA),
							"Enter solutions provided")).isPresent(),
					Matchers.equalTo(true));
			Validator.verifyThat("Enter benefits accrued text area should be present", new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle().getString(INPUT_TEXTAREA), "Enter benefits accrued"))
							.isPresent(),
					Matchers.equalTo(true));
			Validator
					.verifyThat("Enter Contributions text area should be present",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString(INPUT_TEXTAREA), "Enter contributions"))
											.isPresent(),
							Matchers.equalTo(true));

			Validator.verifyThat("Select Project dropdown should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), SELECT_PROJECT))
									.isPresent(),
					Matchers.equalTo(true));
			break;
		}

		Validator.verifyThat("Select groups dropdown should be present",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Select Group"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Select Band dropdown should be present",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Select Band"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Select Nominee dropdown should be present",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), SELECT_NOMINEE))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Post button should be present",
				new QAFExtendedWebElement(
						String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Post"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Preview button should be present",
				new QAFExtendedWebElement(
						String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Preview"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Cancel button should be present",
				new QAFExtendedWebElement(
						String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Cancel"))
								.isPresent(),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user should see required filed with astric sign for {page_name} of nominate module")
	public void verifyAstersikSign(String pageName) {

		Validator.verifyThat("Select a Card field having astric sign",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Select a Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator
				.verifyThat("Nominee field having astric sign",
						new QAFExtendedWebElement(String
								.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Nominee"))
										.getText().contains("*"),
						Matchers.equalTo(true));

		Validator.verifyThat("Message on Card field having astric sign",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Message on Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		switch (pageName) {
		case "Pat On Back":
			Validator.verifyThat("Project field having astric sign",
					new QAFExtendedWebElement(String
							.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Project"))
									.getText().contains("*"),
					Matchers.equalTo(true));

			Validator.verifyThat("Key Contribution by Nominee field having astric sign",
					new QAFExtendedWebElement(
							String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON),
									"Key Contribution by Nominee")).getText().contains("*"),
					Matchers.equalTo(true));

			break;

		case "Bright Spark":
			Validator.verifyThat("Project field having astric sign",
					new QAFExtendedWebElement(String
							.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Project"))
									.getText().contains("*"),
					Matchers.equalTo(true));

			Validator.verifyThat("Challenging situations faced field having astric sign",
					new QAFExtendedWebElement(
							String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON),
									"Challenging situations faced")).getText().contains("*"),
					Matchers.equalTo(true));

			Validator.verifyThat("Solutions provided/Situation handling field having astric sign",
					new QAFExtendedWebElement(
							String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON),
									"Solutions provided/Situation handling")).getText().contains("*"),
					Matchers.equalTo(true));

			Validator.verifyThat("Rationale for Nomination field having astric sign",
					new QAFExtendedWebElement(
							String.format(ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON),
									"Rationale for Nomination")).getText().contains("*"),
					Matchers.equalTo(true));

			Validator.verifyThat("Benefits accrued field having astric sign",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString(AVAILABLE_FIELDS_BUTTON), "Benefits accrued"))
									.getText().contains("*"),
					Matchers.equalTo(true));
			break;

		}

	}

	@SuppressWarnings("static-access")
	public String getRandomString(int count) {
		RandomStringUtils util = new RandomStringUtils();
		return util.randomAlphabetic(count);
	}

	@QAFTestStep(description = "user role {0} responds {1} request to {2} for reward {3}")
	public void approveOrRejectRequest(String usertype, String fromStatus, String toStatus, String reward) {
		NestUtils.scrollUpToElement(getElement(RnRPage.BUTTON_NAME, toStatus));
		getElement(RnRPage.BUTTON_NAME, toStatus).click();
		if (reward.equals(PATON_BACK)) {
			if (usertype.equalsIgnoreCase("manager"))
				writeComment();
		} else if (reward.equals(BRIGHT_SPARK)) {
			writeComment();
		}
		if (toStatus.equalsIgnoreCase("Approve")) {
			verifyVisible("success.msg");
			Validator.verifyThat("request approved message visible!",
					CommonStep.getText("success.msg").contains("Request approved successfully"),
					Matchers.equalTo(true));
		} else {
			Validator.verifyThat("request rejected message visible!",
					CommonStep.getText("success.msg").contains("Request rejected successfully"),
					Matchers.equalTo(true));
		}
		waitForNotVisible("success.msg");
		waitForNotPresent("loading.bar");
	}

	private void writeComment() {
		String comment = getRandomString(20);
		sendKeys(comment, ConfigurationManager.getBundle().getString("input.comment.field"));
		getElement(RnRPage.BUTTON_NAME, "Submit").click();
	}

	private void clickReward(List<RRRequestComponent> requestList, int i, int paginationNum) {

		requestList.get(i).getRewardName().get(i).waitForVisible();
		ConfigurationManager.getBundle().setProperty("EXPECTED_NOMINEE",
				requestList.get(i).getNominee().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_LOCATION",
				requestList.get(i).getLocationName().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_REWARD",
				requestList.get(i).getRewardName().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_MANAGER_STATUS",
				requestList.get(i).getManagerStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_HR_STATUS",
				requestList.get(i).getHrStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_POSTED_DATE",
				requestList.get(i).getPostedDate().get(i).getText());
		ConfigurationManager.getBundle().setProperty("INDEX", i);
		ConfigurationManager.getBundle().setProperty("PAGINATION_INDEX", paginationNum);
		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript(requestList.get(i).getRewardName().get(i));
		NestUtils.waitForLoader();
	}

	@QAFTestStep(description = "verify status is changed from {0} to {1} for {2}")
	public void verifyStatusIsChanged(String fromStatus, String toStatus, String filterColumn) {
		String nominee = ConfigurationManager.getBundle().getProperty("EXPECTED_NOMINEE").toString();
		String location = ConfigurationManager.getBundle().getProperty("EXPECTED_LOCATION").toString();
		String reward = ConfigurationManager.getBundle().getProperty("EXPECTED_REWARD").toString();
		String managerstatus = ConfigurationManager.getBundle().getProperty("EXPECTED_MANAGER_STATUS").toString();
		String hrstatus = ConfigurationManager.getBundle().getProperty("EXPECTED_HR_STATUS").toString();
		String posteddate = ConfigurationManager.getBundle().getProperty("EXPECTED_POSTED_DATE").toString();
		int index = Integer.parseInt(ConfigurationManager.getBundle().getProperty("INDEX").toString());
		Validator.verifyThat("Nominee name verified in list!",
				requestList.get(index).getNominee().get(index).getText().equals(nominee), Matchers.equalTo(true));
		Validator.verifyThat("location name verified in list!",
				requestList.get(index).getLocationName().get(index).getText().equals(location), Matchers.equalTo(true));
		Validator.verifyThat("Reward name verified in list!",
				requestList.get(index).getRewardName().get(index).getText().equals(reward), Matchers.equalTo(true));
		if (filterColumn.equals("manager")) {
			Validator.verifyThat("Manager staus verified in list!",
					requestList.get(index).getManagerStatus().get(index).getText().trim().equalsIgnoreCase(toStatus),
					Matchers.equalTo(true));
		} else {
			Validator.verifyThat("Manager staus verified in list!", requestList.get(index).getManagerStatus().get(index)
					.getText().trim().equalsIgnoreCase(managerstatus), Matchers.equalTo(true));

		}
		if (filterColumn.equalsIgnoreCase(HR_ADMIN)) {
			Validator.verifyThat("HR status appoved verified in list!",
					requestList.get(index).getHrStatus().get(index).getText().equals(toStatus), Matchers.equalTo(true));
		} else {
			Validator.verifyThat("HR status appoved verified in list!",
					requestList.get(index).getHrStatus().get(index).getText().equals(hrstatus), Matchers.equalTo(true));
		}

		Validator.verifyThat("Posted date appoved verified in list!",
				requestList.get(index).getPostedDate().get(index).getText().equals(posteddate), Matchers.equalTo(true));

	}

	public void sortColumn(String columnName) {
		getElement("column.name", columnName).waitForVisible();
		NestUtils.clickUsingJavaScript("column.name", columnName);
		waitForNotPresent(ConfigurationManager.getBundle().getString("loading.bar"));
		getElement("column.name", columnName).waitForVisible();
		NestUtils.clickUsingJavaScript("column.name", columnName);
		waitForNotPresent(ConfigurationManager.getBundle().getString("loading.bar"));
	}

	public boolean statusFoundFromRequests(String columnName, String status, int index) {
		Reporter.log("hiiiii");
		Reporter.log(requestList.get(index).getManagerStatus().get(index).getText());
		if (columnName.equalsIgnoreCase(MANAGER_STATUS)) {

			return requestList.get(index).getManagerStatus().get(index).getText().trim().equalsIgnoreCase(status);
		} else if (columnName.equalsIgnoreCase(HR_STATUS)) {
			return requestList.get(index).getHrStatus().get(index).getText().trim().equals(status);
		}
		return false;

	}

	public void clickPagination(List<QAFWebElement> paginations, int index, int pageindex) {
		index = 0;
		waitForVisible(PAGINATION_LIST);
		paginations.get(pageindex).click();
		NestUtils.waitForLoader();
		pageindex++;
	}

	@QAFTestStep(description = "user clicks on {0} reward {1} by {2}")
	public void clickOnRewards(String reward, String status, String columnName) {
		int pageindex = 0;
		boolean RRRequestFound = false;
		boolean approveRecordfound = false;
		NestUtils.waitForLoader();
		int size = requestList.size();
		for (int i = 0; i < size; i++) {
			boolean statusFound = statusFoundFromRequests(columnName, status, i);
			boolean rewardFound = requestList.get(i).getRewardName().get(i).getText().equalsIgnoreCase(reward);
			RRRequestFound = statusFound && rewardFound;
			approveRecordfound = false;
			if (RRRequestFound) {
				RRRequestFound = true;
				NestUtils.waitForLoader();
				clickReward(requestList, i, pageindex);
				NestUtils.waitForLoader();
				if (isApproveButtonVisible()) {
					approveRecordfound = true;
					break;
				} else {
					approveRecordfound = false;
					if (pageindex == paginations.size()) {
						break;
					}
				}
				if (!approveRecordfound && (i == size - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible(PAGINATION_LIST);
					NestUtils.clickUsingJavaScript(paginations.get(pageindex));
					NestUtils.waitForLoader();
					pageindex++;
				}

			} else {
				if (pageindex == paginations.size()) {
					break;
				}
				if ((i == size - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible(PAGINATION_LIST);

					paginations.get(pageindex).click();
					NestUtils.waitForLoader();
					pageindex++;
				}
			}
		}
		if (!RRRequestFound || !approveRecordfound) {
			Reporter.log("No proper record found in existing request!", MessageTypes.Fail);
		}
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// PAGE

	}

	@QAFTestStep(description = "usertype {0} clicks on {1} reward {2} by {3}")
	public void chooseStatusByUserRole(String usertype, String reward, String statusName, String columnName) {
		boolean RRRequestFound = false;
		int pageindex = 0;

		for (int i = 0; i < requestList.size(); i++) {
			boolean statusFound = statusFoundFromRequests(columnName, statusName, i);
			boolean rewardFound = requestList.get(i).getRewardName().get(i).getText().equalsIgnoreCase(reward);
			RRRequestFound = statusFound && rewardFound;
			if (RRRequestFound) {
				requestList.get(i).getRewardName().get(i).click();
				break;
			} else {
				if (pageindex == paginations.size()) {
					break;
				}
				if ((i == requestList.size() - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible(PAGINATION_LIST);
					paginations.get(pageindex).click();

					NestUtils.waitForLoader();
					pageindex++;
				}
			}
		}

	}

	@QAFTestStep(description = "user {0} nominates {1} for {2}")
	public void nominateAward(String username, String nomineeName, String awardName) throws InterruptedException {
		new UserHomePage().verifyHomePage();
		NavigationPage.menuNavigation(awardName);
		new ReportsPage().userSelectsACard();
		new RR_RequestPage().userSelectWhereStatusIs(SELECT_NOMINEE, nomineeName);
		new RR_RequestPage().userSelectWhereStatusIs(SELECT_PROJECT, "test client");
		if (awardName.contains(BRIGHT_SPARK)) {
			userEnterRequiredDetailsForBrightSpark();

		} else if (awardName.contains(PATON_BACK)) {
			new ReportsPage().userEnterKeyContributions();
			new ReportsPage().postsComment();
		}
		new ReportsPage().userClickOnPostButton("Post");
	}

	@QAFTestStep(description = "Manager {0} of nominee {1} request for {2}")
	public void responseToRequestByManager(String username, String response, String awardName)
			throws AWTException, InterruptedException {
		new LandingPage().switchToManagerView();
		NavigationPage.menuNavigation("R & R : R & R Requests");
		new RR_RequestPage().clickOnRewards(awardName, PENDING, MANAGER_STATUS);
		approveOrRejectRequest("manager", PENDING, "Approve", awardName);
	}

	@QAFTestStep(description = "user selects and verify date")
	public void userSelectsAndVerifyDate() {
		String fromDate = new QAFExtendedWebElement("datepiker.rnrrequestpage").getAttribute("value");
		String toDate = new QAFExtendedWebElement(TO_DATE_RNR_REQUESTPAGE).getAttribute("value");
		isValidDate(fromDate);
		isValidDate(toDate);
		checkTwoMonthsBeforeDate(fromDate);
		checkEndOfMonth(toDate);
	}

	public static boolean isValidDate(String inDate) {
		Date date = null;
		try {
			date = sdf.parse(inDate);
			if (!inDate.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		if (date == null) {
			Reporter.logWithScreenShot(date + "\t Date is in the format dd-MMMM-yyyy", MessageTypes.Pass);
			return false;
		} else {
			Reporter.logWithScreenShot(date + "\t  Date is not in the format dd-MMMM-yyyy", MessageTypes.Fail);
			return true;
		}
	}

	public static boolean checkTwoMonthsBeforeDate(String date) {
		calender.add(Calendar.MONTH, -ConfigurationManager.getBundle().getInt("months.add"));
		int monthsToAdd = ConfigurationManager.getBundle().getInt("months.add");
		int month = (calender.get(Calendar.MONTH) + monthsToAdd);
		String getMonth = Integer.toString(month);
		if (date.contains(getMonth)) {
			Reporter.logWithScreenShot(date+"\t Date is" + monthsToAdd + " months before current date", MessageTypes.Pass);
			return true;
		} else {
			Reporter.logWithScreenShot(date+"\tDate is not" + monthsToAdd + " months before current date", MessageTypes.Fail);
			return false;
		}

	}

	public static void checkEndOfMonth(String date) {
		LocalDate today = LocalDate.now();
		String lastDay = today.withDayOfMonth(today.lengthOfMonth()).toString();
		try {
			Date date1 = sdf.parse(date);
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			ReportsPage.verifyDate(sdf.format(date1), lastDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	@QAFTestStep(description = "user should see reward card and back button")
	public void verifyRewardCardAndBackBtn() {
		CommonStep.verifyPresent("img.rewardCard.rnr");
		CommonStep.verifyPresent(CommonPage.buttonTextContains("Back"));

	}

	@QAFTestStep(description = "user should see + sign on screen")
	public void userShouldSeeSignOnScreen() {
		CommonStep.verifyPresent("symbol.MyRewardPageList.rnr");
	}

	@QAFTestStep(description = "user should see pagination")
	public void userShouldSeePagination() {
		CommonStep.verifyPresent("pagination.MyRewardListPage.rnr");
	}

	@QAFTestStep(description = "user should see the {0}")
	public void userShouldSeeThe(String key) {

	}

	@QAFTestStep(description = "user enter required details to nominate for bright spark")
	public void userEnterRequiredDetailsForBrightSpark() {
		sendKeys(getRandomString(20), "txt.challenging.situations");
		sendKeys(getRandomString(20), "txt.solutions.provided");
		sendKeys(getRandomString(20), "txt.benefits.accured");
		sendKeys(getRandomString(20), "txt.rationale.nomination");
	}

	private void verifyBlocksinRequestPage() {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement("request.details.label.nominee"));
		verifyVisible("request.details.label.nominee");
		verifyVisible("request.details.label.project");
		verifyVisible("request.details.label.reward.name");
		verifyVisible("request.details.label.posted.date");
		verifyVisible("request.details.label.nominated.by");
		verifyVisible("request.details.label.manager");
		verifyVisible("request.details.label.manager.status");
		verifyVisible("request.details.label.hr.status");
	}

	@QAFTestStep(description = "user selects from date {0} from calender")
	public void userSelectsFromDateFromCalender(String date) {
		CommonPage.waitForLocToBeClickable(TO_DATE_RNR_REQUESTPAGE);
		selectDate(TO_DATE_RNR_REQUESTPAGE, "btn.all.date.rnrrequestpage", date);
	}

	private void verifyStatusInBlocks(String fieldName, String values) {
		String[] fields = fieldName.split(" : ");
		String[] valueInFields = values.split(" : ");
		for (int i = 0; i < fields.length; i++) {
			Validator.verifyThat("Status values verified",
					getElement("request.details.value.by.field", fields[i].trim()).getText()
							.equalsIgnoreCase(valueInFields[i]),
					Matchers.equalTo(true));
		}
	}

	private void verifyBlockByRewardName(String reward, String fieldName, String values) {

		String[] fields = fieldName.split(" : ");
		String[] valueInFields = values.split(" : ");

		Validator.verifyThat("reward is displayed!",
				getElement("request.details.value.by.field", "Reward Name").getText().equalsIgnoreCase(reward),
				Matchers.equalTo(true));
		verifyVisible("reward.card.image");
		if (fields[0].equalsIgnoreCase("Manager Status") && (valueInFields[0].equalsIgnoreCase("Approved"))
				|| valueInFields[0].equalsIgnoreCase("Rejected")) {
			verifyVisible("review.comments.field.managers.comments");
		}
		if (reward.equalsIgnoreCase("Bright Spark")) {
			if (fields[0].equalsIgnoreCase("HR Status") && valueInFields[0].equalsIgnoreCase("Approved")) {
				verifyVisible("review.comments.field.hr.comments");
			}
		}

		if (reward.equalsIgnoreCase(PATON_BACK)) {
			verifyVisible("value.addition.field.key.contribution");
		} else {
			verifyVisible("value.addition.field.challenging.stituation");
			verifyVisible("value.addition.field.solutions.provided");
			verifyVisible("value.addition.field.benefits.accured");
			verifyVisible("value.addition.field.rationale.for.nomination");
		}
		showOnlyBackbutton();
	}

	@QAFTestStep(description = "user selects to date {0} from calender")
	public void userSelectsToDateFromCalender(String date) {
		CommonPage.waitForLocToBeClickable(TO_DATE_RNR_REQUESTPAGE);
		selectDate("date.from.rnrrequestpage", "btn.all.date.rnrrequestpage", date);

	}

	@QAFTestStep(description = "user verifies request details page for reward {0} and fields {1} having values {2}")
	public void verifyRequestDetailPage(String reward, String fieldName, String values) {
		verifyBlocksinRequestPage();
		verifyStatusInBlocks(fieldName, values);
		verifyBlockByRewardName(reward, fieldName, values);
	}

	public static void selectDate(String type, String loc, String date) {
		NestUtils.waitForLoader();
		click(type);

		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		element.click();
	}

	@QAFTestStep(description = "user verifies that R&R request page fields should be clear")
	public void userVerifiesThatRRRequestPageFieldsShouldBeClear() {
		List<QAFWebElement> options = NestUtils.getQAFWebElements("dropdown.select.fileds.rnrreportpage");
		for (QAFWebElement e : options) {
			String text = e.getText();
			if ((text).contains("Select")) {
				Reporter.log(text + " field has been cleared", MessageTypes.Pass);

			} else {
				Reporter.log(text + " field not cleared", MessageTypes.Fail);

			}
		}

		LocalDate today = LocalDate.now();
		String firstDay = today.withDayOfMonth(1).toString();
		String lastDay = today.withDayOfMonth(today.lengthOfMonth()).toString();
		String fromDate = CommonStep.getText(TO_DATE_RNR_REQUESTPAGE).toString();
		String toDate = CommonStep.getText(TO_DATE_RNR_REQUESTPAGE).toString();
		ReportsPage.verifyDate(firstDay, fromDate);
		ReportsPage.verifyDate(lastDay, toDate);
	}

	@QAFTestStep(description = "user click on export button")
	public void userClickOnExportButton() {
		if (verifyVisible("btn.export.rnrrequestpage")) {
			Reporter.logWithScreenShot("Export Button is now Present", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Export Button is not Present", MessageTypes.Fail);

		}
	}

	@QAFTestStep(description = "user select the reward type {Bright Spark}")
	public void userSelectTheRewardType(String rewardType) {
		List<QAFWebElement> nominatorList = NestUtils.getQAFWebElements("column.nominatedBy.MyRewardPageListPage.rnr");
		int i = 0;
		switch (rewardType) {

		case "Bright Spark":
			while (!nominatorList.isEmpty()) {

				nominatorList.get(i).click();

				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				NestUtils.scrollUpToElement(new QAFExtendedWebElement("link.BrightSpark.MyRewardPageListPage.rnr"));
				if (CommonStep.verifyPresent("link.BrightSpark.MyRewardPageListPage.rnr")) {
					CommonPage.clickUsingJavaScript("link.BrightSpark.MyRewardPageListPage.rnr");
					break;
				} else {
					Reporter.logWithScreenShot("Failed to click the selected reward link");
				}

				i++;
			}
			break;
		case "Pat On Back":
			while (!nominatorList.isEmpty()) {
				nominatorList.get(i).click();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				NestUtils.scrollUpToElement(new QAFExtendedWebElement(LINK_POB));
				if (CommonStep.verifyPresent(LINK_POB)) {

					CommonPage.clickUsingJavaScript(LINK_POB);
					break;
				} else {
					Reporter.logWithScreenShot("Failed to click the selected reward link");
				}

				i++;
			}
			break;

		case "You Made My Day":
			while (!nominatorList.isEmpty()) {
				nominatorList.get(i).click();
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				NestUtils.scrollUpToElement(new QAFExtendedWebElement("link.YouMadeMyDay.MyRewardPageListPage.rnr"));
				if (CommonStep.verifyPresent("link.YouMadeMyDay.MyRewardPageListPage.rnr")) {

					CommonPage.clickUsingJavaScript("link.YouMadeMyDay.MyRewardPageListPage.rnr");
					break;
				} else {
					Reporter.logWithScreenShot("Failed to click the selected reward link");
				}

				i++;
			}
			break;
		}
	}
}
