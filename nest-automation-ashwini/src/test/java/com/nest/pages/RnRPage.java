package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.nest.beans.BrightsparkBean;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RnRPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	CommonPage commonPage = new CommonPage();
	MISCPage miscPage = new MISCPage();
	public static final String SELECT_CARD = "select.card.r&r";
	public static final String SELECT_BAND = "select.band.r&r";
	public static final String SELECT_NOMINEE =
			CommonPage.buttonTextContains("Select Nominee");
	public static final String SEARCH_ELEMENT = "search.element.r&r";
	public static final String PROJECT_NAME = "project.name.r&r";
	public static final String NOMINEE_NAME = "nominee.name.r&r";
	public static final String WINDOW_SCROLL = "window.scrollBy(0,1200)";
	public static final String APPROVAL_COMMENT = "approval.comment.rnr";
	public static final String BAND_LIST = "band.list.r&r";
	public static final String LIST_REWARDS = "list.rewards.r&r";
	public static final String IMG_LOADER = "img.loader.userhomepage";
	public static final String BTN_ALL = "text.contains.loc.common";
	public static final String BUTTON_NAME = "button.name";
	public static final String TABLE_COLUMN = "table.column";
	public static final String SEARCH_BUTTON = CommonPage.buttonTextEquals("Search");
	public static final String GROUP_DROPDOWN =
			CommonPage.buttonTextContains("Select Group");
	public static final String SELECT_PROJECT =
			CommonPage.buttonTextContains("Select Project");
	public static final String MY_RNR = CommonPage.divTextEquals("My View");
	public static final String SUBMIT_BUTTON = CommonPage.buttonTextContains("Submit");

	public QAFExtendedWebElement getElement(String loc, String text) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), text));
		return element;

	}

	@QAFTestStep(description = "user click search button")
	public void clickSearch() {
		QAFWebElement searchButton = new QAFExtendedWebElement(SEARCH_BUTTON);
		NestUtils.scrollUpToElement(searchButton);
		waitForVisible(SEARCH_BUTTON);
		NestUtils.clickUsingJavaScript(searchButton);
		waitForNotPresent("loading.bar");
	}

	public void verifyTableColumnsForRRRequests() {
		Validator.verifyThat("Nominator column is visible",
				getElement(TABLE_COLUMN, "Nominee").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward Name column is visible",
				getElement(TABLE_COLUMN, "Location").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Nominee column is visible",
				getElement(TABLE_COLUMN, "Reward").isDisplayed(), Matchers.equalTo(true));
		Validator.verifyThat("Department column is visible",
				getElement(TABLE_COLUMN, "Posted Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Manager Status column is visible",
				getElement(TABLE_COLUMN, "Manager Status").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("HR Status column is visible",
				getElement(TABLE_COLUMN, "HR Status").isDisplayed(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies R&R Request list for {0}")
	public void vereifyRRRequestsList(String userType) {
		List<QAFWebElement> requestList =
				NestUtils.getQAFWebElements("request.list.table");
		if (requestList.size() > 0)
			requestList.get(0).click();
		if (userType.equalsIgnoreCase("manager")) {
			verifyVisible(String.format(
					ConfigurationManager.getBundle().getString(BUTTON_NAME), "Approve"));
		}
		verifyVisible(String.format(
				ConfigurationManager.getBundle().getString(BUTTON_NAME), "Reject"));
		verifyVisible(String
				.format(ConfigurationManager.getBundle().getString(BUTTON_NAME), "Back"));
	}

	@QAFTestStep(description = "user verify {page_name} of nominate module with all fileds")
	public void verifyNominatePages(String pageName) {
		verifyPresent("tab.cardselection.nominatepages");
		int totalCards =
				NestUtils.getQAFWebElements("cards.cardselection.nominatepages").size();
		switch (pageName) {
			case "You Made My Day" :
				Validator.verifyThat("Select Card Block must have 5 cards.",
						totalCards == 5, Matchers.equalTo(true));
				verifyPresent("imput.comment.youmademydaypage");
				break;

			case "Pat On Back" :
				Validator.verifyThat("Select Card Block must have 3 cards.",
						totalCards == 3, Matchers.equalTo(true));

				Validator.verifyThat("Enter Contributions text area should be present",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("imput.textarea"),
								"Enter Contributions")).isPresent(),
						Matchers.equalTo(true));

				Validator.verifyThat("Select Project dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Project")).isPresent(),
						Matchers.equalTo(true));
				break;

			case "Bright Spark" :
				Validator.verifyThat("Select Card Block must have 3 cards.",
						totalCards == 3, Matchers.equalTo(true));
				Validator.verifyThat(
						"Enter challenging situations text area should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("imput.textarea"),
								"Enter challenging situations")).isPresent(),
						Matchers.equalTo(true));
				Validator
						.verifyThat(
								"Enter solutions provided text area should be present",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle()
												.getString("imput.textarea"),
										"Enter solutions provided")).isPresent(),
								Matchers.equalTo(true));
				Validator
						.verifyThat("Enter benefits accrued text area should be present",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle()
												.getString("imput.textarea"),
										"Enter benefits accrued")).isPresent(),
								Matchers.equalTo(true));
				Validator.verifyThat("Enter Contributions text area should be present",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("imput.textarea"),
								"Enter contributions")).isPresent(),
						Matchers.equalTo(true));

				Validator.verifyThat("Select Project dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Project")).isPresent(),
						Matchers.equalTo(true));
				break;
		}

		Validator
				.verifyThat("Select groups dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Group")).isPresent(),
						Matchers.equalTo(true));

		Validator.verifyThat("Select Band dropdown should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Select Band"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator
				.verifyThat("Select Nominee dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Nominee")).isPresent(),
						Matchers.equalTo(true));

		Validator.verifyThat("Post button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Post"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Preview button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Preview"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Cancel button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Cancel"))
								.isPresent(),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user should see required filed with astric sign for {page_name} of nominate module")
	public void verifyAstersikSign(String pageName) {

		Validator.verifyThat("Select a Card field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Select a Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator.verifyThat("Nominee field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Nominee"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator.verifyThat("Message on Card field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Message on Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		switch (pageName) {
			case "Pat On Back" :
				Validator
						.verifyThat("Project field having astric sign",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle().getString(
												"button.avlfields.nominatepages"),
										"Project")).getText().contains("*"),
								Matchers.equalTo(true));

				Validator.verifyThat(
						"Key Contribution by Nominee field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Key Contribution by Nominee")).getText().contains("*"),
						Matchers.equalTo(true));

				break;

			case "Bright Spark" :
				Validator
						.verifyThat("Project field having astric sign",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle().getString(
												"button.avlfields.nominatepages"),
										"Project")).getText().contains("*"),
								Matchers.equalTo(true));

				Validator.verifyThat(
						"Challenging situations faced field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Challenging situations faced")).getText().contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat(
						"Solutions provided/Situation handling field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Solutions provided/Situation handling")).getText()
										.contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat("Rationale for Nomination field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Rationale for Nomination")).getText().contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat("Benefits accrued field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Benefits accrued")).getText().contains("*"),
						Matchers.equalTo(true));
				break;

		}

	}

	@QAFTestStep(description = "user select and verify selection of card should get highlighted")
	public void verifySelectedCards() {
		NestUtils.getQAFWebElements("cards.cardselection.nominatepages").get(0).click();
		String selectedCardBorder =
				new QAFExtendedWebElement("select.selectedcard.nominatepages")
						.getCssValue("border-bottom-color");
		Validator.verifyThat("Selected card border should be orange",
				selectedCardBorder.equals(
						ConfigurationManager.getBundle().getString("orange.colorcode")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user nominate pat on back for {0} {1} {2}")
	public void nominatePatonBack(String band, String nomineename, String project)
			throws InterruptedException {

		verifyPresent(SELECT_CARD);

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, -250)");
		click(SELECT_CARD);

		jse.executeScript("scroll(0,250)");

		selectOptions(SELECT_BAND, BAND_LIST, band);

		verifyPresent(SELECT_NOMINEE);

		NestUtils.clickUsingJavaScript(SELECT_NOMINEE);

		sendKeys(nomineename, SEARCH_ELEMENT);
		String nomineeName = new QAFExtendedWebElement(NOMINEE_NAME).getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript(NOMINEE_NAME);

		NestUtils.clickUsingJavaScript(SELECT_PROJECT);
		sendKeys(project, SEARCH_ELEMENT);
		verifyPresent(PROJECT_NAME);
		click(PROJECT_NAME);

	}

	public void checkList() {

		Reporter.log("element list size ::" + returnList("nominee.list.r&r").size());

		List<QAFWebElement> list = returnList("nominee.list.r&r");

		for (int i = 0; i < list.size(); i++) {
			Reporter.log("list is :::" + list.get(i).getText().split("\\+ ")[1]);
			Reporter.log(ConfigurationManager.getBundle().getProperty("NomineeName")
					.toString());
			if (list.get(i).getText().split("\\+ ")[1]
					.equalsIgnoreCase(ConfigurationManager.getBundle()
							.getProperty("NomineeName").toString())) {

				list.get(i).click();

				break;

			}

		}

	}

	public List<QAFWebElement> returnList(String loc) {
		List<QAFWebElement> list = NestUtils.getQAFWebElements(loc);

		return list;

	}

	@QAFTestStep(description = "user verifies R&R nominee Request list for {0}")
	public void displayList(String loc) {

		NestUtils.waitForLoader();
		List<QAFWebElement> list = null;
		switch (loc) {
			case "Pat on Back" :
				list = returnList("rewards.list.r&r");

				Validator.verifyThat("nominee list is rendering...", list.size(),
						Matchers.greaterThan(0));

				for (int i = 0; i < list.size(); i++) {
					Reporter.log("list is ::::" + list.get(i).getText());

				}
				break;
			case "Bright Spark" :
				list = returnList("rewards.list.r&r");

				Validator.verifyThat("nominee list is rendering...", list.size(),
						Matchers.greaterThan(0));

				for (int i = 0; i < list.size(); i++) {
					Reporter.log("list is ::" + list.get(i).getText());

				}
				break;

		}

	}

	public static boolean verifyText(String loc, String text) {
		return new QAFExtendedWebElement(loc).getText().contains(text);
	}

	@SuppressWarnings("unused")
	@QAFTestStep(description = "user click on reward {0}")
	public void checkReward(String loc) {
		NestUtils.waitForLoader();
		List<QAFWebElement> list = null;
		switch (loc) {
			case "Pat on Back" :
				list = returnList(LIST_REWARDS);

				for (int i = 0; i < list.size(); i++) {
					if (loc.equalsIgnoreCase(list.get(i).getText())) {
						list.get(i).click();
						NestUtils.waitForLoader();
						break;
					}
				}
				break;
			case "Bright Spark" :
				list = returnList(LIST_REWARDS);

				for (int i = 0; i < list.size(); i++) {
					if (loc.equalsIgnoreCase(list.get(i).getText())) {
						list.get(i).click();
						break;
					}
				}
				break;
			case "You Made My Day" :
				list = returnList(LIST_REWARDS);

				for (int i = 0; i < list.size(); i++) {
					if (loc.equalsIgnoreCase(list.get(i).getText())) {
						list.get(i).click();
						break;

					}
					break;
				}
		}
	}

	public void selectOptions(String loc, String listloc, String value) {

		CommonPage.clickUsingJavaScript(loc);
		List<QAFWebElement> list = NestUtils.getQAFWebElements(listloc);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().contains(value)) {
				list.get(i).click();
				break;
			}
		}
	}

	@QAFTestStep(description = "user click on logout from page")
	public void clickLogout() {
		NestUtils.waitForLoader();
		verifyPresent("nominee.logout.r&r");
		NestUtils.clickUsingJavaScript("nominee.logout.r&r");

	}

	@QAFTestStep(description = "user text message on page")
	public void messageCard() {
		verifyPresent("message.cardsection.misc");
		NestUtils.waitForLoader();
		CommonStep.sendKeys(
				"Congratulations! on winning Award for your outstanding performance in your project.",
				"message.cardsection.misc");
	}

	@QAFTestStep(description = "user text key contribution")
	public void keyContribution() {
		waitForVisible("key.contributiontextarea.r&r");
		sendKeys("Good work..!!!", "key.contributiontextarea.r&r");
	}

	@QAFTestStep(description = "user click post button")
	public void clickPost() {
		QAFWebElement postButton = new QAFExtendedWebElement(MISCPage.POST_BUTTON);
		NestUtils.scrollUpToElement(postButton);
		waitForVisible(MISCPage.POST_BUTTON);
		NestUtils.clickUsingJavaScript(postButton);

	}

	@QAFTestStep(description = "user click submit button")
	public void clickbutton() {
		waitForVisible(SUBMIT_BUTTON);
		click(SUBMIT_BUTTON);

	}

	@QAFTestStep(description = "user check nominee list for {0}")
	public void nomineeList(String reward) {
		NestUtils.scrollToAxis(255, 0);
		checkList();

		if (reward.equalsIgnoreCase("Pat On Back")) {
			NestUtils.clickUsingJavaScript("nomineelist.patonback.r&r");
		} else {
			NestUtils.clickUsingJavaScript("nomineeelist.brightspark.rnr");
		}
		Reporter.logWithScreenShot("nominee list is display");

	}

	@QAFTestStep(description = "user verify manager name")
	public void manegerName() {

		NestUtils.waitForLoader();
		waitForVisible("nominee.managername.r&r");
		String managerName =
				((JavascriptExecutor) driver)
						.executeScript("return arguments[0].innerText;",
								new QAFExtendedWebElement("nominee.managername.r&r"))
						.toString();

		Reporter.log("maneger name ::: " + managerName);

	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// predefined method

	}

	@QAFTestStep(description = "user nominate bright spark for {0} {1} {2}")
	public void nominateBrightSpark(String band, String nomineename, String project) {

		verifyPresent(SELECT_CARD);
		click(SELECT_CARD);
NestUtils.scrollToAxis(255, 255);
		selectOptions(SELECT_BAND, BAND_LIST, band);

		verifyPresent(SELECT_NOMINEE);

		NestUtils.clickUsingJavaScript(SELECT_NOMINEE);

		sendKeys(nomineename, SEARCH_ELEMENT);
		String nomineeName = new QAFExtendedWebElement(NOMINEE_NAME).getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript(NOMINEE_NAME);

		NestUtils.clickUsingJavaScript(SELECT_PROJECT);
		sendKeys(project, SEARCH_ELEMENT);
		verifyPresent(PROJECT_NAME);
		click(PROJECT_NAME);

	}

	@QAFTestStep(description = "user fill {0} required details for bright spark")
	public void fillRequiredDetailsBrightSpark(String key) {

		BrightsparkBean detail = new BrightsparkBean();
		detail.fillFromConfig(key);
		CommonPage.clickUsingJavaScript("challenging.situations.r&r");
		sendKeys(detail.getChallengingSituations(), "challenging.situations.r&r");
		CommonPage.clickUsingJavaScript("solutions.provided.r&r");
		sendKeys(detail.getSolutionsProvided(), "solutions.provided.r&r");
		CommonPage.clickUsingJavaScript("benefits.accrued.r&r");
		sendKeys(detail.getBenefitsAccrued(), "benefits.accrued.r&r");
		CommonPage.clickUsingJavaScript("rationale.Nomination.r&r");
		sendKeys(detail.getRationaleForNomination(), "rationale.Nomination.r&r");
	}

	@QAFTestStep(description = "user nominate You Made My Day for {0} {1}")
	public void nominateYouMadeMyDay(String group, String nomineename) {

		verifyPresent(SELECT_CARD);
		click(SELECT_CARD);
		verifyPresent(GROUP_DROPDOWN);
		NestUtils.clickUsingJavaScript(GROUP_DROPDOWN);
		sendKeys(group, SEARCH_ELEMENT);

		click("group.name.r&r");

		

		verifyPresent(SELECT_NOMINEE);

		NestUtils.clickUsingJavaScript(SELECT_NOMINEE);

		sendKeys(nomineename, SEARCH_ELEMENT);
		String nomineeName = new QAFExtendedWebElement(NOMINEE_NAME).getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript(NOMINEE_NAME);
	}

	@QAFTestStep(description = "user click on nominee reward list for {0} {1} {2}")
	public void clickOnReward(String nomineeName, String status, String reward) {

		NestUtils.waitForLoader();
		NestUtils.clickUsingJavaScript("list.status.rnr", nomineeName, status, reward);
		NestUtils.waitForLoader();
		Reporter.logWithScreenShot("nominee list and reward is display");

	}

	@QAFTestStep(description = "user click on {0} button")
	public void button(String buttonName) {

		NestUtils.waitForLoader();

		/*
		 * JavascriptExecutor executor =
		 * (JavascriptExecutor) new WebDriverTestBase().getDriver();
		 * executor.executeScript(WINDOW_SCROLL, "");
		 */
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		QAFExtendedWebElement element = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString(BTN_ALL), buttonName));

		NestUtils.clickUsingJavaScript(element);
		NestUtils.waitForElementToBeClickable(element);
		Reporter.logWithScreenShot("button is display");

	}

	@QAFTestStep(description = "user text approval comment and submit")
	public void submitApproval() {
		NestUtils.waitForLoader();
		verifyPresent(APPROVAL_COMMENT);
		verifyVisible(APPROVAL_COMMENT);
		NestUtils.clickUsingJavaScript(APPROVAL_COMMENT);
		sendKeys("good work", APPROVAL_COMMENT);

		Validator.verifyThat("submit button is present", verifyPresent("submit.btn.rnr"),
				Matchers.equalTo(true));
		NestUtils.clickUsingJavaScript("submit.btn.rnr");

	}

	@QAFTestStep(description = "user verifies {0} message")
	public void verifyMessage(String message) {
		NestUtils.waitForLoader();
		NestUtils.verifyMessagePresent("label.status.common", message);

	}

	@QAFTestStep(description = "user verifies {0} Button")
	public void verifyButtons(String btn) {
		waitForNotPresent(IMG_LOADER);
		try {
			QAFExtendedWebElement element = new QAFExtendedWebElement(String
					.format(ConfigurationManager.getBundle().getString(BTN_ALL), btn));
			element.waitForVisible(5000);
			if (element.verifyPresent()) {

				Reporter.log("Button is present");
			}

		} catch (Exception e) {
			Reporter.log("Button is not present");

		}
	}

	@QAFTestStep(description = "user verifies {0} Buttons")
	public void verifyButton(String btn) {
		waitForNotPresent(IMG_LOADER);

		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript(WINDOW_SCROLL, "");
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(BTN_ALL), btn));
		
		System.out.println("$$$$$$$$$$$$$$"+element);
		element.waitForVisible(5000);
		if (element.verifyPresent()) {

			Reporter.logWithScreenShot("Button is Present", MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("Button is not  Present", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies nominee list {0}")
	public void verifyName(String name) {
		waitForNotPresent(IMG_LOADER);
		Reporter.log("Actual Values ::::"
				+ new QAFExtendedWebElement("nomineename.list.r&r").getText());
		Validator.verifyThat(new QAFExtendedWebElement("nomineename.list.r&r").getText()
				.contains(name), Matchers.equalTo(false));

		Reporter.log("nominee name should not display");
	}

	@QAFTestStep(description = "user should see plus sign on screen")
	public void userShouldSeePlusSignOnScreen() {
		verifyPresent(CommonPage.buttonTextContains("+"));
	}

	@QAFTestStep(description = "user should see {0} as {1}")
	public void userShouldSeeAs(String userRole, String status) {
		NestUtils.waitForLoader();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		QAFExtendedWebElement role =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("list.buttonsOnRewardPage.rnr"), userRole));

		if (role.getText().equalsIgnoreCase("Manager Status")) {

			Validator
					.verifyThat(
							new QAFExtendedWebElement(String.format(ConfigurationManager
									.getBundle().getString("Label.Manager.status.rnr"),
									status)).getText().contains(status),
							Matchers.equalTo(true));
			Reporter.logWithScreenShot(userRole + "is" + status);

		} else if (role.getText().equalsIgnoreCase("HR Status")) {
			Validator
					.verifyThat(
							new QAFExtendedWebElement(
									String.format(ConfigurationManager.getBundle()
											.getString("Label.HR.status.rnr"), status))
													.getText().contains(status),
							Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user check team list {0}")
	public void userCheckTeamList(String name) {

		NestUtils.waitForLoader();
		verifyPresent("professional.information.r&r");
		NestUtils.clickUsingJavaScript("professional.information.r&r");
		verifyPresent("reporting.structure.r&r");
		NestUtils.clickUsingJavaScript("reporting.structure.r&r");
		NestUtils.waitForLoader();
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript(WINDOW_SCROLL, "");
		
		String reportees = CommonPage.elementTextContains("My Reportees");
		CommonStep.click(reportees);
		NestUtils.waitForLoader();
		NestUtils.scrollToAxis(255, 255);
		Validator.verifyThat(
				new QAFExtendedWebElement("team.membername.r&r").getText().contains(name),
				Matchers.equalTo(true));

		Reporter.log("nominee name should display");
	}

	@QAFTestStep(description = "user should see the fields")
	public void userShouldSeeTheFields() {
		CommonStep.waitForEnabled("cell.Rewards.MyRewardPageListPage.rnr");
		verifyPresent("cell.Rewards.MyRewardPageListPage.rnr");
		verifyPresent("cell.PublishedDate.MyRewardPageListPage.rnr");
		verifyPresent("cell.NominatedBy.MyRewardPageListPage.rnr");
	}

	@QAFTestStep(description = "user check {0} page")
	public void userCheckText(String name) {

		NestUtils.waitForLoader();
		verifyPresent(MY_RNR);

		Validator.verifyThat(new QAFExtendedWebElement(MY_RNR).getText().contains(name),
				Matchers.equalTo(true));

		Reporter.log("My view page should display");
	}

	@QAFTestStep(description = "user check mandatory field color {0}")
	public void checkButtonColor(String page) {

		NestUtils.waitForLoader();
		switch (page) {
			case "You Made My Day" :
				NestUtils.waitForLoader();

				Validator.verifyThat("nominee field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Reporter.logWithScreenShot("all field display in red color");
				break;

			case "Pat On Back" :

				NestUtils.waitForLoader();
				Validator.verifyThat("nominee field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("project field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("text area color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				break;

			case "Bright Spark" :

				NestUtils.waitForLoader();
				Validator.verifyThat("nominee field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("project field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("Challenging situations faced color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("Solutions provided/Situation handling color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("Benefits accrued color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("Rationale for Nomination color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));
				break;

			case "Bright Spark (US/UK)" :
				Validator.verifyThat("nominee field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Validator.verifyThat("Quarter field color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.buttonmandatoryfield.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));

				Reporter.logWithScreenShot("all field display in red color");

				Validator.verifyThat("Your Comments color",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("page.textarea.r&r")))
										.getCssValue("border-bottom-color").contains(
												"red.colorcode"),
						Matchers.equalTo(true));
				break;
		}

	}

	@QAFTestStep(description = "user selects and verify date of reports page")
	public void userSelectsAndVerifyDateOfReportsPage() {
		String fromDate = new QAFExtendedWebElement("datepiker.rnrrequestpage")
				.getAttribute("value");
		String toDate = new QAFExtendedWebElement("date.from.rnrreportspage")
				.getAttribute("value");
		RR_RequestPage.isValidDate(fromDate);
		RR_RequestPage.isValidDate(toDate);
		RR_RequestPage.checkEndOfMonth(toDate);
	}

	@QAFTestStep(description = "user should verify the fields {0}")
	public void userShouldVerifyTheFields(String fields) {
		NestUtils.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonStep.waitForEnabled(CommonPage.COMMON_BREADCRUMB);
		String[] fieldList = fields.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));
		for (int i = 0; i < fieldList.length; i++) {

			if ((CommonPage.getWebElementFromList("link.tabs.configureLeavePage",
					fieldList[i])).isPresent()) {
				Reporter.log(fieldList[i], MessageTypes.Pass);
			} else {
				Reporter.log(fieldList[i] + "has not been added at the timeof nomination",
						MessageTypes.Pass);
			}

		}

	}

	@QAFTestStep(description = "user enter date as {0}")
	public void userEnterDateAs(String dateString) {
		LeavePage leavePage=new LeavePage();
		leavePage.fillFromDate(dateString);
		
		
	}
	@QAFTestStep(description = "user verify date")
	public void userVerifyDate() {
		String fromDate = new QAFExtendedWebElement("datepiker.rnrrequestpage").getAttribute("value");
		String toDate = new QAFExtendedWebElement(RR_RequestPage.TO_DATE_RNR_REQUESTPAGE).getAttribute("value");
		RR_RequestPage.isValidDate(fromDate);
		RR_RequestPage.isValidDate(toDate);
	}
}