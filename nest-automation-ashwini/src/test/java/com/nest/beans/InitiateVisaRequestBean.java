package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class InitiateVisaRequestBean extends BaseFormDataBean {

	@Randomizer(prefix = "DemoOnsite", type = RandomizerTypes.LETTERS_ONLY, length = 5)
	@UiElement(fieldLoc = "input.txt.onsite.manager.name", order = 1)
	private String onsiteManagerName;

	@Randomizer(prefix = "userOffsite", type = RandomizerTypes.LETTERS_ONLY, length = 5)
	@UiElement(fieldLoc = "input.txt.offsite.manager.name", order = 2)
	private String offsiteManagerName;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 3)
	@UiElement(fieldLoc = "input.txt.duration.number", order = 3)
	private String durationOfVisit;

	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 3)
	@UiElement(fieldLoc = "input.txt.under.graduate.degree", order = 3)
	private String underGraduateDegree;

	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 3)
	@UiElement(fieldLoc = "input.txt.under.graduate.uni", order = 4)
	private String underGraduateUniversity;

	@Randomizer(dataset = "2012")
	@UiElement(fieldLoc = "input.txt.under.graduate.year", order = 5)
	private String undergraduateYear;

	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 3)
	@UiElement(fieldLoc = "input.txt.master.degree", order = 6)
	private String masterDegree;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 3)
	@UiElement(fieldLoc = "input.txt.master.uni", order = 7)
	private String masterUniversity;

	@Randomizer(dataset = "2015", length = 4)
	@UiElement(fieldLoc = "input.txt.master.graduate.year", order = 8)
	private String masterGraduateYear;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 10)
	@UiElement(fieldLoc = "input.txt.address.to.visit", order = 9)
	private String addressTovisit;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 10)
	@UiElement(fieldLoc = "input.txt.reason.for.visa", order = 10)
	private String reasonForVisa;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 10)
	@UiElement(fieldLoc = "input.txt.reason.for.specific.individual", order = 11)
	private String specificIndividual;

	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length = 10)
	@UiElement(fieldLoc = "input.txt.client.name", order = 12)
	private String clientName;
}
