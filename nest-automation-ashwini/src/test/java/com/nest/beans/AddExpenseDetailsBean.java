
package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class AddExpenseDetailsBean extends BaseDataBean {

	@Randomizer(length = 6, type = RandomizerTypes.LETTERS_ONLY)
	private String projectTitle;

	@Randomizer(length = 4, type = RandomizerTypes.LETTERS_ONLY)
	private String projectOther;

	@Randomizer(length = 4, type = RandomizerTypes.DIGITS_ONLY)
	private String expenseAmount;

	@Randomizer(length = 5, type = RandomizerTypes.DIGITS_ONLY)
	private String billNumber;

	@Randomizer(length = 6, type = RandomizerTypes.LETTERS_ONLY)
	private String remarks;

	@Randomizer(length = 8, type = RandomizerTypes.LETTERS_ONLY)
	private String expenseTitle;

	@Randomizer(length = 7, type = RandomizerTypes.LETTERS_ONLY)
	private String expenseDescription;

	@Randomizer(length = 6, type = RandomizerTypes.DIGITS_ONLY)
	private String erpId;

	public String getErpId() {
		return erpId;
	}

	public void setErpId(String erpId) {
		this.erpId = erpId;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getProjectOther() {
		return projectOther;
	}

	public void setProjectOther(String projectOther) {
		this.projectOther = projectOther;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getExpenseTitle() {
		return expenseTitle;
	}

	public void setExpenseTitle(String expenseTitle) {
		this.expenseTitle = expenseTitle;
	}

	public String getExpenseDescription() {
		return expenseDescription;
	}

	public void setExpenseDescription(String expenseDescription) {
		this.expenseDescription = expenseDescription;
	}
}
