package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class LoginBean extends BaseDataBean {

	private String userName;
	private String password;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
