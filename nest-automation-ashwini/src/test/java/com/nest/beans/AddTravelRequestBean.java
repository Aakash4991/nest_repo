package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class AddTravelRequestBean extends BaseDataBean {
	
	private String travelPurpose;
	private String tripType;
	private String travelType;
	private String journeyFrom;
	private String journeyTo;
	private String projectName;
	private String carBooking;
	private String hotelBooking;
	private String extraInfo;
	private String travelTitle;
	
	public String getTravelTitle() {
		return travelTitle;
	}

	public void setTravelTitle(String travelTitle) {
		this.travelTitle = travelTitle;
	}

	public String getTravelpurpose() {
		return travelPurpose;
	}
	
	public String getTriptype() {
		return tripType;
	}
	
	public String getTraveltype() {
		return travelType;
	}

	public String getJourneyfrom() {
		return journeyFrom;
	}
	
	public String getJourneyto() {
		return journeyTo;
	}
	
	public String getProjectname() {
		return projectName;
	}
	
	public String getCarbooking() {
		return carBooking;
	}
	
	public String getHotelbooking() {
		return hotelBooking;
	}
	
	public String getExtrainfo() {
		return extraInfo;
	}
	

}
