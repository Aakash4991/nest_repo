package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class ApplyLeaveBean extends BaseDataBean {

	private String userType;
	private String leaveDays;
	private String leaveReason;
	private String leaveType;
	private String dayType;

	public void setUserType(String userType) {
		this.userType = userType;
	}
	public void setLeaveDays(String leaveDays) {
		this.leaveDays = leaveDays;
	}
	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public void setDayType(String dayType) {
		this.dayType = dayType;
	}

	public String getUserType() {
		return userType;
	}
	public String getLeaveReason() {
		return leaveReason;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public String getDayType() {
		return dayType;
	}
	public String getLeaveDays() {
		return leaveDays;
	}

}
