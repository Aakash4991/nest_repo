package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class CreateNewPostBean extends BaseDataBean {

	private String title;
	private String postUrl;
	private String description;
	
	
	public String getTitle() {
		return title;
	}
	public String getPostUrl() {
		return postUrl;
	}
	public String getDescription() {
		return description;
	}
	
}
