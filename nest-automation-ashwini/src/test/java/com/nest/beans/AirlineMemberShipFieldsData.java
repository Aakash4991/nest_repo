package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class AirlineMemberShipFieldsData extends BaseDataBean  {
	private String airLine;
	private String memberShip;
	private String comment;
	
	public String getAirLine() {
		return airLine;
	}
	public void setAirLine(String airLine) {
		this.airLine = airLine;
	}
	public String getMemberShip() {
		return memberShip;
	}
	public void setMemberShip(String memberShip) {
		this.memberShip = memberShip;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}
