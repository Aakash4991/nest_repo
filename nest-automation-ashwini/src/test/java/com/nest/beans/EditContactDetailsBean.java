package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class EditContactDetailsBean extends BaseDataBean {
	@Randomizer(length=9,type = RandomizerTypes.LETTERS_ONLY)
	private String permanentAddress;
	@Randomizer(length=4,type = RandomizerTypes.LETTERS_ONLY)
	private String percity;
	@Randomizer(dataset="India")
	private String perCountry;
	@Randomizer(length=5,type = RandomizerTypes.LETTERS_ONLY)
	private String perState;
	@Randomizer(length=6,type = RandomizerTypes.DIGITS_ONLY)
	private String perPostalCode;
	@Randomizer(length=9,type = RandomizerTypes.LETTERS_ONLY)
	private String curAddress;
	@Randomizer(length=4,type = RandomizerTypes.LETTERS_ONLY)
	private String curcity;
	@Randomizer(dataset="India")
	private String curCountry;
	@Randomizer(length=5,type = RandomizerTypes.LETTERS_ONLY)
	private String curState;
	@Randomizer(length=6,type = RandomizerTypes.DIGITS_ONLY)
	private String curPostalCode;
	@Randomizer(length=6,type = RandomizerTypes.DIGITS_ONLY)
	private String homeNumber;
	@Randomizer(length=6,type = RandomizerTypes.DIGITS_ONLY)
	private String mobileNumber;
	@Randomizer(length=6,type = RandomizerTypes.DIGITS_ONLY)
	private String workNumber;
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public String getPercity() {
		return percity;
	}
	public void setPercity(String percity) {
		this.percity = percity;
	}
	public String getPerCountry() {
		return perCountry;
	}
	public void setPerCountry(String perCountry) {
		this.perCountry = perCountry;
	}
	public String getPerState() {
		return perState;
	}
	public void setPerState(String perState) {
		this.perState = perState;
	}
	public String getPerPostalCode() {
		return perPostalCode;
	}
	public void setPerPostalCode(String perPostalCode) {
		this.perPostalCode = perPostalCode;
	}
	public String getCurAddress() {
		return curAddress;
	}
	public void setCurAddress(String curmanentAddress) {
		this.curAddress = curmanentAddress;
	}
	public String getCurcity() {
		return curcity;
	}
	public void setCurcity(String curcity) {
		this.curcity = curcity;
	}
	public String getCurCountry() {
		return curCountry;
	}
	public void setCurCountry(String curCountry) {
		this.curCountry = curCountry;
	}
	public String getCurState() {
		return curState;
	}
	public void setCurState(String curState) {
		this.curState = curState;
	}
	public String getCurPostalCode() {
		return curPostalCode;
	}
	public void setCurPostalCode(String curPostalCode) {
		this.curPostalCode = curPostalCode;
	}
	public String getHomeNumber() {
		return homeNumber;
	}
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getWorkNumber() {
		return workNumber;
	}
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}
	public String getPersonelNumber() {
		return personelNumber;
	}
	public void setPersonelNumber(String personelNumber) {
		this.personelNumber = personelNumber;
	}
	private String personelNumber;

}
