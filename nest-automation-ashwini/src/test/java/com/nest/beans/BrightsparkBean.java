package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class BrightsparkBean extends BaseDataBean {

	private String challengingSituations;
	private String solutionsProvided;
	private String benefitsAccrued;
	private String rationaleForNomination;

	public String getChallengingSituations() {
		return challengingSituations;
	}

	public String getSolutionsProvided() {
		return solutionsProvided;
	}

	public String getBenefitsAccrued() {
		return benefitsAccrued;
	}

	public String getRationaleForNomination() {
		return rationaleForNomination;
	}

}
