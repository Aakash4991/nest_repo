package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class ProfessionalExperienceBean extends BaseDataBean {

	private String ComapnyName;
	private String jobTitle;
	private String fromDate;
	private String toDate;
	private String comment;
	
	public String getComapnyName() {
		return ComapnyName;
	}
	public void setComapnyName(String comapnyName) {
		ComapnyName = comapnyName;
	}
	
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
